

%:
	echo '(load "compiler.scm") (compile-scheme-file "$(MAKECMDGOALS).scm" "$(MAKECMDGOALS).s")' | scheme -q

	nasm -f elf64 $(MAKECMDGOALS).s -o $(MAKECMDGOALS).o

	gcc -m64 -Wall -g $(MAKECMDGOALS).o -o $(MAKECMDGOALS)
	
#tell make that "clean" is not a file name!
.PHONY: clean

#Clean the build directory
c: 
	rm -f test.s test.o test