 
;;; Execution file compiled by Scheme Compiler.
;;; 
;;; Programmers: Shahar Dotan & Amir Itzhaky

%define T_UNDEFINED 0
%define T_VOID 1
%define T_NIL 2
%define T_INTEGER 3
%define T_FRACTION 4
%define T_BOOL 5
%define T_CHAR 6
%define T_STRING 7
%define T_SYMBOL 8
%define T_CLOSURE 9
%define T_PAIR 10
%define T_VECTOR 11
%define T_PAIR_OPT 12

%define CHAR_NUL 0
%define CHAR_TAB 9
%define CHAR_NEWLINE 10
%define CHAR_PAGE 12
%define CHAR_RETURN 13
%define CHAR_QUOTE 34
%define CHAR_SPACE 32

%define TYPE_BITS 4
%define WORD_SIZE 64

%macro PRINTFF 1                       
    push rax
    push rdi
    mov rax, 0
    mov rdi, %1
    call printf
    pop rdi
    pop rax
%endmacro 

;;; target, index
%macro GET_FROM_STACK 2
    mov qword %1, [rsp + 8*%2]
%endmacro 

;;; address, offset
%macro COPY_STACK 2
	push rbx
	
    mov rbx, qword [%1]
	mov qword [%1 + %2*8], rbx
	
	pop rbx
%endmacro 

;;; size_to_malloc, target
%macro NEW_MALLOC 2                    ; gets 2 args (allocate size, register of pointer)
    push R15
	push R14
	push R13
	push R12
	push R11
	push R10
	push R9
	push R8
    push rcx                           ; save register before malloc
    push rdx                           ; save register before malloc
    push rsi                           ; save register before malloc
    push rdi
    push rax                           ; save register before malloc
    mov rdi, %1                        ; push allocate size
    call malloc                        ; function call
    mov rbx, rax                       ; arg2 <- the return pointer of malloc
    pop rax                            ; pop register after malloc 
    pop rdi
    pop rsi                            ; pop register after malloc
    pop rdx                            ; pop register after malloc
    pop rcx                            ; pop register after malloc
	pop R8
	pop R9
	pop R10
	pop R11
	pop R12
	pop R13
	pop R14
	pop R15                           ; pop register after malloc
    mov %2, rbx
%endmacro 

%define MAKE_LITERAL(type, lit) ((lit << TYPE_BITS) | type)

%define MAKE_LITERAL_FRACTION(num, denum) ((((num - start_of_data << 28) | denum - start_of_data) << TYPE_BITS) | T_FRACTION)

%macro MAKE_LITERAL_FRACTION 2
        NEW_MALLOC 8, rdx
        mov qword [rdx], %1
        NEW_MALLOC 8, rcx
        mov qword [rcx], %2
        sub rdx, start_of_data
        sub rcx, start_of_data
        shl rdx, 28
        or rdx, rcx
        shl rdx, 4
        or rdx, T_FRACTION
        mov rax, rdx
%endmacro

%macro NUMERATOR 1
	sar %1, 32
	add %1, start_of_data
	mov %1, qword [%1]
	DATA %1
%endmacro

%macro DENOMERATOR 1
        sal %1, 32
	sar %1, 36
	add %1, start_of_data
	mov %1, qword [%1]
	DATA %1
%endmacro


%macro TYPE 1
	and %1, ((1 << TYPE_BITS) - 1) 
%endmacro

%macro DATA 1
	sar %1, TYPE_BITS
%endmacro

%macro DATA_UPPER 1
	sar %1, (((WORD_SIZE - TYPE_BITS) >> 1) + TYPE_BITS)
%endmacro

%macro DATA_LOWER 1
	sal %1, ((WORD_SIZE - TYPE_BITS) >> 1)
	DATA_UPPER %1
%endmacro

%define MAKE_LITERAL_PAIR(car, cdr) (((((car - start_of_data) << ((WORD_SIZE - TYPE_BITS) >> 1)) | (cdr - start_of_data)) << TYPE_BITS) | T_PAIR)

;;car cdr
%macro MAKE_LITERAL_PAIR_OPT 2
    NEW_MALLOC 16, rax
    
    push R10
    
    mov R10, %1
	mov qword [rax], R10
    mov R10, %2
    mov qword [rax + 8], R10
	shl rax, 4
	or rax, T_PAIR_OPT
    
    pop R10
%endmacro

%macro CAR_OPT 1
        shr %1, 4
	mov rax, qword [%1]
%endmacro

%macro CDR_OPT 1
	shr %1, 4
	mov rax, qword [%1 + 8]
%endmacro

%macro CAR 1
	DATA_UPPER %1
	add %1, start_of_data
	mov %1, qword [%1]
%endmacro

%macro CDR 1
	DATA_LOWER %1
	add %1, start_of_data
	mov %1, qword [%1]
%endmacro

;;; MAKE_LITERAL_CLOSURE target, env, code
%macro MAKE_LITERAL_CLOSURE 3
	push rax
	push rbx
	mov rax, %1
	mov qword [rax], %2
	sub qword [rax], start_of_data
	shl qword [rax], ((WORD_SIZE - TYPE_BITS) >> 1)
	lea rbx, [rax + 8]
	sub rbx, start_of_data
	or qword [rax], rbx
	shl qword [rax], TYPE_BITS
	or qword [rax], T_CLOSURE
	mov qword [rax + 8], %3
	pop rbx
	pop rax
%endmacro

%macro CLOSURE_ENV 1
	DATA_UPPER %1
	add %1, start_of_data
%endmacro

%macro CLOSURE_CODE 1
	DATA_LOWER %1
	add %1, start_of_data
	mov %1, qword [%1]
%endmacro

%macro MAKE_LITERAL_STRING 1+
	dq (((((%%LstrEnd - %%Lstr) << ((WORD_SIZE - TYPE_BITS) >> 1)) | (%%Lstr - start_of_data)) << TYPE_BITS) | T_STRING)
	%%Lstr:
	db %1
	%%LstrEnd:
%endmacro

%define MAKE_LITERAL_SYMBOL(str) ((str - start_of_data) << TYPE_BITS | T_SYMBOL)


%macro STRING_LENGTH 1
	DATA_UPPER %1
%endmacro

%macro STRING_ELEMENTS 1
	DATA_LOWER %1
	add %1, start_of_data
%endmacro

;;; STRING_REF dest, src, index
;;; dest cannot be RAX! (fix this!)
%macro STRING_REF 3
	push rax
	mov rax, %2
	STRING_ELEMENTS rax
	add rax, %3
	mov %1, byte [rax]
	pop rax
%endmacro

%macro MAKE_LITERAL_VECTOR 1+
	dq ((((((%%VecEnd - %%Vec) >> 3) << ((WORD_SIZE - TYPE_BITS) >> 1)) | (%%Vec - start_of_data)) << TYPE_BITS) | T_VECTOR)
	%%Vec:
	dq %1
	%%VecEnd:
%endmacro

%macro MAKE_LITERAL_VECTOR 0
	dq ((((((%%VecEnd - %%Vec) >> 3) << ((WORD_SIZE - TYPE_BITS) >> 1)) | (%%Vec - start_of_data)) << TYPE_BITS) | T_VECTOR)
	%%Vec:
	%%VecEnd:
%endmacro

%macro VECTOR_LENGTH 1
	DATA_UPPER %1
%endmacro

%macro VECTOR_ELEMENTS 1
	DATA_LOWER %1
	add %1, start_of_data
%endmacro

;;; VECTOR_REF dest, src, index
;;; dest cannot be RAX! (fix this!)
%macro VECTOR_REF 3
	mov %1, %2
	VECTOR_ELEMENTS %1
	lea %1, [%1 + %3*8]
	mov %1, qword [%1]
	mov %1, qword [%1]
%endmacro

%define SOB_UNDEFINED MAKE_LITERAL(T_UNDEFINED, 0)
%define SOB_VOID MAKE_LITERAL(T_VOID, 0)
%define SOB_FALSE MAKE_LITERAL(T_BOOL, 0)
%define SOB_TRUE MAKE_LITERAL(T_BOOL, 1)
%define SOB_NIL MAKE_LITERAL(T_NIL, 0)


section .text
write_sob_undefined:
	push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .undefined
	call printf

	leave
	ret

section .data
.undefined:
	db "#<undefined>", 0

write_sob_integer:
	push rbp
	mov rbp, rsp

	mov rsi, qword [rbp + 8 + 1*8]
	sar rsi, TYPE_BITS
	mov rdi, .int_format_string
	mov rax, 0
	call printf

	leave
	ret

section .data
.int_format_string:
	db "%ld", 0

write_sob_char:
	push rbp
	mov rbp, rsp

	mov rsi, qword [rbp + 8 + 1*8]
	DATA rsi

	cmp rsi, CHAR_NUL
	je .Lnul

	cmp rsi, CHAR_TAB
	je .Ltab

	cmp rsi, CHAR_NEWLINE
	je .Lnewline

	cmp rsi, CHAR_PAGE
	je .Lpage

	cmp rsi, CHAR_RETURN
	je .Lreturn

	cmp rsi, CHAR_SPACE
	je .Lspace
	jg .Lregular

	mov rdi, .special
	jmp .done	

.Lnul:
	mov rdi, .nul
	jmp .done

.Ltab:
	mov rdi, .tab
	jmp .done

.Lnewline:
	mov rdi, .newline
	jmp .done

.Lpage:
	mov rdi, .page
	jmp .done

.Lreturn:
	mov rdi, .return
	jmp .done

.Lspace:
	mov rdi, .space
	jmp .done

.Lregular:
	mov rdi, .regular
	jmp .done

.done:
	mov rax, 0
	call printf

	leave
	ret

section .data
.space:
	db "#\space", 0
.newline:
	db "#\newline", 0
.return:
	db "#\return", 0
.tab:
	db "#\tab", 0
.page:
	db "#\page", 0
.nul:
	db "#\nul", 0
.special:
	db "#\x%01x", 0
.regular:
	db "#\%c", 0

write_sob_void:
	push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .void
	call printf

	leave
	ret

section .data
.void:
	db "#<void>", 0
	
write_sob_bool:
	push rbp
	mov rbp, rsp

	mov rax, qword [rbp + 8 + 1*8]
	cmp rax, SOB_FALSE
	je .sobFalse
	
	mov rdi, .true
	jmp .continue

.sobFalse:
	mov rdi, .false

.continue:
	mov rax, 0
	call printf	

	leave
	ret

section .data			
.false:
	db "#f", 0
.true:
	db "#t", 0

write_sob_nil:
	push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .nil
	call printf

	leave
	ret

section .data
.nil:
	db "()", 0

write_sob_string:
	push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .double_quote
	call printf

	mov rax, qword [rbp + 8 + 1*8]
	mov rcx, rax
	STRING_LENGTH rcx
	STRING_ELEMENTS rax

.loop:
	cmp rcx, 0
	je .done
	mov bl, byte [rax]
	and rbx, 0xff

	cmp rbx, CHAR_TAB
	je .ch_tab
	cmp rbx, CHAR_NEWLINE
	je .ch_newline
	cmp rbx, CHAR_PAGE
	je .ch_page
	cmp rbx, CHAR_RETURN
	je .ch_return
	cmp rbx, CHAR_QUOTE
	je .ch_quote
	cmp rbx, CHAR_SPACE
	jl .ch_hex
	
	mov rdi, .fs_simple_char
	mov rsi, rbx
	jmp .printf
	
.ch_hex:
	mov rdi, .fs_hex_char
	mov rsi, rbx
	jmp .printf
	
.ch_tab:
	mov rdi, .fs_tab
	mov rsi, rbx
	jmp .printf
	
.ch_page:
	mov rdi, .fs_page
	mov rsi, rbx
	jmp .printf
	
.ch_return:
	mov rdi, .fs_return
	mov rsi, rbx
	jmp .printf
	
.ch_quote:
	mov rdi, .fs_quote
	mov rsi, rbx
	jmp .printf

.ch_newline:
	mov rdi, .fs_newline
	mov rsi, rbx

.printf:
	push rax
	push rcx
	mov rax, 0
	call printf
	pop rcx
	pop rax

	dec rcx
	inc rax
	jmp .loop

.done:
	mov rax, 0
	mov rdi, .double_quote
	call printf

	leave
	ret
section .data
.double_quote:
	db '"', 0
.fs_simple_char:
	db "%c", 0
.fs_hex_char:
	db "\x%01x;", 0	
.fs_tab:
	db "\t", 0
.fs_quote:
        db "Fail", 0
.fs_page:
	db "\f", 0
.fs_return:
	db "\r", 0
.fs_newline:
	db "\n", 0

write_sob_pair_opt:
    push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .open_paren
	call printf
	mov rax, qword [rbp + 8 + 1*8]
	CAR_OPT rax
	push rax
	call write_sob
	add rsp, 1*8
	mov rax, qword [rbp + 8 + 1*8]
	CDR_OPT rax
	push rax
	call write_sob_pair_on_cdr_opt
	add rsp, 1*8
	mov rdi, .close_paren
	mov rax, 0
	call printf

	leave
	ret
	
section .data
.open_paren:
	db "(", 0
.close_paren:
	db ")", 0
	
write_sob_pair_on_cdr_opt:
	push rbp
	mov rbp, rsp

	mov rbx, qword [rbp + 8 + 1*8]
	
	mov rax, rbx
	TYPE rbx
	cmp rbx, T_NIL
	je .done
	cmp rbx, T_PAIR
	je write_sob_pair_on_cdr.cdrIsPair
	cmp rbx, T_PAIR_OPT
	je .cdrIsPair_opt
	push rax
	mov rax, 0
	mov rdi, .dot
	call printf
	call write_sob
	add rsp, 1*8
	jmp .done

.cdrIsPair_opt:
	mov rbx, rax
	mov rcx, rbx
	CDR_OPT rbx
	push rax
	CAR_OPT rcx
	push rax
	mov rax, 0
	mov rdi, .space
	call printf
	call write_sob
	add rsp, 1*8
	call write_sob_pair_on_cdr_opt
	add rsp, 1*8

.done:
	leave
	ret

section .data
.space:
	db " ", 0
.dot:
	db " . ", 0
	
write_sob_pair:
	push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .open_paren
	call printf
	mov rax, qword [rbp + 8 + 1*8]
	CAR rax
	push rax
	call write_sob
	add rsp, 1*8
	mov rax, qword [rbp + 8 + 1*8]
	CDR rax
	push rax
	call write_sob_pair_on_cdr
	add rsp, 1*8
	mov rdi, .close_paren
	mov rax, 0
	call printf

	leave
	ret

section .data
.open_paren:
	db "(", 0
.close_paren:
	db ")", 0

write_sob_pair_on_cdr:
	push rbp
	mov rbp, rsp

	mov rbx, qword [rbp + 8 + 1*8]
	mov rax, rbx
	TYPE rbx
	cmp rbx, T_NIL
	je .done
	cmp rbx, T_PAIR
	je .cdrIsPair
	cmp rbx, T_PAIR_OPT
	je write_sob_pair_on_cdr_opt.cdrIsPair_opt
	push rax
	mov rax, 0
	mov rdi, .dot
	call printf
	call write_sob
	add rsp, 1*8
	jmp .done

.cdrIsPair:
	mov rbx, rax
	CDR rbx
	push rbx
	CAR rax
	push rax
	mov rax, 0
	mov rdi, .space
	call printf
	call write_sob
	add rsp, 1*8
	call write_sob_pair_on_cdr
	add rsp, 1*8

.done:
	leave
	ret

section .data
.space:
	db " ", 0
.dot:
	db " . ", 0

write_sob_vector:
	push rbp
	mov rbp, rsp

	mov rax, 0
	mov rdi, .fs_open_vector
	call printf

	mov rax, qword [rbp + 8 + 1*8]
	mov rcx, rax
	VECTOR_LENGTH rcx
	cmp rcx, 0
	je .done
	VECTOR_ELEMENTS rax

	push rcx
	push rax
	mov rax, qword [rax]
	push qword [rax]
	call write_sob
	add rsp, 1*8
	pop rax
	pop rcx
	dec rcx
	add rax, 8

.loop:
	cmp rcx, 0
	je .done

	push rcx
	push rax
	mov rax, 0
	mov rdi, .fs_space
	call printf
	
	pop rax
	push rax
	mov rax, qword [rax]
	push qword [rax]
	call write_sob
	add rsp, 1*8
	pop rax
	pop rcx
	dec rcx
	add rax, 8
	jmp .loop

.done:
	mov rax, 0
	mov rdi, .fs_close_vector
	call printf

	leave
	ret

section	.data
.fs_open_vector:
	db "#(", 0
.fs_close_vector:
	db ")", 0
.fs_space:
	db " ", 0

write_sob_symbol:
	push rbp
	mov rbp, rsp

	mov rax, qword [rbp + 8 + 1*8]
	shr rax, 4
	add rax, start_of_data
	mov rax, qword [rax]
	
	mov rcx, rax
	STRING_LENGTH rcx
	STRING_ELEMENTS rax

.loop:
	cmp rcx, 0
	je .done
	mov bl, byte [rax]
	and rbx, 0xff

	cmp rbx, CHAR_TAB
	je .ch_tab
	cmp rbx, CHAR_NEWLINE
	je .ch_newline
	cmp rbx, CHAR_PAGE
	je .ch_page
	cmp rbx, CHAR_RETURN
	je .ch_return
	cmp rbx, CHAR_SPACE
	jl .ch_hex
	
	mov rdi, .fs_simple_char
	mov rsi, rbx
	jmp .printf
	
.ch_hex:
	mov rdi, .fs_hex_char
	mov rsi, rbx
	jmp .printf
	
.ch_tab:
	mov rdi, .fs_tab
	mov rsi, rbx
	jmp .printf
	
.ch_page:
	mov rdi, .fs_page
	mov rsi, rbx
	jmp .printf
	
.ch_return:
	mov rdi, .fs_return
	mov rsi, rbx
	jmp .printf

.ch_newline:
	mov rdi, .fs_newline
	mov rsi, rbx

.printf:
	push rax
	push rcx
	mov rax, 0
	call printf
	pop rcx
	pop rax

	dec rcx
	inc rax
	jmp .loop

.done:

	leave
	ret
	
section .data
.double_quote:
	db '"', 0
.fs_simple_char:
	db "%c", 0
.fs_hex_char:
	db "\x%02x;", 0	
.fs_tab:
	db "\t", 0
.fs_page:
	db "\f", 0
.fs_return:
	db "\r", 0
.fs_newline:
	db "\n", 0
	
write_sob_fraction:
	push rbp
	mov rbp, rsp

	
	mov rsi, qword [rbp + 8 + 1*8]
	shr rsi, 32
	add rsi, start_of_data
	mov rsi, qword [rsi]
	DATA rsi
	mov rdx, qword [rbp + 8 + 1*8]
	shl rdx, 32
	shr rdx, 36
	add rdx, start_of_data
	mov rdx, qword [rdx]
	DATA rdx
	mov rdi, .fraction_format_string
	mov rax, 0
	call printf
	
	leave
	ret
	
section .data
.fraction_format_string:
	db "%ld/%ld", 0

write_sob_closure:
	push rbp
	mov rbp, rsp

	mov rsi, qword [rbp + 8 + 1*8]
	mov rdx, rsi
	CLOSURE_ENV rsi
	CLOSURE_CODE rdx
	mov rdi, .closure
	mov rax, 0
	call printf

	leave
	ret
section .data
.closure:
	db "#<closure [env:%p, code:%p]>", 0

write_sob:
	mov rax, qword [rsp + 1*8]
	TYPE rax
	jmp qword [.jmp_table + rax * 8]

section .data
.jmp_table:
	dq write_sob_undefined, write_sob_void, write_sob_nil
	dq write_sob_integer, write_sob_fraction, write_sob_bool
	dq write_sob_char, write_sob_string, write_sob_symbol
	dq write_sob_closure, write_sob_pair, write_sob_vector
	dq write_sob_pair_opt

section .text
write_sob_if_not_void:
	mov rax, qword [rsp + 1*8]
	cmp rax, SOB_VOID
	je .continue

	push rax
	call write_sob
	add rsp, 1*8
	mov rax, 0
	mov rdi, .newline
	call printf
	
.continue:
	ret
section .data
.newline:
	db CHAR_NEWLINE, 0
	
gcd:
	push rbp
	mov rbp, rsp
	
	push rbx
	push rdx

	mov rdx, 0
	mov rax, [rbp + 8 + 1*8] ; first
	mov rbx, [rbp + 8 + 2*8] ; second

	;;MAYBE ABS HERE
	
.loop:
	cmp rbx, 0
	je .done
	mov rdx, 0
	div rbx
	mov rax, rbx
	mov rbx, rdx
	jmp .loop

.done:
        pop rdx
        pop rbx
    
	leave
	ret

     

section .bss

extern exit, printf, scanf, malloc

section .text 
     align 16 
     global main

section .data
start_of_data:
symbol_table:
	dq SOB_UNDEFINED
c0:
	 dq SOB_NIL
c1:
	 dq MAKE_LITERAL(T_BOOL, 1)
c2:
	 dq MAKE_LITERAL(T_BOOL, 0)
c3:
	 dq MAKE_LITERAL(T_INTEGER, 0)
c4:
	 dq MAKE_LITERAL(T_INTEGER, 1)
c5:
	 dq MAKE_LITERAL(T_INTEGER, 2)
c6:
	 dq MAKE_LITERAL(T_INTEGER, 5)
c7:
	 dq MAKE_LITERAL_FRACTION(c6, c5)
c8:
	 dq MAKE_LITERAL(T_INTEGER, 4)
c9:
	 dq SOB_VOID

section .data
string_table:
s2:
	 MAKE_LITERAL_STRING "applic"
s3:
	 MAKE_LITERAL_STRING "fvar"
s0:
	 MAKE_LITERAL_STRING "<"
s1:
	 MAKE_LITERAL_STRING "const"

section .data
globals:
g49:
	 dq SOB_UNDEFINED
g50:
	 dq SOB_UNDEFINED
g47:
	 dq SOB_UNDEFINED
g48:
	 dq SOB_UNDEFINED
g45:
	 dq SOB_UNDEFINED
g46:
	 dq SOB_UNDEFINED
g43:
	 dq SOB_UNDEFINED
g44:
	 dq SOB_UNDEFINED
g41:
	 dq SOB_UNDEFINED
g42:
	 dq SOB_UNDEFINED
g39:
	 dq SOB_UNDEFINED
g40:
	 dq SOB_UNDEFINED
g37:
	 dq SOB_UNDEFINED
g38:
	 dq SOB_UNDEFINED
g35:
	 dq SOB_UNDEFINED
g36:
	 dq SOB_UNDEFINED
g33:
	 dq SOB_UNDEFINED
g34:
	 dq SOB_UNDEFINED
g31:
	 dq SOB_UNDEFINED
g32:
	 dq SOB_UNDEFINED
g29:
	 dq SOB_UNDEFINED
g30:
	 dq SOB_UNDEFINED
g27:
	 dq SOB_UNDEFINED
g28:
	 dq SOB_UNDEFINED
g25:
	 dq SOB_UNDEFINED
g26:
	 dq SOB_UNDEFINED
g23:
	 dq SOB_UNDEFINED
g24:
	 dq SOB_UNDEFINED
g21:
	 dq SOB_UNDEFINED
g22:
	 dq SOB_UNDEFINED
g19:
	 dq SOB_UNDEFINED
g20:
	 dq SOB_UNDEFINED
g17:
	 dq SOB_UNDEFINED
g18:
	 dq SOB_UNDEFINED
g15:
	 dq SOB_UNDEFINED
g16:
	 dq SOB_UNDEFINED
g13:
	 dq SOB_UNDEFINED
g14:
	 dq SOB_UNDEFINED
g11:
	 dq SOB_UNDEFINED
g12:
	 dq SOB_UNDEFINED
g9:
	 dq SOB_UNDEFINED
g10:
	 dq SOB_UNDEFINED
g7:
	 dq SOB_UNDEFINED
g8:
	 dq SOB_UNDEFINED
g5:
	 dq SOB_UNDEFINED
g6:
	 dq SOB_UNDEFINED
g3:
	 dq SOB_UNDEFINED
g4:
	 dq SOB_UNDEFINED
g1:
	 dq SOB_UNDEFINED
g2:
	 dq SOB_UNDEFINED
g0:
	 dq SOB_UNDEFINED
my_very_own_reverse:
	 dq SOB_UNDEFINED

section .text

main:

	push 0
	push 0
	NEW_MALLOC 8, rbx
	mov qword [rbx], -1
	mov rax, rbx
	NEW_MALLOC 8, rbx
	mov qword [rbx], rax
	push rbx
	push 0
	push -1
	mov rbp, rsp

	jmp L_main_is_ready

B_apply:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	
	mov rbx, qword [rbp + 5*8]
	mov rcx, rbx
	mov rdx, rbx
	mov R11, rax
	
	mov rax, [my_very_own_reverse]
	
	push SOB_NIL
	push rbx
	push 1
	
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	mov rbx, rax
	CLOSURE_CODE rbx
	call rbx
	
	mov R10, rax
	add rsp, 8
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8
	mov rax, R10
	mov rdx, rax
	
	mov R8, 0
	mov rcx, rax
	mov rdx, rax
	push SOB_NIL
	TYPE rcx
	cmp rcx, T_PAIR
	je .continue_reg
	cmp rcx, T_PAIR_OPT
	je .continue_opt
	cmp rcx, T_NIL
	je .done
	jmp ERROR_INVALID_ARGS
	
.continue_reg:
	cmp rdx, SOB_NIL
	je .done
	inc R8
	mov rcx, rdx
	CAR rcx
	push rcx
	CDR rdx
	jmp .continue_reg
	
.continue_opt:
	cmp rdx, SOB_NIL
	je .done
	inc R8
	mov rax, rdx
	CAR_OPT rax
	push rax
	CDR_OPT rdx
	mov rdx, rax
	jmp .continue_opt
        
	
.done:
	mov R11, qword [rbp + 4*8]
	mov rax, R11
	push R8
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	mov rbx, rax
	CLOSURE_CODE rbx
	call rbx
	
	mov R10, rax
	add rsp, 8
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8
	mov rax, R10
	
	leave
	ret
	

B_car:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	CAR rax
	jmp .done_car
	
.pair_opt:
	CAR_OPT rax

.done_car:
	leave
	ret
	

B_cdr:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	CDR rax
	jmp .done_cdr
	
.pair_opt:
	CDR_OPT rax

.done_cdr:
	leave
	ret
	

B_length:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	cmp rax, SOB_NIL
	je .done_length
	
.loop_length:
	inc rcx
	CDR rax
	cmp rax, SOB_NIL
	jne .loop_length
	jmp .done_length
	
.pair_opt:
	cmp rax, SOB_NIL
	je .done_length
	
.loop_opt_length:
	inc rcx
	CDR_OPT rax
	cmp rax, SOB_NIL
	jne .loop_opt_length

.done_length:
	mov rax, rcx	
	shl rax, 4
	or rax, T_INTEGER
	leave
	ret
	

B_multiply:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 0
	mov rax, 1
	cmp rcx, rbx
	je .done_multiply
	
.loop_multiply:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_multiply
	DATA rdx
	mul rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_multiply
	jmp .done_multiply
	
.fractions_multiply:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .mul_frac_to_frac

.mul_frac_to_int:
        DATA rdx
        mul rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	jmp .loop_fraction
	
.mul_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R13, rax
	pop rax
	mul R11
	sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	jmp .loop_fraction
	
.done_multiply:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
.done_multiply_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        cmp R13, 1
        je .denom_is_1
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
	
.denom_is_1:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	

B_divide:
	push rbp
	mov rbp, rsp
	
	mov R13, 1
	mov rbx, qword [rbp + 3*8]
	mov rcx, 1
        cmp rcx, rbx
	je .one_arg_divide
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .first_fraction
	DATA rax
	jmp .loop_fraction
	
.first_fraction:
        mov rdx, rax
	NUMERATOR rax
	mov R13, rdx
	DENOMERATOR R13
	jmp .loop_fraction
	
.loop_divide:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_divide
	DATA rdx
	mov R11, rdx
	mov rdx, 0
	div R11
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_divide
	jmp .done_divide_fraction
	
.fractions_divide:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	push rax
	mov rax, R13
	mul R11
	mov R13, rax
	pop rax
	sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .div_frac_to_frac

.div_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mov rdx, 0
        mul R13
        mov R13, rax
        pop rax
        sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	jmp .loop_fraction
	
.div_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R11
	mov R13, rax
	pop rax
	mul R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	jmp .loop_fraction
	
.done_divide_fraction:

        mov rbx, R13
        shl rbx, 4
        cmp rbx, 0
        jg .no_switch
        
        push rax
        mov rax, R13
        mov rdx, -1
        mul rdx
        mov R13, rax
        pop rax
        mov rdx, -1
        mul rdx
        
.no_switch:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:
        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        

        cmp R13, 1
        je .done_divide
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret

.one_arg_divide:
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .only_fraction
	
.only_integer:
        mov rax, 1
        mov R13, qword [rbp + 4*8]
        DATA R13
        jmp .done_divide_fraction
        
.only_fraction:
        mov rax, qword [rbp + 4*8]
        mov R13, rax
        NUMERATOR R13
        DENOMERATOR rax
        jmp .done_divide_fraction

.done_divide:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	

B_minus:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .first_fraction
	DATA rax
	cmp rcx, rbx
	je .one_arg_minus
	jmp .loop_minus
	
.first_fraction:
        mov rdx, rax
	NUMERATOR rax
	mov R13, rdx
	DENOMERATOR R13
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	jne .loop_fraction
	mov rbx, rax
	mov rax, -1
	mul rbx
	jmp .done_minus_fraction
	
.loop_minus:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_minus
	DATA rdx
	sub rax, rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_minus
	jmp .done_minus
	
.fractions_minus:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	sub rax, R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .sub_frac_to_frac

.sub_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mul R13
        mov rdx, rax
        pop rax
        sub rax, rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	jmp .loop_fraction
	
.sub_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R14, rax
	mov rax, R11
	mul R13
	mov R15, rax
	pop rax
	mul R12
	sub rax, R15
	mov R13, R14
	sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	jmp .loop_fraction
	
.done_minus_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
    

.one_arg_minus:
	mov rbx, 0
	sub rbx, rax
	mov rax, rbx

.done_minus:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	

B_plus:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 0
	mov rax, 0
	cmp rcx, rbx
	je .done_plus
	
.loop_plus:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_plus
	DATA rdx
	add rax, rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_plus
	jmp .done_plus
	
.fractions_plus:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	add rax, R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .add_frac_to_frac

.add_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mul R13
        mov rdx, rax
        pop rax
        add rax, rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	jmp .loop_fraction
	
.add_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R14, rax
	mov rax, R11
	mul R13
	mov R15, rax
	pop rax
	mul R12
	add rax, R15
	mov R13, R14
	sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	jmp .loop_fraction
        
	
	
.done_plus:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
.done_plus_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        cmp R13, 1
        je .denom_is_1
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
	
.denom_is_1:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	

B_equal:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov R14, qword [rbp + 3*8]
	
	cmp R14, 1
	je .equals
	
	mov rdi, 1
	mov rsi, 5
	
.loop:
        add rdi, 3
	
	mov rax, qword [rbp + rdi*8]
	mov rcx, rax
	TYPE rcx
	
	mov rbx, qword [rbp + rsi*8]
	mov rdx, rbx
	TYPE rdx
	
	cmp rcx, rdx
	jne .not_equals
	
	cmp rcx, T_FRACTION
	je .fraction
	
.integer:
	DATA rax
	DATA rbx
	
	cmp rax, rbx
	jne .not_equals
	
	inc rdi
	inc rsi
	
	sub rdi, 3
	cmp rdi, R14
	jne .loop
	jmp .equals
	
.fraction:
        mov rax, qword [rbp + rdi*8]
        mov rbx, qword [rbp + rsi*8]
        NUMERATOR rax
	NUMERATOR rbx
	
	cmp rax, rbx
	jne .not_equals
	
	mov rax, qword [rbp + rdi*8]
        mov rbx, qword [rbp + rsi*8]
        DENOMERATOR rax
	DENOMERATOR rbx
	
	cmp rax, rbx
	jne .not_equals
	
	inc rdi
	inc rsi
	
	sub rdi, 3
	cmp rdi, R14
	jne .loop
	
.equals:
	
	mov rax, SOB_TRUE
	leave
	ret

.not_equals:

	mov rax, SOB_FALSE	
	leave
	ret
	

B_greater:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	je .done_greater
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .fraction
	DATA rax
	
.loop_greater:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	TYPE rdx
	cmp rdx, T_FRACTION
	je .int_fraction
	
	.int_int:
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	mov rax, rdx
	cmp rcx, rbx
	jne .loop_greater
	jmp .done_greater
	
	.int_fraction:
	mov rdx, qword [rbp + rcx*8]
	DENOMERATOR rdx
	push rax
	mul rdx
	mov rdx, qword [rbp + rcx*8]
	NUMERATOR rdx
	cmp rax, rdx
	jle .not_greater
	pop rax
	mov rax, qword [rbp + rcx*8]
	sub rcx, 3
	cmp rcx, rbx
	jne .fraction
	
.done_greater:
	mov rax, SOB_TRUE
	leave
	ret
	
.fraction:
        mov R13, rax
        NUMERATOR rax
        DENOMERATOR R13
        
.loop_fraction:
        add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	TYPE rdx
	cmp rdx, T_FRACTION
	je .fraction_fraction
	
	.fraction_int:
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	push rax
	mov rax, R13
	mul rdx
	mov rdx, rax
	pop rax
	cmp rax, rdx
	jle .not_greater
	mov rax, qword [rbp + rcx*8]
	DATA rax
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_greater
	jmp .done_greater
	
	.fraction_fraction:
	mov rdx, qword [rbp + rcx*8]
	DENOMERATOR rdx
	push rax
	push R13
	mul rdx
	mov rdx, qword [rbp + rcx*8]
	NUMERATOR rdx
	push rax
	mov rax, R13
	mul rdx
	mov rdx, rax
	pop rax
	cmp rax, rdx
	jle .not_greater
    pop R13
	pop rax
	mov rax, qword [rbp + rcx*8]
	sub rcx, 3
	cmp rcx, rbx
	jne .fraction
	jmp .done_greater

.not_greater:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_boolean?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_BOOL
	jne .not_boolean
	
.done_boolean:
	mov rax, SOB_TRUE
	leave
	ret

.not_boolean:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_char?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_CHAR
	jne .not_char
	
.done_char:
	mov rax, SOB_TRUE
	leave
	ret

.not_char:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_integer?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .not_integer
	
.done_integer:
	mov rax, SOB_TRUE
	leave
	ret

.not_integer:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_string?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_STRING
	jne .not_string
	
.done_string:
	mov rax, SOB_TRUE
	leave
	ret

.not_string:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_pair?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_PAIR
	jne .not_regular_pair
	
.done_pair:
	mov rax, SOB_TRUE
	leave
	ret

.not_regular_pair:
	cmp rbx, T_PAIR_OPT
	jne .not_pair
	jmp .done_pair
	
.not_pair:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_vector?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_VECTOR
	jne .not_vector
	
.done_vector:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_vector:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_null?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_NIL
	jne .not_null
	
.done_null:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_null:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_zero?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	cmp rbx, MAKE_LITERAL(T_INTEGER, 0)
	jne .not_zero
	
.done_zero:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_zero:
	mov rax, SOB_FALSE	
	leave
	ret

B_symbol?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_SYMBOL
	jne .not_symbol
	
.done_symbol:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_symbol:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_procedure?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne .not_procedure
	
.done_procedure:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_procedure:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_number?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .not_regular_number
	
.done_number:
	mov rax, SOB_TRUE
	leave
	ret

.not_regular_number:
	cmp rbx, T_FRACTION
	jne .not_number
	jmp .done_number
	
.not_number:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_rational?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_FRACTION
	je .done_rational
	cmp rbx, T_INTEGER
	je .done_rational
	jmp .not_rational
	
.done_rational:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_rational:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_not:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	cmp rbx, SOB_FALSE
	je .done_true
	jmp .done_false
	
.done_true:
	mov rax, SOB_TRUE
	leave
	ret
	
.done_false:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_cons:
	push rbp
	mov rbp, rsp
	
	mov rdx, qword [rbp + 4*8]
	;;NEW_MALLOC 8, R8
	;;mov qword [R8], rdx
	mov R8, rdx
	mov rdx, qword [rbp + 5*8]
	;;NEW_MALLOC 8, R9
	;;mov qword [R9], rdx
	mov R9, rdx
	MAKE_LITERAL_PAIR_OPT R8, R9
	;;mov rax, qword [rax]
	test:
	leave
	ret
	

B_eq?:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rdx, qword [rbp + 4*8]
	TYPE rdx
	mov rcx, qword [rbp + 5*8]
	TYPE rcx
	cmp rcx, rdx
	jne .not_eq
	
	mov rdx, qword [rbp + 4*8]
	DATA rdx
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	cmp rcx, rdx
	jne .not_eq
	
.eq:
	mov rax, SOB_TRUE
	leave
	ret
	
;; .symbols:
;;         mov rdx, qword [rbp + 4*8]
;; 	STRING_LENGTH rdx
;; 	
;; 	mov rcx, qword [rbp + 5*8]
;; 	STRING_LENGTH rcx
;; 	
;; 	cmp rdx, rcx
;; 	jne .not_eq
;; 	
;; 	mov rdi, rcx
;; 	
;; 	mov rdx, qword [rbp + 4*8]
;; 	STRING_ELEMENTS rdx
;; 	
;; 	mov rcx, qword [rbp + 5*8]
;; 	STRING_ELEMENTS rcx
;; 	
;; .loop:
;;         dec rdi
;;         mov al, byte [rdx + rdi]
;; 	mov bl, byte [rcx + rdi]
;; 	cmp al, bl
;; 	jne .not_eq
;; 	cmp rdi, 0
;; 	jne .loop
;; 	jmp .eq

.not_eq:
	mov rax, SOB_FALSE	
	leave
	ret
	

B_string_to_symbol:
	push rbp
	mov rbp, rsp
	
	mov rdx, qword [rbp + 4*8]
	TYPE rdx
	cmp rdx, T_STRING
	jne ERROR_INVALID_ARGS
	
	mov R12, qword [rbp + 4*8]
	STRING_LENGTH R12
	mov R11, qword [rbp + 4*8]
	STRING_ELEMENTS R11
	
	mov R13, qword [symbol_table]
	
.loop:
	
	cmp R13, SOB_NIL
        je .not_found
        push R13
	CAR_OPT R13
	pop R13
	mov rdx, qword [rax]
	shr rdx, 4
	add rdx, start_of_data
	mov rdx, qword [rdx]
	
	mov rax, rdx
	STRING_LENGTH rax
	cmp rax, R12
	jne .next_iter
	
	mov R14, rdx
	STRING_ELEMENTS R14
	mov rcx, 0
	
	.compare_loop:
	mov rdx, 0
	mov rbx, 0
	mov dl, byte [R11 + rcx]
	mov bl, byte [R14 + rcx]
	cmp dl, bl
	jne .next_iter
	inc rcx
	cmp rcx, R12
	je .found
	jmp .compare_loop
	
	.next_iter:
	CDR_OPT R13
	mov R13, rax
	jmp .loop

	
.not_found:
        NEW_MALLOC 8, R15   ;;the string
        NEW_MALLOC R12, R14   ;;the elements
        
	mov rcx, 0
	
        .loop_copy:
        mov rdx, 0
	mov dl, byte [R11 + rcx]
	mov byte [R14 + rcx], dl
        inc rcx
        cmp rcx, R12
        jne .loop_copy

        mov rax, R12
        shl rax, 34
        sub R14, start_of_data
        shl R14, 4
        or rax, R14
        or rax, T_STRING
        mov qword [R15], rax
        
        NEW_MALLOC 8, R12   ;;the symbol
        
        sub R15, start_of_data
        shl R15, 4
        or R15, T_SYMBOL
        
        mov qword [R12], R15
        mov rdx, qword [symbol_table]
        MAKE_LITERAL_PAIR_OPT R12, rdx
        
        mov qword [symbol_table], rax
        
        mov rax, qword [R12]
        
        leave
        ret


.found:
        CAR_OPT R13
	mov rax, qword [rax]
        
	leave
	ret
	

B_symbol_to_string:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_SYMBOL
	jne ERROR_INVALID_ARGS
	
	mov rbx, qword [rbp + 4*8]
	
	mov rcx, qword [symbol_table]
	
.loop:

        cmp rcx, SOB_NIL
        je .not_found
        mov rdx, rcx
	CAR_OPT rdx
	mov rdx, qword [rax]
	cmp rdx, rbx
	je .found
	CDR_OPT rcx
	mov rcx, rax
	jmp .loop
	
.found:
	shr rdx, 4
	add rdx, start_of_data
	mov rax, qword [rdx]
	
	leave
	ret
	
.not_found:	
	
        jmp ERROR_NOT_MAKE_SENSE
	

B_char_to_integer:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_CHAR
	jne ERROR_INVALID_ARGS
	
	mov rbx, rax
	DATA rbx
	cmp rbx, 0
	jl ERROR_INVALID_ARGS
	cmp rbx, 127
	jg .patch
	
.done:
	shl rbx, 4
	or rbx, T_INTEGER
	mov rax, rbx
		
	leave
	ret
	
.patch:
        mov rcx, rbx
        and rcx, 255
        sub rcx, 194
        mov rax, 64
        mul rcx
        shr rbx, 8
        add rbx, rax
	jmp .done
        
	

B_integer_to_char:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne ERROR_INVALID_ARGS
	
	mov rbx, rax
	DATA rbx
	cmp rbx, 0
	jl ERROR_INVALID_ARGS
	cmp rbx, 256
	jge ERROR_INVALID_ARGS
	
	xor rax, (T_CHAR ^ T_INTEGER)
		
	leave
	ret
	

B_numerator:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	je .integer

.fraction:
	NUMERATOR rax
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	jmp .done
	
.integer:
	jmp .done
	
.done:
	leave
	ret
	

B_denominator:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	je .integer


.fraction:
	DENOMERATOR rax
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	jmp .done
	
.integer:
	mov rax, 1
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	jmp .done
	
.done:
	leave
	ret
	

B_set_car:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	mov rdx, qword [rbp + 5*8]
	TYPE rbx
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	jmp .done

.pair_opt:
        DATA rax
        mov qword [rax], rdx
	
.done:
        mov rax, SOB_VOID
	leave
	ret
	

B_set_cdr:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	mov rdx, qword [rbp + 5*8]
	TYPE rbx
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	jmp .done

.pair_opt:
        DATA rax
        mov qword [rax+8], rdx
	
.done:
        mov rax, SOB_VOID
	leave
	ret
	

B_vector:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 3*8]
	cmp rax, 0
	je .empty_vector
	
	mov rdx, 8
	mul rdx
	mov rcx, rax
	NEW_MALLOC rcx, rdx
	
	mov rcx, qword [rbp + 3*8]
	add rcx, 4
	mov rdi, 4
.loop:
        mov rax, qword [rbp + rdi*8]
        mov R10, 8
        NEW_MALLOC R10, R11
        mov qword [R11], rax
        sub rdi, 4
        mov qword [rdx + 8*rdi], R11
        add rdi, 5
        cmp rdi, rcx
        jne .loop
	
.done:
        mov rax, qword [rbp + 3*8]
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
.empty_vector:
        mov rax, qword [rbp + 3*8]
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	

B_vector_length:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	VECTOR_LENGTH rax
	shl rax, 4
	or rax, T_INTEGER
	
.done:
	leave
	ret
	

B_vector_ref:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	;VECTOR_REF rax, rbx, rcx
	
	mov rdx, rbx
	VECTOR_ELEMENTS rdx
	mov rax, rcx
	mov rbx, 8
	push rdx
	mul rbx
	pop rdx
	add rdx, rax
	mov rdx, qword [rdx]
	mov rdx, qword [rdx]
	
	mov rax, rdx
	
.done:
	leave
	ret
	

B_vector_set:
	push rbp
	mov rbp, rsp
	
	mov rax, 8
	NEW_MALLOC rax, R11
	mov rax, qword [rbp + 6*8]
	mov qword [R11], rax
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	VECTOR_ELEMENTS rbx
	push rbx
	mov rax, rcx
	mov rbx, 8
	mul rbx
	pop rbx
	add rbx, rax
	mov qword [rbx], R11
	
	mov rax, SOB_VOID
	
.done:
	leave
	ret
	

B_string_length:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	STRING_LENGTH rax
	shl rax, 4
	or rax, T_INTEGER
	
.done:
	leave
	ret
	

B_string_ref:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	mov rdx, 0
	push rax
	mov rax, rbx
	STRING_ELEMENTS rax
	add rax, rcx
	mov dl, byte [rax]
	pop rax
	
	mov rax, rdx
	shl rax, 4
	or rax, T_CHAR
	
.done:
	leave
	ret
	

B_string_set:
	push rbp
	mov rbp, rsp
	
	mov rdx, 0
	mov R11, qword [rbp + 6*8]
	DATA R11
	mov rdx, R11
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	
	push rax
	mov rax, rbx
	STRING_ELEMENTS rax
	add rax, rcx
	mov byte [rax], dl
	pop rax
	
	mov rax, SOB_VOID
	
.done:
	leave
	ret
	

B_make_string:
        push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	DATA rbx
        
	cmp rbx, 0
	je .empty_list
	
	NEW_MALLOC rbx, rdx
	
	mov rsi, qword [rbp + 3*8]
	
	cmp rsi, 1
	jne .regular_args
	
	mov rcx, 0
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
	jmp .loop
	
.regular_args:
        mov rcx, qword [rbp + 5*8]
        DATA rcx
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
.loop:
        mov byte [rdx + rdi], cl
        inc rdi
        dec rbx
        cmp rbx, 0
        jne .loop
	
        mov rax, qword [rbp + 4*8]
        DATA rax
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_STRING
        
	leave
	ret
	
.empty_list:
        mov rax, 0
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_STRING
        
	leave
	ret
	

B_make_vector:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	DATA rbx
        
	cmp rbx, 0
	je .empty_vector
	
	mov rax, rbx
	mov rdx, 8
	mul rdx
	mov rcx, rax
	NEW_MALLOC rcx, rdx
	
	mov rsi, qword [rbp + 3*8]
	
	cmp rsi, 1
	jne .regular_args
	
	mov rcx, MAKE_LITERAL(T_INTEGER, 0)
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
	jmp .loop
	
.regular_args:
        mov rcx, qword [rbp + 5*8]
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
.loop:
        mov R10, 8
        push rbx
        NEW_MALLOC R10, R11
        pop rbx
        mov qword [R11], rcx
        mov qword [rdx + 8*rdi], R11
        inc rdi
        dec rbx
        cmp rbx, 0
        jne .loop
	
        mov rax, qword [rbp + 4*8]
        DATA rax
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
.empty_vector:
        mov rax, 0
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	

L_main_is_ready:

	NEW_MALLOC 8, R12
	mov rdi, s2
	sub rdi, start_of_data
	shl rdi, 4
	or rdi, T_SYMBOL
	mov qword [R12], rdi
	mov rdi, SOB_NIL
	MAKE_LITERAL_PAIR_OPT R12, rdi
	NEW_MALLOC 8, R12
	mov rdi, s3
	sub rdi, start_of_data
	shl rdi, 4
	or rdi, T_SYMBOL
	mov qword [R12], rdi
	mov rdi, rax
	MAKE_LITERAL_PAIR_OPT R12, rdi
	NEW_MALLOC 8, R12
	mov rdi, s0
	sub rdi, start_of_data
	shl rdi, 4
	or rdi, T_SYMBOL
	mov qword [R12], rdi
	mov rdi, rax
	MAKE_LITERAL_PAIR_OPT R12, rdi
	NEW_MALLOC 8, R12
	mov rdi, s1
	sub rdi, start_of_data
	shl rdi, 4
	or rdi, T_SYMBOL
	mov qword [R12], rdi
	mov rdi, rax
	MAKE_LITERAL_PAIR_OPT R12, rdi
	mov qword [symbol_table], rax

        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_length
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g42], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_multiply
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g39], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_divide
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g40], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_minus
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g37], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_plus
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g38], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_equal
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g35], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_greater
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g36], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_boolean?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g33], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_char?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g34], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_integer?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g31], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_string?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g32], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_pair?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g29], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_vector?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g30], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_null?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g27], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_zero?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g28], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_symbol?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g25], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_procedure?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g26], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_number?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g23], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_rational?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g24], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_string_to_symbol
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g21], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_symbol_to_string
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g22], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_char_to_integer
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g19], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_integer_to_char
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g20], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_eq?
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g17], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_apply
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g18], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_not
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g15], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_cons
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g16], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_car
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g13], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_cdr
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g14], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_numerator
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g11], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_denominator
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g12], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_set_car
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g9], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_set_cdr
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g10], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_vector
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g7], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_vector_length
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g8], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_vector_ref
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g5], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_vector_set
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g6], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_string_length
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g3], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_make_vector
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g4], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_string_ref
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g1], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_string_set
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g2], rax
        
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_make_string
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [g0], rax
        	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV0:
	cmp rdx, 0
	je LOOP_ENV_END0
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_0.CONT
	EXTRA_ARGS_ON_STACK_0:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_0.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV0
	LOOP_ENV_END0:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_0.done
LOOP_FILL_ENV_0:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_0
.done:
	mov rbx, R15
	mov rdx, B0
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L0
B0:
	push rbp
	mov rbp, rsp
	GET_FROM_STACK rcx, 3
	sub rcx, 0
	cmp rcx, 0
	je NO_LOOP_FILL_ENV_OPT_0
	FILL_ENV_OPT_0:
	mov rax, rcx
	add rax, 3
	add rax, 0
	GET_FROM_STACK rdx, rax
	mov rdi, SOB_NIL
	MAKE_LITERAL_PAIR_OPT rdx, rdi
	dec rcx
	cmp rcx, 0
	je PUT_LIST_IN_ENV_0
LOOP_MAKE_PAIRS_0:
	push rax
	mov rax, rcx
	add rax, 4
	add rax, 0
	GET_FROM_STACK rdx, rax
	pop rax
	push rcx
	mov rcx, rax
	MAKE_LITERAL_PAIR_OPT rdx, rcx
	pop rcx
	dec rcx
	cmp rcx, 0
	jne LOOP_MAKE_PAIRS_0
	jmp PUT_LIST_IN_ENV_0
	NO_LOOP_FILL_ENV_OPT_0:
	NEW_MALLOC 8, rcx
	mov qword [rcx], SOB_NIL
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	mov rax, qword [rcx]
	mov qword [rdx + 8*0], rax
	jmp DDD0
PUT_LIST_IN_ENV_0:
	mov rdx, rbp
	add rdx, 32
	mov qword [rdx], rax
	DDD0:
	mov rax, qword [rbp + 4*8]
	leave
	ret
L0:

	mov qword [g49], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV1:
	cmp rdx, 0
	je LOOP_ENV_END1
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_1.CONT
	EXTRA_ARGS_ON_STACK_1:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_1.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV1
	LOOP_ENV_END1:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_1.done
LOOP_FILL_ENV_1:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_1
.done:
	mov rbx, R15
	mov rdx, B1
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L1
B1:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g27]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE0
	THEN0:
		mov rax, [c0]
	jmp END_IF0
	ELSE0:
		push SOB_NIL
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	mov rax, qword [rbp + 4*8]
	push rax
	push 2
	mov rax, qword [g50]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rax, qword [rbp + 4*8]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g16]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF0:

	leave
	ret
L1:

	mov qword [g50], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV2:
	cmp rdx, 0
	je LOOP_ENV_END2
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_2.CONT
	EXTRA_ARGS_ON_STACK_2:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_2.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV2
	LOOP_ENV_END2:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_2.done
LOOP_FILL_ENV_2:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_2
.done:
	mov rbx, R15
	mov rdx, B2
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L2
B2:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g27]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE1
	THEN1:
		mov rax, qword [rbp + 5*8]
	jmp END_IF1
	ELSE1:
		push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g47]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g16]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF1:

	leave
	ret
L2:

	mov qword [g47], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV3:
	cmp rdx, 0
	je LOOP_ENV_END3
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_3.CONT
	EXTRA_ARGS_ON_STACK_3:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_3.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV3
	LOOP_ENV_END3:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_3.done
LOOP_FILL_ENV_3:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_3
.done:
	mov rbx, R15
	mov rdx, B3
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L3
B3:
	push rbp
	mov rbp, rsp
	GET_FROM_STACK rcx, 3
	sub rcx, 1
	cmp rcx, 0
	je NO_LOOP_FILL_ENV_OPT_3
	FILL_ENV_OPT_3:
	mov rax, rcx
	add rax, 3
	add rax, 1
	GET_FROM_STACK rdx, rax
	mov rdi, SOB_NIL
	MAKE_LITERAL_PAIR_OPT rdx, rdi
	dec rcx
	cmp rcx, 0
	je PUT_LIST_IN_ENV_3
LOOP_MAKE_PAIRS_3:
	push rax
	mov rax, rcx
	add rax, 4
	add rax, 1
	GET_FROM_STACK rdx, rax
	pop rax
	push rcx
	mov rcx, rax
	MAKE_LITERAL_PAIR_OPT rdx, rcx
	pop rcx
	dec rcx
	cmp rcx, 0
	jne LOOP_MAKE_PAIRS_3
	jmp PUT_LIST_IN_ENV_3
	NO_LOOP_FILL_ENV_OPT_3:
	NEW_MALLOC 8, rcx
	mov qword [rcx], SOB_NIL
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	mov rax, qword [rcx]
	mov qword [rdx + 8*1], rax
	jmp DDD3
PUT_LIST_IN_ENV_3:
	mov rdx, rbp
	add rdx, 40
	mov qword [rdx], rax
	DDD3:
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g27]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE3
	THEN3:
		mov rax, [c1]
	jmp END_IF3
	ELSE3:
		push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	NEW_MALLOC 16, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV4:
	cmp rdx, 1
	je LOOP_ENV_END4
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_4.CONT
	EXTRA_ARGS_ON_STACK_4:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_4.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV4
	LOOP_ENV_END4:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_4.done
LOOP_FILL_ENV_4:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_4
.done:
	mov rbx, R15
	mov rdx, B4
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L4
B4:
	push rbp
	mov rbp, rsp
	mov rax, SOB_FALSE
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, qword [rbx + 0*8]
	push rax
	push 2
	mov rax, qword [g36]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	jne END_OR0
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, qword [rbx + 0*8]
	push rax
	push 2
	mov rax, qword [g35]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	jne END_OR0
	END_OR0:

	cmp rax, SOB_FALSE
	je ELSE2
	THEN2:
		mov rax, [c2]
	jmp END_IF2
	ELSE2:
		push SOB_NIL
	push SOB_NIL
	push SOB_NIL
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, qword [rbx + 1*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g49]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g46]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	mov rax, qword [g48]
	push rax
	push 2
	mov rax, qword [g18]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF2:

	leave
	ret
L4:

	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 1
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF3:

	leave
	ret
L3:

	mov qword [g48], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV5:
	cmp rdx, 0
	je LOOP_ENV_END5
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_5.CONT
	EXTRA_ARGS_ON_STACK_5:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_5.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV5
	LOOP_ENV_END5:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_5.done
LOOP_FILL_ENV_5:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_5
.done:
	mov rbx, R15
	mov rdx, B5
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L5
B5:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, [c3]
	push rax
	mov rax, qword [rbp + 4*8]
	push rax
	push 2
	mov rax, qword [g48]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE7
	THEN7:
		push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g37]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g45]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rax, qword [g37]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 1
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	jmp END_IF7
	ELSE7:
		push SOB_NIL
	mov rax, [c3]
	push rax
	mov rax, qword [rbp + 5*8]
	push rax
	push 2
	mov rax, qword [g48]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE6
	THEN6:
		push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g37]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	mov rax, qword [rbp + 4*8]
	push rax
	push 2
	mov rax, qword [g45]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	jmp END_IF6
	ELSE6:
		push SOB_NIL
	mov rax, [c4]
	push rax
	mov rax, qword [rbp + 5*8]
	push rax
	push 2
	mov rax, qword [g35]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE5
	THEN5:
		mov rax, [c3]
	jmp END_IF5
	ELSE5:
		push SOB_NIL
	mov rax, [c3]
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	mov rax, qword [rbp + 4*8]
	push rax
	push 2
	mov rax, qword [g37]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g48]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE4
	THEN4:
		mov rax, qword [rbp + 4*8]
	jmp END_IF4
	ELSE4:
		push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	mov rax, qword [rbp + 4*8]
	push rax
	push 2
	mov rax, qword [g37]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g45]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF4:

	END_IF5:

	END_IF6:

	END_IF7:

	leave
	ret
L5:

	mov qword [g45], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV6:
	cmp rdx, 0
	je LOOP_ENV_END6
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_6.CONT
	EXTRA_ARGS_ON_STACK_6:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_6.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV6
	LOOP_ENV_END6:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_6.done
LOOP_FILL_ENV_6:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_6
.done:
	mov rbx, R15
	mov rdx, B6
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L6
B6:
	push rbp
	mov rbp, rsp
	GET_FROM_STACK rcx, 3
	sub rcx, 0
	cmp rcx, 0
	je NO_LOOP_FILL_ENV_OPT_6
	FILL_ENV_OPT_6:
	mov rax, rcx
	add rax, 3
	add rax, 0
	GET_FROM_STACK rdx, rax
	mov rdi, SOB_NIL
	MAKE_LITERAL_PAIR_OPT rdx, rdi
	dec rcx
	cmp rcx, 0
	je PUT_LIST_IN_ENV_6
LOOP_MAKE_PAIRS_6:
	push rax
	mov rax, rcx
	add rax, 4
	add rax, 0
	GET_FROM_STACK rdx, rax
	pop rax
	push rcx
	mov rcx, rax
	MAKE_LITERAL_PAIR_OPT rdx, rcx
	pop rcx
	dec rcx
	cmp rcx, 0
	jne LOOP_MAKE_PAIRS_6
	jmp PUT_LIST_IN_ENV_6
	NO_LOOP_FILL_ENV_OPT_6:
	NEW_MALLOC 8, rcx
	mov qword [rcx], SOB_NIL
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	mov rax, qword [rcx]
	mov qword [rdx + 8*0], rax
	jmp DDD6
PUT_LIST_IN_ENV_6:
	mov rdx, rbp
	add rdx, 32
	mov qword [rdx], rax
	DDD6:
	push SOB_NIL
	mov rax, [c2]
	push rax
	mov rax, [c2]
	push rax
	push 2
	NEW_MALLOC 16, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV7:
	cmp rdx, 1
	je LOOP_ENV_END7
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_7.CONT
	EXTRA_ARGS_ON_STACK_7:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_7.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV7
	LOOP_ENV_END7:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_7.done
LOOP_FILL_ENV_7:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_7
.done:
	mov rbx, R15
	mov rdx, B7
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L7
B7:
	push rbp
	mov rbp, rsp
	mov rax, qword [rbp + 4*8]
	NEW_MALLOC 8, rdi
	mov qword [rdi], rax
	mov rax, rdi
	mov rdx, rax
	mov rax, rbp
	add rax, 32
	mov qword [rax], rdx
	mov rax, SOB_VOID
	mov rax, qword [rbp + 5*8]
	NEW_MALLOC 8, rdi
	mov qword [rdi], rax
	mov rax, rdi
	mov rdx, rax
	mov rax, rbp
	add rax, 40
	mov qword [rax], rdx
	mov rax, SOB_VOID
	NEW_MALLOC 24, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV8:
	cmp rdx, 2
	je LOOP_ENV_END8
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_8.CONT
	EXTRA_ARGS_ON_STACK_8:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_8.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV8
	LOOP_ENV_END8:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_8.done
LOOP_FILL_ENV_8:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_8
.done:
	mov rbx, R15
	mov rdx, B8
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L8
B8:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g27]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE8
	THEN8:
		mov rax, qword [rbp + 5*8]
	jmp END_IF8
	ELSE8:
		push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, rbx
	mov rbx, 0
	add rax, rbx
	mov rax, qword [rax]
	mov rax, qword [rax]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g16]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF8:

	leave
	ret
L8:

	mov rdx, rax
	mov rax, rbp
	add rax, 32
	mov rax, qword [rax]
	mov qword [rax], rdx
	mov rax, SOB_VOID
	NEW_MALLOC 24, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV9:
	cmp rdx, 2
	je LOOP_ENV_END9
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_9.CONT
	EXTRA_ARGS_ON_STACK_9:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_9.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV9
	LOOP_ENV_END9:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_9.done
LOOP_FILL_ENV_9:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_9
.done:
	mov rbx, R15
	mov rdx, B9
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L9
B9:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g27]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE10
	THEN10:
		mov rax, qword [rbp + 4*8]
	jmp END_IF10
	ELSE10:
		push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rax, qword [g29]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE9
	THEN9:
		push SOB_NIL
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, rbx
	mov rbx, 8
	add rax, rbx
	mov rax, qword [rax]
	mov rax, qword [rax]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, rbx
	mov rbx, 0
	add rax, rbx
	mov rax, qword [rax]
	mov rax, qword [rax]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	jmp END_IF9
	ELSE9:
		push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 1
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF9:

	END_IF10:

	leave
	ret
L9:

	mov rdx, rax
	mov rax, rbp
	add rax, 40
	mov rax, qword [rax]
	mov qword [rax], rdx
	mov rax, SOB_VOID
	push SOB_NIL
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, qword [rbx + 0*8]
	push rax
	push 1
	mov rax, rbp
	add rax, 40
	mov rax, qword [rax]
	mov rax, qword [rax]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 1
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	leave
	ret
L7:

	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	leave
	ret
L6:

	mov qword [g46], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV10:
	cmp rdx, 0
	je LOOP_ENV_END10
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_10.CONT
	EXTRA_ARGS_ON_STACK_10:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_10.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV10
	LOOP_ENV_END10:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_10.done
LOOP_FILL_ENV_10:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_10
.done:
	mov rbx, R15
	mov rdx, B10
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L10
B10:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g27]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE11
	THEN11:
		mov rax, [c0]
	jmp END_IF11
	ELSE11:
		push SOB_NIL
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rax, qword [g49]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rax, qword [g43]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g47]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF11:

	leave
	ret
L10:

	mov qword [g43], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV11:
	cmp rdx, 0
	je LOOP_ENV_END11
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_11.CONT
	EXTRA_ARGS_ON_STACK_11:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_11.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV11
	LOOP_ENV_END11:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_11.done
LOOP_FILL_ENV_11:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_11
.done:
	mov rbx, R15
	mov rdx, B11
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L11
B11:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g29]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE12
	THEN12:
		mov rax, SOB_FALSE
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g13]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	mov rax, qword [rbp + 4*8]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	jne END_OR1
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 5*8]
	push rax
	push 1
	mov rax, qword [g14]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	mov rax, qword [rbp + 4*8]
	push rax
	push 2
	mov rax, qword [g44]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	cmp rax, SOB_FALSE
	jne END_OR1
	END_OR1:

	jmp END_IF12
	ELSE12:
		mov rax, [c2]
	END_IF12:

	leave
	ret
L11:

	mov qword [g44], rax
mov rax, SOB_VOID
	NEW_MALLOC 8, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV12:
	cmp rdx, 0
	je LOOP_ENV_END12
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_12.CONT
	EXTRA_ARGS_ON_STACK_12:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_12.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV12
	LOOP_ENV_END12:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_12.done
LOOP_FILL_ENV_12:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_12
.done:
	mov rbx, R15
	mov rdx, B12
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L12
B12:
	push rbp
	mov rbp, rsp
	GET_FROM_STACK rcx, 3
	sub rcx, 2
	cmp rcx, 0
	je NO_LOOP_FILL_ENV_OPT_12
	FILL_ENV_OPT_12:
	mov rax, rcx
	add rax, 3
	add rax, 2
	GET_FROM_STACK rdx, rax
	mov rdi, SOB_NIL
	MAKE_LITERAL_PAIR_OPT rdx, rdi
	dec rcx
	cmp rcx, 0
	je PUT_LIST_IN_ENV_12
LOOP_MAKE_PAIRS_12:
	push rax
	mov rax, rcx
	add rax, 4
	add rax, 2
	GET_FROM_STACK rdx, rax
	pop rax
	push rcx
	mov rcx, rax
	MAKE_LITERAL_PAIR_OPT rdx, rcx
	pop rcx
	dec rcx
	cmp rcx, 0
	jne LOOP_MAKE_PAIRS_12
	jmp PUT_LIST_IN_ENV_12
	NO_LOOP_FILL_ENV_OPT_12:
	NEW_MALLOC 8, rcx
	mov qword [rcx], SOB_NIL
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	mov rax, qword [rcx]
	mov qword [rdx + 8*2], rax
	jmp DDD12
PUT_LIST_IN_ENV_12:
	mov rdx, rbp
	add rdx, 48
	mov qword [rdx], rax
	DDD12:
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 6*8]
	push rax
	mov rax, qword [rbp + 5*8]
	push rax
	push 2
	mov rax, qword [g16]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 1
	NEW_MALLOC 16, rbx
	push rbx
	mov rax, qword [rbp + 3*8]
	mov rbx, 8
	mul rbx
	mov rbx, rax
	NEW_MALLOC rbx, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov qword [rbx], rcx
	mov rdx, 0
	LOOP_ENV13:
	cmp rdx, 1
	je LOOP_ENV_END13
	mov rdi, qword [rbp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_13.CONT
	EXTRA_ARGS_ON_STACK_13:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
	EXTRA_ARGS_ON_STACK_13.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV13
	LOOP_ENV_END13:
	mov R15, rbx
	mov rdi, qword [rbp + 3*8]
	mov rcx, 0
	cmp rcx, rdi 
	je LOOP_FILL_ENV_13.done
LOOP_FILL_ENV_13:
	add rcx, 4
	mov rdx, qword [rbx]
	mov rsi, qword [rbp + rcx*8]
	sub rcx, 4
	mov qword [rdx+rcx*8], rsi
	inc rcx
	cmp rcx, rdi
	jne LOOP_FILL_ENV_13
.done:
	mov rbx, R15
	mov rdx, B13
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L13
B13:
	push rbp
	mov rbp, rsp
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	mov rax, qword [g27]
	push rax
	push 2
	mov rax, qword [g44]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	cmp rax, SOB_FALSE
	je ELSE13
	THEN13:
		mov rax, [c0]
	jmp END_IF13
	ELSE13:
		push SOB_NIL
	push SOB_NIL
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	mov rax, qword [g14]
	push rax
	push 2
	mov rax, qword [g50]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, qword [rbx + 0*8]
	push rax
	push 1
	mov rax, qword [g49]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g47]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	mov rax, qword [g41]
	push rax
	push 2
	mov rax, qword [g18]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push SOB_NIL
	push SOB_NIL
	mov rax, qword [rbp + 4*8]
	push rax
	mov rax, qword [g13]
	push rax
	push 2
	mov rax, qword [g50]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx + 0*8]
	mov rax, qword [rbx + 0*8]
	push rax
	push 2
	mov rax, qword [g18]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10
	push rax
	push 2
	mov rax, qword [g16]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 2
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	END_IF13:

	leave
	ret
L13:

	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	mov rdx, qword [rbp+3*8]
	add rdx, 5
	mov rcx, 1
	add rcx, 3
	mov rdi, rbp
	sub rdi, 8
	mov R8, qword [rbp + 1*8]
	mov R9, qword [rbp + 0*8]
	mov R10, rax
.COPY:
	COPY_STACK rdi, rdx
	sub rdi, 8
	dec rcx
	cmp rcx, 0
	jne .COPY
	mov rax, rdx
	inc rax
	mov rcx, 8
	mul rcx
	add rdi, rax
	mov rsp, rdi
	push R8
	mov rbp, R9
	mov rax, R10
	jmp rax
	leave
	ret
L12:

	mov qword [g41], rax
mov rax, SOB_VOID

        mov rax, qword [g43]
        mov qword [my_very_own_reverse], rax
        	push SOB_NIL
	mov rax, [c8]
	push rax
	mov rax, [c6]
	push rax
	mov rax, [c7]
	push rax
	mov rax, [c5]
	push rax
	mov rax, [c4]
	push rax
	push 5
	mov rax, qword [g48]
	push rax
	TYPE rax
	cmp rax, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	pop rax
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	CLOSURE_CODE rax
	call rax
	mov R10, rax
	add rsp, 8*1
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8*1
	mov rax, R10


    push rax
    call write_sob_if_not_void
    add rsp, 1*8
    
    

    pop    rbp
        
    push 0
    call exit
    
error_apply_non_closure_text:
    dq "Exception: attempt to apply non procedure", CHAR_NEWLINE, 0
	
error_variable_not_bound_text:
    dq "Exception: variable _ is not bound", CHAR_NEWLINE, 0
	
error_invalid_args:
    dq "Exception: invalid args to proc", CHAR_NEWLINE, 0
    
error_not_make_sense:
    dq "Exception: something doesnt make sense!", CHAR_NEWLINE, 0
	
	
ERROR_NOT_MAKE_SENSE:
    PRINTFF error_not_make_sense
    push -1
    call exit
	
ERROR_INVALID_ARGS:
    PRINTFF error_invalid_args
    push -1
    call exit
	
ERROR_VARIABLE_NOT_BOUND:
    PRINTFF error_variable_not_bound_text
    push -1
    call exit

ERROR_APPLY_NON_CLOSURE:
    PRINTFF error_apply_non_closure_text
    push -1
    call exit

