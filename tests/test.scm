;(map + '())

;(append '(1 2) '(5 6) '(a v x) (list #t (apply list '(1 2 3))))

;(apply + (list 1 2 (apply - '(9 4 2)) 4))

;(apply + '(1 2 3))

;(reverse '(1 2))

;(eq? (string->symbol "a") 'a)

;(make-string 9)

;(map number? '(1 2 3))

;(remainder -12 5)

;((lambda (ch) (if (char? ch) (char->integer ch))) #\٦)

;(char->integer #\x666)

;(integer->char 127)

;(set-cdr! '(1 2 3) (cdr '(1 2 3)))

;'(1 . 2)

;'(1 . (2 . (3 . 4)))

;(map (lambda (x) x) '(-1 -2 -3/5))l

;"\""

;; (define x '(1 2))
;; (define y '(3 4))
;; (define z (append x y))
;; (set-car! x '*)
;; (set-car! y '$)
;; z

;; (define with (lambda (s f) (apply f s)))
;; 



;; ((lambda (x) ((x x) (x x)))
;;     (lambda (x)
;;       (lambda (y)
;;             (x (x y)))))

;((lambda (x . s) 1) 2)


;; (define with (lambda (s f) (apply f s)))
;; (define list (lambda args args))
;; (define fact-1
;;   (lambda (n)
;;     (if (zero? n)
;; 	(list 1 'fact-1)
;; 	(with (fact-2 (- n 1))
;; 	  (lambda (r . trail)
;; 	    (cons (* n r)
;; 	      (cons 'fact-1 trail)))))))
;; (define fact-2
;;   (lambda (n)
;;     (if (zero? n)
;; 	(list 1 'fact-2)
;; 	(with (fact-3 (- n 1))
;; 	  (lambda (r . trail)
;; 	    (cons (* n r)
;; 	      (cons 'fact-2 trail)))))))
;; (define fact-3
;;   (lambda (n)
;;     (if (zero? n)
;; 	(list 1 'fact-3)
;; 	(with (fact-1 (- n 1))
;; 	  (lambda (r . trail)
;; 	    (cons (* n r)
;; 	      (cons 'fact-3 trail)))))))
;; (fact-1 10)

;(lambda (x) '(a b c))

;; (eq? (string->symbol "abc") 'abc) ; #t   
;; 
;;(eq? (symbol->string 'abc) "abc") ; #f
;; 
;; (eq? (string->symbol (symbol->string 'abc)) 'abc) ;#t
;; 
;; (eq? (symbol->string (string->symbol "abc")) "abc") ;#f
;; 
;; (eq? (symbol->string 'abc) (symbol->string 'abc)) ;#t
;; 
;; (eq? (string->symbol "abc") (string->symbol "abc")) ;#t

;;(vector-length '#(vector? vector-ref))!#

;(map (lambda (x) x) '(-1 -2 -3/5))

;; (string-set! (symbol->string 'sss) 1 #\b)
;; 
;; (symbol->string 'sss)

(< 1 2 5/2 5 4)
