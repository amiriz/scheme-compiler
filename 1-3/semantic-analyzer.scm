(define empty?
    (lambda (lst)
        (and (list? lst) (= (length lst) 0))))

(define is-applic-lambda-nil?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'applic) (equal? (caadr sexpr) 'lambda-simple))
            (let* ((lambd (cadr sexpr))
                   (args (list-ref lambd 1))
                   (params (list-ref sexpr 2)))
                (and (empty? args) (empty? params)))
            #f)))
                
(define removal 
    (lambda (app-expr)
        (let* ((lambd (cadr app-expr))
               (body (list-ref lambd 2)))
              (remove-applic-lambda-nil body))))

;;;DO NOT DELETE              
(define remove-applic-lambda-nil
    (lambda (sexpr)
		;(display (car sexpr))
		;(newline)
        (if (is-applic-lambda-nil? sexpr)
            (removal sexpr)
            (if (not (list? sexpr))
                sexpr
                (let ((tag (car sexpr)))
                    (cond ((equal? tag 'if3) ((lambda () (remove-if-sexpr sexpr))))
                        ((equal? tag 'or) ((lambda () (remove-or-sexpr sexpr))))
                        ((equal? tag 'lambda-simple) ((lambda () (remove-lambda-simple-sexpr sexpr))))
                        ((equal? tag 'lambda-opt) ((lambda () (remove-lambda-opt-sexpr sexpr))))
                        ((equal? tag 'define) ((lambda () (remove-define-sexpr sexpr))))
                        ((equal? tag 'set) ((lambda () (remove-set-sexpr sexpr))))
                        ((equal? tag 'applic) ((lambda () (remove-applic-sexpr sexpr))))
                        ((equal? tag 'seq) ((lambda () (remove-seq-sexpr sexpr))))
                        (else sexpr)))))))
;;                        
(define remove-seq-sexpr
    (lambda (seq-expr)
        (let* ((exprs (list-ref seq-expr 1))
               (removed-exprs (map remove-applic-lambda-nil exprs)))
              `(seq ,removed-exprs))))
                       
(define remove-applic-sexpr
    (lambda (applic-expr)
        (let* ((closure (list-ref applic-expr 1))
               (removed-closure (remove-applic-lambda-nil closure))
               (exprs (list-ref applic-expr 2))
               (removed-exprs (map remove-applic-lambda-nil exprs)))
              `(applic ,removed-closure ,removed-exprs))))
                       
(define remove-set-sexpr
    (lambda (set-expr)
        (let ((name (list-ref set-expr 1))
              (expr (list-ref set-expr 2)))
             (list 'set name (remove-applic-lambda-nil expr)))))
                       
(define remove-define-sexpr
    (lambda (define-expr)
        (let ((name (list-ref define-expr 1))
              (expr (list-ref define-expr 2)))
             (list 'define name (remove-applic-lambda-nil expr)))))
        

(define remove-lambda-opt-sexpr
    (lambda (lambda-simple-expr)
        (let ((args (list-ref lambda-simple-expr 1))
              (rest (list-ref lambda-simple-expr 2))
              (body (list-ref lambda-simple-expr 3)))
             (list 'lambda-opt args rest (remove-applic-lambda-nil body)))))                            
                       
(define remove-lambda-simple-sexpr
    (lambda (lambda-simple-expr)
        (let ((args (list-ref lambda-simple-expr 1))
              (body (list-ref lambda-simple-expr 2)))
             (list 'lambda-simple args (remove-applic-lambda-nil body)))))                      
                       
(define remove-or-sexpr
    (lambda (or-expr)
        (let ((exprs (cadr or-expr)))
            (list 'or (map remove-applic-lambda-nil exprs)))))
              
(define remove-if-sexpr
    (lambda (if-expr)
        (let* ((test (list-ref if-expr 1))
               (then (list-ref if-expr 2))
               (else (list-ref if-expr 3)))
              (list 'if3 (remove-applic-lambda-nil test) (remove-applic-lambda-nil then) (remove-applic-lambda-nil else)))))

(define dedupe  
    (lambda (e)
        (if (null? e) '()
            (cons (car e) (dedupe (filter (lambda (x) (not (equal? x (car e)))) 
                                    (cdr e)))))))
(define vars-extractor
    (lambda (body vars)
        (let ((vars-with-dups (if (or (not (list? body)) (empty? body))
                                    '()
                                    (let* ((tag (car body)))
                                        (cond 
                                            ((equal? tag 'var) (append vars (list (list-ref body 1))))
                                            (else (fold-left
                                                    (lambda (acc expr)
                                                        (append acc (vars-extractor expr '())))
                                                    vars body)))))))
        (dedupe vars-with-dups))))

(define get-set-finder-var
    (lambda (var expr)
        (if (and (get-find-var var expr) (set-find-var var expr))
            (list var)
            '())))
        
(define get-set-finder
    (lambda (body bounded-vars)
        (fold-left 
            (lambda (acc bounded-var) (append acc (get-set-finder-var bounded-var body)))
            '()
            bounded-vars)))

(define (set-difference s1 s2)
  (cond ((null? s1)
         '())
        ((not (member (car s1) s2))
         (cons (car s1) (set-difference (cdr s1) s2)))
        (else
         (set-difference (cdr s1) s2))))            
            
(define bound-finder
    (lambda (pe bounded)
        (let* ((tag (car pe)))
                (cond 
                  ((equal? tag 'lambda-simple)
                    (let* ((args (list-ref pe 1))
                           (body (list-ref pe 2))
                           (boundedVars (vars-extractor body '())))
                           boundedVars))
                  ((equal? tag 'lambda-opt)
                    (let* ((args (list-ref pe 1))
                           (rest (list-ref pe 2))
                           (body (list-ref pe 3))
                           (boundedVars (set-difference (vars-extractor body '()) args)))
                           boundedVars))))))              

              
(define do-box
    (lambda (body var-to-box)
        body))
        
(define get-vars-to-box
    (lambda (expr)
        (if (or (equal? (car expr) 'lambda-simple) (equal? (car expr) 'lambda-opt))
            (let* ((argList (list-ref expr 1))
                   (boundedVars (bound-finder expr argList))
                   (body (list-ref expr 2)))
                  (get-set-finder body boundedVars))
            '())))
              
(define list-index
        (lambda (e lst)
                (if (null? lst)
                        -1
                        (if (equal? (car lst) e)
                                0
                                (if (= (list-index e (cdr lst)) -1) 
                                        -1
                                        (+ 1 (list-index e (cdr lst))))))))
  
(define (contains? l i)
  (if (empty? l) #f
      (or (equal? (car l) i) (contains? (cdr l) i))))
  
(define replace-vars
    (lambda (var vars count)
        (if (empty? vars)
            `(fvar ,var)
            (let ((current (car vars)))
                (cond 
                    ((and (contains? current var) (= count 0))
                        (let ((index (list-index var current)))
                            `(pvar ,var ,index)))
                    ((contains? current var)
                        (let ((index (list-index var current))
                              (major (- count 1)))
                            `(bvar ,var ,major ,index)))
                    (else (replace-vars var (cdr vars) (+ count 1))))))))
  
(define real-pe->lex-pe
    (lambda (parsed-expr vars)
        (if (or (not (list? parsed-expr)) (empty? parsed-expr))
            parsed-expr
            (let ((tag (car parsed-expr)))
                (cond
                    ((equal? tag 'var) (replace-vars (cadr parsed-expr) vars 0))
                    ((equal? tag 'lambda-simple) 
                        (let* ((tag (list-ref parsed-expr 0))
                               (args (list-ref parsed-expr 1))
                               (body (list-ref parsed-expr 2))
                               (args-listed `(,args))
                               (new-vars (append args-listed vars)))
                            (list tag args (real-pe->lex-pe body new-vars))))
                    ((equal? tag 'lambda-opt)
                        (if (= (length parsed-expr) 2)
                            parsed-expr
                            (let* ((tag (list-ref parsed-expr 0))
                                (args (list-ref parsed-expr 1))
                                (rest (list-ref parsed-expr 2))
                                (body (list-ref parsed-expr 3))
                                (args-and-rest `(,@args ,rest))
                                (args-listed `(,args-and-rest))
                                (new-vars (append args-listed vars)))
                                (list tag args rest (real-pe->lex-pe body new-vars)))))
                    (else (map (lambda (expr) (real-pe->lex-pe expr vars)) parsed-expr)))))))

(define replace-applic
    (lambda (applic-expr tp)
        (let* ((function (list-ref applic-expr 1))
                (exprs (list-ref applic-expr 2))
                (new-function (annotate function #f))
                (new-exprs (map (lambda (x) (annotate x #f)) exprs)))
            (if tp
                `(tc-applic ,new-function ,new-exprs)
                `(applic ,new-function ,new-exprs)))))
             
(define applic-expr?
    (lambda (expr)
        (if (equal? (car expr) 'applic)
            #t
            #f)))
            
(define last
    (lambda (list)
        (let ((len (length list)))
            (list-ref list (- len 1)))))
            
(define all-but-last 
    (lambda (l) (reverse (cdr (reverse l)))))
                    
(define annotate
    (lambda (pe tp)
        (if (not (list? pe))
            pe
            (let ((tag (car pe)))
                    (cond ((equal? tag 'if3) ((lambda () (replace-if-applic pe tp))))
                        ((equal? tag 'or) ((lambda () (replace-or-applic pe tp))))
                        ((equal? tag 'seq) ((lambda () (replace-seq-applic pe tp))))
                        ((equal? tag 'lambda-simple) ((lambda () (replace-lambda-simple-applic pe tp))))
                        ((equal? tag 'lambda-opt) ((lambda () (replace-lambda-opt-applic pe tp))))
                        ((or (equal? tag 'define) (equal? tag 'set) (equal? tag 'box-set)) ((lambda () (replace-define-set-applic pe tp))))
                        ((equal? tag 'applic) ((lambda () (replace-applic-applic pe tp))))
                        (else pe))))))

(define replace-tc-applic-applic
    (lambda (pe tp)
        (let* ((closure (list-ref pe 1))
               (exprs (list-ref pe 2))
               (new-closure (annotate closure tp))
               (new-exprs (map (lambda (x) (annotate x tp)) exprs)))
              `(tc-applic ,new-closure ,new-exprs))))                    
                    
(define replace-applic-applic
    (lambda (pe tp)
        (let* ((closure (list-ref pe 1))
               (exprs (list-ref pe 2))
               (new-closure (annotate closure tp))
               (new-exprs (map (lambda (x) (annotate x tp)) exprs)))
              `(applic ,new-closure ,new-exprs))))
        
(define replace-define-set-applic
    (lambda (pe tp)
        (let* ((tag (list-ref pe 0))
               (name (list-ref pe 1))
               (expr (list-ref pe 2))
               (new-expr (annotate expr tp)))
             `(,tag ,name ,new-expr))))
                                      
                
(define replace-lambda-opt-applic
    (lambda (pe tp)
         (let* ((args (list-ref pe 1))
                (rest (list-ref pe 2))
                (body (list-ref pe 3))
                (new-body (if (applic-expr? body)
                              (replace-applic body #t)
                              (annotate body #t))))
             `(lambda-opt ,args ,rest ,new-body))))

(define replace-lambda-simple-applic
    (lambda (pe tp)
        (let* ((args (list-ref pe 1))
               (body (list-ref pe 2))
               (new-body (if (applic-expr? body)
                             (replace-applic body #t)
                             (annotate body #t))))
             `(lambda-simple ,args ,new-body))))
                   
                    
(define replace-or-applic
    (lambda (pe tp)
        (let*  ((exprs (car (cdr pe)))
                (final (last exprs))
                (rest (all-but-last exprs))
                (new-final (if (applic-expr? final)
                                (replace-applic final tp)
                                (annotate final tp)))
                (new-rest (map (lambda (x) (annotate x #f)) rest)))
            (list 'or (append new-rest (list new-final))))))
            
(define replace-seq-applic
    (lambda (pe tp)
        (let* ((exprs (car (cdr pe)))
               (final (last exprs))
               (rest (all-but-last exprs))
               (new-final (if (applic-expr? final)
                              (replace-applic final tp)
                              (annotate final tp)))
               (new-rest (map (lambda (x) (annotate x #f)) rest)))
               (list 'seq (append new-rest (list new-final))))))
                
(define replace-if-applic
    (lambda (pe tp)
        (let* ((test (list-ref pe 1))
               (then (list-ref pe 2))
               (else (list-ref pe 3))
               (new-test (annotate test #f))
               (new-then (if (applic-expr? then)
                             (replace-applic then tp)
                             (annotate then tp)))
               (new-else (if (applic-expr? else)
                             (replace-applic else tp)
                             (annotate else tp))))
            `(if3 ,new-test ,new-then ,new-else))))
               
;;;DO NOT DELETE    
(define pe->lex-pe
  (lambda (parsed-expr)
     (real-pe->lex-pe parsed-expr '())))

;;;DO NOT DELETE    
(define annotate-tc
    (lambda (parsed-expr)
            (annotate parsed-expr #f)))

  
  
(define full-test
    (lambda (expr)
        (annotate-tc (pe->lex-pe (remove-applic-lambda-nil (parse expr))))))  



(define set?
    (lambda (var expr)
        (if (or (not (list? expr)) (empty? expr))
            #f
            (let* ((tag (car expr)))
                (cond ((equal? tag 'set)
                        (let* ((var-name (list-ref (list-ref expr 1) 1)))
                            (if (equal? var var-name)
                                #t
                                (fold-left
                                    (lambda (acc exp) (or acc (set? var exp)))
                                    #f
                                    expr))))
                      ((equal? tag 'lambda-simple)
                            (let* ((args (list-ref expr 1))
                                   (body (list-ref expr 2)))
                                (if (contains? args var)
                                    #f
                                    (fold-left
                                        (lambda (acc exp) (or acc (set? var exp)))
                                        #f
                                        (list body)))))
                        ((and (equal? tag 'lambda-opt) (> (length expr) 2))
                            (let* ((args (append (list-ref expr 1) (list (list-ref expr 2))))
                                   (body (list-ref expr 3)))
                                (if (contains? args var)
                                    #f
                                    (fold-left
                                        (lambda (acc exp) (or acc (set? var exp)))
                                        #f
                                        (list body)))))              
                      (else (fold-left
                                    (lambda (acc exp) (or acc (set? var exp)))
                                    #f
                                    expr)))))))
                                
                                
        
(define get?
    (lambda (var expr)
        (if (or (not (list? expr)) (empty? expr))
            #f
            (let* ((tag (car expr)))
                (cond ((equal? tag 'var)
                        (let* ((var-name (list-ref expr 1)))
                            (if (equal? var var-name)
                                #t
                                (fold-left
                                    (lambda (acc exp) (or acc (get? var exp)))
                                    #f
                                    expr))))
                      ((equal? tag 'set)
                        (let* ((exprs-to-search (list (list-ref expr 2))))
                            (fold-left
                                    (lambda (acc exp) (or acc (get? var exp)))
                                    #f
                                    exprs-to-search)))
                      ((equal? tag 'lambda-simple)
                            (let* ((args (list-ref expr 1))
                                   (body (list-ref expr 2)))
                                (if (contains? args var)
                                    #f
                                    (fold-left
                                        (lambda (acc exp) (or acc (get? var exp)))
                                        #f
                                        (list body)))))
                      ((and (equal? tag 'lambda-opt) (> (length expr) 2))
                            (let* ((args (append (list-ref expr 1) (list (list-ref expr 2))))
                                   (body (list-ref expr 3)))
                                (if (contains? args var)
                                    #f
                                    (fold-left
                                        (lambda (acc exp) (or acc (get? var exp)))
                                        #f
                                        (list body)))))             
                      (else (fold-left
                                    (lambda (acc exp) (or acc (get? var exp)))
                                    #f
                                    expr)))))))
                                    
(define bound2?
    (lambda (var code)
        (if (or (not (list? code)) (empty? code))
            #f
            (let* ((tag (car code)))
                  (cond ((equal? tag 'lambda-simple)
                            (let* ((args (list-ref code 1))
                                   (body (list-ref code 2)))
                                (if (contains? args var)
                                    #f
                                    (fold-left
                                        (lambda (acc exp) (or acc (bound2? var exp)))
                                        #f
                                        (list body)))))
                        ((and (equal? tag 'lambda-opt) (> (length code) 2))
                            (let* ((args (append (list-ref code 1) (list (list-ref code 2))))
                                   (body (list-ref code 3)))
                                (if (contains? args var)
                                    #f
                                    (fold-left
                                        (lambda (acc exp) (or acc (bound2? var exp)))
                                        #f
                                        (list body)))))
                        ((equal? tag 'var)
                         (let* ((var-name (list-ref code 1)))
                            (if (equal? var-name var)
                                #t
                                #f)))
                        (else (fold-left
                                    (lambda (acc exp) (or acc (bound2? var exp)))
                                    #f
                                    code)))))))
   
(define bound?
    (lambda (var code)
        (if (or (not (list? code)) (empty? code))
            #f
            (let* ((tag (car code)))
                (cond ((equal? tag 'lambda-simple)
                        (let* ((args (list-ref code 1))
                               (body (list-ref code 2)))

                            (if (contains? args var)
                                #f
                                (fold-left
                                        (lambda (acc exp) (or acc (bound2? var exp)))
                                        #f
                                        (list body)))))
                      ((and (equal? tag 'lambda-opt) (> (length code) 2))
                        (let* ((args (append (list-ref code 1) (list (list-ref code 2))))
                               (body (list-ref code 3)))
                            (if (contains? args var)
                                #f
                                (fold-left
                                        (lambda (acc exp) (or acc (bound2? var exp)))
                                        #f
                                        (list body)))))
                      (else (fold-left
                                    (lambda (acc exp) (or acc (bound? var exp)))
                                    #f
                                    code)))))))
                                
(define seq-destroyer
    (lambda (l)
        (letrec ((rec-func (lambda (ls)
            (if (and (list? ls) (equal? (car ls) 'seq))
                (fold-left (lambda (acc x) (append acc (rec-func x))) '() (cadr ls))
                (list ls)))))
    `(seq ,(rec-func l)))))     

(define replace-get-and-set-deeply
    (lambda (var code)
        (if (or (not (list? code)) (empty? code))
            code
            (let* ((tag (car code)))
                (cond ((equal? tag 'set) 
                        (let* ((var-name (list-ref code 1))
                               (var-value (list (list-ref code 2))))
                            (if (equal? (list-ref var-name 1) var)
                                (let ((new-var-value (map (lambda (exp) (replace-get-and-set-deeply var exp)) var-value)))
                                `(box-set ,var-name ,@new-var-value))
                                (map (lambda (exp) (replace-get-and-set-deeply var exp)) code))))
                      ((equal? tag 'var)
                        (let* ((var-name (list-ref code 1)))
                            (if (equal? var-name var)
                                `(box-get ,code)
                                code)))
                      ((equal? tag 'lambda-simple)
                        (let* ((args (list-ref code 1))
                               (body (list-ref code 2)))
                            (if (contains? args var)
                                code
                                (let ((new-body (map (lambda (exp) (replace-get-and-set-deeply var exp)) (list body))))
                                    `(lambda-simple ,args ,@new-body)))))
                      ((and (equal? tag 'lambda-opt) (> (length code) 2))
                        (let* ((args (list-ref code 1))
                               (rest (list-ref code 2))
                               (args-and-rest (append (list-ref code 1) (list (list-ref code 2))))
                               (body (list (list-ref code 3))))
                            (if (contains? args-and-rest var)
                                code
                                (let ((new-body (map (lambda (exp) (replace-get-and-set-deeply var exp)) body)))
                                    `(lambda-opt ,args ,rest ,@new-body)))))
                      (else (map (lambda (exp) (replace-get-and-set-deeply var exp)) code)))))))
    
(define do-box
    (lambda (var code)
        (let* ((newCode (replace-get-and-set-deeply var code))
               (after-addition `(seq ((set (var ,var) (box (var ,var))) ,newCode))))
               (seq-destroyer after-addition))))

(define check-lambda-simple
    (lambda (lambda-expr)
        (let* ((args (list-ref lambda-expr 1))
               (reversed-args (reverse args))
               (body (list-ref lambda-expr 2))
               (newBody (fold-left
                            (lambda (acc var) (check-and-make-box-for-var acc var))
                            body
                            reversed-args)))
            `(lambda-simple ,args ,newBody))))
                
(define check-lambda-opt
    (lambda (lambda-expr)
        (let* ((args (list-ref lambda-expr 1))
               (rest (list-ref lambda-expr 2))
               (args-and-rest (append args (list rest)))
               (reversed-args-and-rest (reverse args-and-rest))
               (body (list-ref lambda-expr 3))
               (newBody (fold-left
                            (lambda (acc var) (check-and-make-box-for-var acc var))
                            body
                            reversed-args-and-rest)))
            `(lambda-opt ,args ,rest ,newBody))))

(define check-and-make-box-for-var
    (lambda (code var)
        (if (should-box? var code)
            (do-box var code)
            code)))

(define should-box?
    (lambda (var code)
        (and 
              (get? var code)
              (set? var code)
              (bound? var code))))
            
;DO NOT DELETE                         
(define box-set
    (lambda (pexpr)
        (if (or (not (list? pexpr)) (empty? pexpr))
            pexpr
            (let* ((tag (car pexpr)))
                (cond ((and (equal? tag 'lambda-simple) (> (length pexpr) 2))
                      (map box-set (check-lambda-simple pexpr)))
                      ((and (equal? tag 'lambda-opt) (> (length pexpr) 2))
                      (map box-set (check-lambda-opt pexpr)))
                      (else (map box-set pexpr)))))))
					
					
					
					
(remove-applic-lambda-nil '(lambda-simple
  (x)
  (or ((applic
         (lambda-opt
           (y)
           z
           (applic
             (lambda-simple
               ()
               (applic
                 (lambda-simple () (applic (var +) ((var x) (var z))))
                 ()))
             ()))
         ((var x) (const 1)))
        (lambda-simple () (set (var x) (var w)))
        (applic (var w) ((var w)))))))