;'(1 2 #(3 4) (5 #f 7) 8)

;(if #f 4 123)
;(lambda (x) 1)
;((lambda (x y z) (if #t ((lambda (s e) 9) 7 1) 8)) 2 3 4)
;((lambda (x) ((lambda (y) 2) 1)) 3)
;(lambda (x) ((lambda (y) 2) 1))

;((lambda (x) 1) ((lambda (x) 2) 3))

(1 2)



;; c0 nil
;; 
;; c1 hello
;; 
;; c2 (c1 c0)
;; 
;; c3 2
;; 
;; c4 (c3 c2)
;; 
;; c5 123
;; 
;; c6 (c5 c4)