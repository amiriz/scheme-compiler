(load "1-3/qq.scm")

(define void?
    (lambda (sexpr)
        (equal? sexpr void)))

(define nilOrVector?
    (lambda (sexpr)
        (or (vector? sexpr) (null? sexpr)))) 
        
(define reservedWord?
    (lambda (sexpr)
        (if (or (equal? sexpr 'and)
                (equal? sexpr 'begin)
                (equal? sexpr 'cond)
                (equal? sexpr 'define)
                (equal? sexpr 'do)
                (equal? sexpr 'else)
                (equal? sexpr 'if)
                (equal? sexpr 'lambda)
                (equal? sexpr 'let)
                (equal? sexpr 'let*)
                (equal? sexpr 'letrec)
                (equal? sexpr 'or)
                (equal? sexpr 'quasiquote)
                (equal? sexpr 'unquote)
                (equal? sexpr 'unquote-splicing)
                (equal? sexpr 'quote)
                (equal? sexpr 'set!))
            #t
            #f)))
            
(define last
    (lambda (list)
        (let ((len (length list)))
            (list-ref list (- len 1)))))
        
(define if? 
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'if) (> (length sexpr) 1))
            #t
            #f)))
            
(define parse-if
    (lambda (if-expr)
        (cond
            ((= (length if-expr) 4)
                (list 'if3 
                    (parse (list-ref if-expr 1)) 
                    (parse (list-ref if-expr 2)) 
                    (parse (list-ref if-expr 3))))
            ((= (length if-expr) 3)
                (list 'if3 
                    (parse (list-ref if-expr 1)) 
                    (parse (list-ref if-expr 2)) 
                    (parse void))))))
                    
(define or?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'or))
            #t
            #f)))
        
        
(define parse-or
    (lambda (or-expr)
        (cond 
            ((= (length or-expr) 1) (parse #f))
            ((= (length or-expr) 2) (parse (list-ref or-expr 1)))
            (else
                (let ((pes (map (lambda (expr) (parse expr)) (cdr or-expr))))
                    `(or ,pes))))))

(define begin?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'begin))
            #t
            #f)))

(define begin-destroyer
    (lambda (l)
        (letrec ((rec-func (lambda (ls)
                                (if (and (list? ls) (equal? (car ls) 'begin))
                                    (fold-left (lambda (acc x) (append acc (rec-func x))) '() (cdr ls))
                                    (list ls)))))
    `(begin ,@(rec-func l)))))            
            
(define parse-begin
    (lambda (begin-expr)
        (cond
            ((= (length begin-expr) 1)
                (parse void))
            ((= (length begin-expr) 2)
                (parse (list-ref begin-expr 1)))
            (else
                (let ((beginless-expr (begin-destroyer begin-expr)))
                    (let ((parsed-expr (map parse (cdr beginless-expr))))
                    `(seq ,parsed-expr)))))))
                    
(define has-duplicates? 
    (lambda (lst) 
        (cond
            ((empty? lst) #f)
            ((member (car lst) (cdr lst)) #t)
            (else (has-duplicates? (cdr lst))))))
                    
                    
(define lambda?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'lambda))
            (if (or (lambda-simple? sexpr) (lambda-opt? sexpr) (lambda-variadic? sexpr))
                #t
                #f)
            #f)))
            
(define lambda-simple?
    (lambda (lambda-expr)
        (if (and (list? (list-ref lambda-expr 1)) (not (has-duplicates? (list-ref lambda-expr 1))))
            #t
            #f)))

(define imp-list?
    (lambda (expr)
        (if (and (not (list? expr)) (pair? expr))
            #t
            #f)))
            
(define lambda-opt?
    (lambda (lambda-expr)
        (if (imp-list? (list-ref lambda-expr 1))
            #t
            #f)))
            
(define lambda-variadic?
    (lambda (lambda-expr)
        (let ((args (list-ref lambda-expr 1)))
            (if (and (not (imp-list? args)) (not (list? args)))
            #t
            #f))))
            
(define parse-lambda-simple
    (lambda (lambda-simple)
        (let ((args (list-ref lambda-simple 1))
              (body (append (list 'begin) (cdr (cdr lambda-simple)))))
              (let ((parsed-body (parse-begin body)))
                `(lambda-simple ,args ,parsed-body)))))

(define findLastElem
    (lambda (imp-list)
        (letrec ((rec-func (lambda (ls) (if (not (imp-list? ls))
                                            ls
                                            (rec-func (cdr ls))))))
            (rec-func imp-list))))
            
(define findRest
    (lambda (imp-list)
        (letrec ((rec-func (lambda (ls) (if (not (imp-list? ls))
                                            '()
                                            (append (list (car ls)) (rec-func (cdr ls)))))))
            (rec-func imp-list))))
                
(define parse-lambda-opt
    (lambda (lambda-opt)
        (let ((lastElement (findLastElem (list-ref lambda-opt 1)))
              (rest (findRest (list-ref lambda-opt 1)))
              (body (append (list 'begin) (cdr (cdr lambda-opt)))))
              (let ((parsed-body (parse-begin body)))
                `(lambda-opt ,rest ,lastElement ,parsed-body)))))
    
(define parse-lambda-variadic
    (lambda (lambda-var)
        (let ((args (list-ref lambda-var 1))
              (body (append (list 'begin) (cdr (cdr lambda-var)))))
              (let ((parsed-body (parse-begin body)))
                `(lambda-opt () ,args ,parsed-body)))))
            
(define parse-lambda
    (lambda (lambda-expr)
        (cond
            ((lambda-simple? lambda-expr) (parse-lambda-simple lambda-expr))
            ((lambda-opt? lambda-expr) (parse-lambda-opt lambda-expr))
            ((lambda-variadic? lambda-expr) (parse-lambda-variadic lambda-expr)))))

(define define?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'define))
            #t
            #f)))
            
(define reg-def?
    (lambda (def-exp)
        (if (and (not (list? (list-ref def-exp 1))) (not (imp-list? (list-ref def-exp 1))) (= (length def-exp) 3))
            #t
            #f)))
            
            
(define mit-def?
    (lambda (def-exp)
        (if (or (list? (list-ref def-exp 1)) (imp-list? (list-ref def-exp 1)))
            #t
            #f)))
            
(define parse-reg-def
    (lambda (reg-def-expr)
        (list 'define
                (parse (list-ref reg-def-expr 1)) 
                (parse (list-ref reg-def-expr 2)))))
        
(define parse-mit-def
    (lambda (mit-def-expr)
        (let ((args (cdr (list-ref mit-def-expr 1)))
              (body (cdr (cdr mit-def-expr))))
            (let ((the-lambda `(lambda ,args ,@body)))
                (list 'define
                    (parse (car (list-ref mit-def-expr 1)))
                    (parse-lambda the-lambda))))))
            
(define parse-define
    (lambda (def-expr)
        (cond ((reg-def? def-expr) (parse-reg-def def-expr))
              ((mit-def? def-expr) (parse-mit-def def-expr)))))
            
(define assign?
    (lambda (sexpr)
        (if (and (list? sexpr) (= (length sexpr) 3) (equal? (list-ref sexpr 0) 'set!))
            #t
            #f)))
            
(define parse-assign
    (lambda (assign-expr)
        (let ((parsed-var (parse (list-ref assign-expr 1)))
              (parsed-value (parse (list-ref assign-expr 2))))
            `(set ,parsed-var ,parsed-value))))
            
(define applic?
    (lambda (sexpr)
        (if (and (list? sexpr) (not (reservedWord? (list-ref sexpr 0))))
            #t
            #f)))
            
(define parse-applic
    (lambda (applic-expr)
        (let ((parsed-func (parse (car applic-expr)))
              (parsed-exprs (map parse (cdr applic-expr))))
        `(applic ,parsed-func ,parsed-exprs))))
        
(define and?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'and))
            #t
            #f)))
        
        
(define parse-and
    (lambda (and-expr)
        (letrec ((recfunc (lambda (params)
                                (cond 
                                      ((= (length params) 2)
                                        (let ((if-expr (list 'if (car params) (car (cdr params)) #f)))
                                        if-expr))
                                      (else 
                                        (let ((if-expr (list 'if (car params) (recfunc (cdr params)) #f)))
                                        if-expr)))))
                 (exprs (cdr and-expr)))
        (cond
            ((= (length exprs) 0)
                (parse #t))
            ((= (length exprs) 1)
                (parse (car exprs)))
            ((= (length exprs) 2)
                (let ((if-expr (list 'if (car exprs) (car (cdr exprs)) #f)))
                    (parse if-expr)))
            (else
                (parse (recfunc exprs))))
        )))
        
(define cond?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'cond))
            #t
            #f)))
            
(define parse-cond
    (lambda (cond-exp)
        (let ((exprs (cdr cond-exp)))
            (cond
                ((and (= (length exprs) 1) (not (equal? (car (car exprs)) 'else)))
                    (let ((case1 (car exprs)))
                        (let ((cdrCase1 (cdr case1)))
                            (if (> (length (cdr case1)) 1)
                                (parse (list 'if (car case1) `(begin ,@cdrCase1)))
                                (parse (list 'if (car case1) (list-ref case1 1)))))))
                ((and (= (length exprs) 1) (equal? (car (car exprs)) 'else))
                    (let ((exprsNotList (cdr (car exprs))))
                        (parse `(begin ,@exprsNotList))))
                ((and (> (length exprs) 1) (not (equal? (car (last exprs)) 'else)))
                    (letrec ((recfunc (lambda (cases)
                                (cond 
                                      ((= (length cases) 1)
                                        (let ((lastCase (car cases)))
                                            (let ((cdrLastCase (cdr lastCase)))
                                                (if (> (length cdrLastCase) 1)
                                                    (list 'if (car lastCase) `(begin ,@cdrLastCase))
                                                    (list 'if (car lastCase) (list-ref lastCase 1))))))
                                      (else 
                                        (let ((case (car cases)))
                                            (let ((cdrCase (cdr case)))
                                                (if (> (length cdrCase) 1)
                                                    (list 'if (car case) `(begin ,@cdrCase) (recfunc (cdr cases)))
                                                    (list 'if (car case) (list-ref case 1) (recfunc (cdr cases)))))))))))
                        (parse (recfunc exprs))))
                (else
                    (let ((cdrElseCase (cdr (last exprs))))
                        (let ((elseExp `(begin ,@cdrElseCase)))
                            (letrec
                                ((recfunc (lambda (cases)
                                    (cond 
                                        ((= (length cases) 2)
                                            (let ((beforeLastCase (car cases)))
                                                (let ((cdrBeforeLastCase (cdr beforeLastCase)))
                                                    (list 'if (car beforeLastCase) `(begin ,@cdrBeforeLastCase) elseExp))))
                                        (else 
                                            (let ((case (car cases)))
                                                (let ((cdrCase (cdr case)))
                                                    (list 'if (car case) `(begin ,@cdrCase) (recfunc (cdr cases))))))))))
                        (parse (recfunc exprs))))))
                        ))))
                        
(define empty?
    (lambda (list)
        (if (= (length list) 0)
            #t
            #f)))    

(define let?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'let))
            #t
            #f)))
            
(define parse-let
    (lambda (let-expr)
        (let ((args (list-ref let-expr 1))
              (bodyListed (cdr (cdr let-expr))))
            (let ((body `(begin ,@bodyListed)))
                (if (empty? args)
                    (parse `((lambda () ,body)))
                    (let ((vars (map car args))
                        (values (map (lambda (list) (list-ref list 1)) args)))
                        (parse `((lambda ,vars ,body) ,@values))))))))
                        
(define let*?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'let*))
            #t
            #f)))
            
(define parse-let*
    (lambda (let-expr)
        (let ((args (list-ref let-expr 1))
              (bodyListed (cdr (cdr let-expr))))
            (let ((body `(begin ,@bodyListed)))
                (if (empty? args)
                    (parse `((lambda () ,body)))
                    (letrec ((vars (map car args))
                             (values (map (lambda (list) (list-ref list 1)) args))
                             (recfunc (lambda (vars values body)
                                (if (= (length vars) 1)
                                    `((lambda ,vars ,body) ,@values)
                                    (let* ((var (car vars))
                                          (value (car values))
                                          (restVars (cdr vars))
                                          (restValues (cdr values))
                                          (rec (recfunc restVars restValues body)))
                                        `((lambda (,var) ,rec) ,value) 
                                    )))))
                     (let ((expended (recfunc vars values body)))
                     (parse expended))))))))          
                     
                     
(define letrec?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'letrec))
            #t
            #f)))

            
(define parse-letrec
    (lambda (letrec-expr)
        (let* ((args (list-ref letrec-expr 1))
               (bodyListed (cdr (cdr letrec-expr)))
               (body `(begin ,@bodyListed))
               (vars (map car args))
               (values (map (lambda (list) (list-ref list 1)) args))
               (falses (map (lambda (x) #f) vars))
               (secondLambda `(lambda () ,body))
               (sets (map (lambda (x) (list 'set! (car x) (list-ref x 1))) args))
               (firstBody `(begin ,@sets (,secondLambda)))
               (firstLambda `(lambda ,vars ,firstBody))
               (expended `(,firstLambda ,@falses)))
        (parse expended))))
        
(define quasi?
    (lambda (sexpr)
        (if (and (list? sexpr) (equal? (car sexpr) 'quasiquote))
            #t
            #f)))
        
(define parse-quasi
    (lambda (quasi)
        (let ((quasiless (car (cdr quasi))))
        (parse (expand-qq quasiless)))))
               
                        
(define parse
  (lambda (sexpr)
    (cond ((quote? sexpr) (list 'const (unquotify sexpr)))
          ((const? sexpr) (list 'const sexpr))
          ((nilOrVector? sexpr) (list 'const sexpr))
          ((symbol? sexpr) (if (not (reservedWord? sexpr)) (list 'var sexpr)))
          ((void? sexpr) `(const ,(void)))
          ((if? sexpr) (parse-if sexpr))
          ((or? sexpr) (parse-or sexpr))
          ((lambda? sexpr) (parse-lambda sexpr))
          ((define? sexpr) (parse-define sexpr))
          ((begin? sexpr) (parse-begin sexpr))
          ((assign? sexpr) (parse-assign sexpr))
          ((applic? sexpr) (parse-applic sexpr))
          ((and? sexpr) (parse-and sexpr))
          ((cond? sexpr) (parse-cond sexpr))
          ((let? sexpr) (parse-let sexpr))
          ((let*? sexpr) (parse-let* sexpr))
          ((letrec? sexpr) (parse-letrec sexpr))
          ((quasi? sexpr) (parse-quasi sexpr))
          (else (ERROR "ERROR"))
    )))
    
