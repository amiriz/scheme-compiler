(define multiply_nasm "
B_multiply:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 0
	mov rax, 1
	cmp rcx, rbx
	je .done_multiply
	
.loop_multiply:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_multiply
	DATA rdx
	mul rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_multiply
	jmp .done_multiply
	
.fractions_multiply:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .mul_frac_to_frac

.mul_frac_to_int:
        DATA rdx
        mul rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	jmp .loop_fraction
	
.mul_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R13, rax
	pop rax
	mul R11
	sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	jmp .loop_fraction
	
.done_multiply:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
.done_multiply_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        cmp R13, 1
        je .denom_is_1
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
	
.denom_is_1:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
")

(define divide_nasm "
B_divide:
	push rbp
	mov rbp, rsp
	
	mov R13, 1
	mov rbx, qword [rbp + 3*8]
	mov rcx, 1
        cmp rcx, rbx
	je .one_arg_divide
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .first_fraction
	DATA rax
	jmp .loop_fraction
	
.first_fraction:
        mov rdx, rax
	NUMERATOR rax
	mov R13, rdx
	DENOMERATOR R13
	jmp .loop_fraction
	
.loop_divide:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_divide
	DATA rdx
	mov R11, rdx
	mov rdx, 0
	div R11
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_divide
	jmp .done_divide_fraction
	
.fractions_divide:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	push rax
	mov rax, R13
	mul R11
	mov R13, rax
	pop rax
	sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .div_frac_to_frac

.div_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mov rdx, 0
        mul R13
        mov R13, rax
        pop rax
        sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	jmp .loop_fraction
	
.div_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R11
	mov R13, rax
	pop rax
	mul R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	jmp .loop_fraction
	
.done_divide_fraction:

        mov rbx, R13
        shl rbx, 4
        cmp rbx, 0
        jg .no_switch
        
        push rax
        mov rax, R13
        mov rdx, -1
        mul rdx
        mov R13, rax
        pop rax
        mov rdx, -1
        mul rdx
        
.no_switch:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:
        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        

        cmp R13, 1
        je .done_divide
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret

.one_arg_divide:
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .only_fraction
	
.only_integer:
        mov rax, 1
        mov R13, qword [rbp + 4*8]
        DATA R13
        jmp .done_divide_fraction
        
.only_fraction:
        mov rax, qword [rbp + 4*8]
        mov R13, rax
        NUMERATOR R13
        DENOMERATOR rax
        jmp .done_divide_fraction

.done_divide:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
")

(define minus_nasm "
B_minus:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .first_fraction
	DATA rax
	cmp rcx, rbx
	je .one_arg_minus
	jmp .loop_minus
	
.first_fraction:
        mov rdx, rax
	NUMERATOR rax
	mov R13, rdx
	DENOMERATOR R13
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	jne .loop_fraction
	mov rbx, rax
	mov rax, -1
	mul rbx
	jmp .done_minus_fraction
	
.loop_minus:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_minus
	DATA rdx
	sub rax, rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_minus
	jmp .done_minus
	
.fractions_minus:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	sub rax, R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .sub_frac_to_frac

.sub_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mul R13
        mov rdx, rax
        pop rax
        sub rax, rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	jmp .loop_fraction
	
.sub_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R14, rax
	mov rax, R11
	mul R13
	mov R15, rax
	pop rax
	mul R12
	sub rax, R15
	mov R13, R14
	sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	jmp .loop_fraction
	
.done_minus_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
    

.one_arg_minus:
	mov rbx, 0
	sub rbx, rax
	mov rax, rbx

.done_minus:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
")

(define plus_nasm "
B_plus:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 0
	mov rax, 0
	cmp rcx, rbx
	je .done_plus
	
.loop_plus:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_plus
	DATA rdx
	add rax, rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_plus
	jmp .done_plus
	
.fractions_plus:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	add rax, R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .add_frac_to_frac

.add_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mul R13
        mov rdx, rax
        pop rax
        add rax, rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	jmp .loop_fraction
	
.add_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R14, rax
	mov rax, R11
	mul R13
	mov R15, rax
	pop rax
	mul R12
	add rax, R15
	mov R13, R14
	sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	jmp .loop_fraction
        
	
	
.done_plus:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
.done_plus_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        cmp R13, 1
        je .denom_is_1
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
	
.denom_is_1:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
")

(define equal_nasm "
B_equal:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov R14, qword [rbp + 3*8]
	
	cmp R14, 1
	je .equals
	
	mov rdi, 1
	mov rsi, 5
	
.loop:
        add rdi, 3
	
	mov rax, qword [rbp + rdi*8]
	mov rcx, rax
	TYPE rcx
	
	mov rbx, qword [rbp + rsi*8]
	mov rdx, rbx
	TYPE rdx
	
	cmp rcx, rdx
	jne .not_equals
	
	cmp rcx, T_FRACTION
	je .fraction
	
.integer:
	DATA rax
	DATA rbx
	
	cmp rax, rbx
	jne .not_equals
	
	inc rdi
	inc rsi
	
	sub rdi, 3
	cmp rdi, R14
	jne .loop
	jmp .equals
	
.fraction:
        mov rax, qword [rbp + rdi*8]
        mov rbx, qword [rbp + rsi*8]
        NUMERATOR rax
	NUMERATOR rbx
	
	cmp rax, rbx
	jne .not_equals
	
	mov rax, qword [rbp + rdi*8]
        mov rbx, qword [rbp + rsi*8]
        DENOMERATOR rax
	DENOMERATOR rbx
	
	cmp rax, rbx
	jne .not_equals
	
	inc rdi
	inc rsi
	
	sub rdi, 3
	cmp rdi, R14
	jne .loop
	
.equals:
	
	mov rax, SOB_TRUE
	leave
	ret

.not_equals:

	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define eq?_nasm "
B_eq?:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rdx, qword [rbp + 4*8]
	TYPE rdx
	mov rcx, qword [rbp + 5*8]
	TYPE rcx
	cmp rcx, rdx
	jne .not_eq
	
	mov rdx, qword [rbp + 4*8]
	DATA rdx
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	cmp rcx, rdx
	jne .not_eq
	
.eq:
	mov rax, SOB_TRUE
	leave
	ret
	
;; .symbols:
;;         mov rdx, qword [rbp + 4*8]
;; 	STRING_LENGTH rdx
;; 	
;; 	mov rcx, qword [rbp + 5*8]
;; 	STRING_LENGTH rcx
;; 	
;; 	cmp rdx, rcx
;; 	jne .not_eq
;; 	
;; 	mov rdi, rcx
;; 	
;; 	mov rdx, qword [rbp + 4*8]
;; 	STRING_ELEMENTS rdx
;; 	
;; 	mov rcx, qword [rbp + 5*8]
;; 	STRING_ELEMENTS rcx
;; 	
;; .loop:
;;         dec rdi
;;         mov al, byte [rdx + rdi]
;; 	mov bl, byte [rcx + rdi]
;; 	cmp al, bl
;; 	jne .not_eq
;; 	cmp rdi, 0
;; 	jne .loop
;; 	jmp .eq

.not_eq:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define greater_nasm "
B_greater:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	je .done_greater
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .fraction
	DATA rax
	
.loop_greater:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	TYPE rdx
	cmp rdx, T_FRACTION
	je .int_fraction
	
	.int_int:
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	cmp rcx, rbx
	jne .loop_greater
	jmp .done_greater
	
	.int_fraction:
	mov rdx, qword [rbp + rcx*8]
	DENOMERATOR rdx
	push rax
	mul rdx
	mov rdx, qword [rbp + rcx*8]
	NUMERATOR rdx
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	pop rax
	cmp rcx, rbx
	jne .loop_greater
	
.done_greater:
	mov rax, SOB_TRUE
	leave
	ret
	
.fraction:
        mov R13, rax
        NUMERATOR rax
        DENOMERATOR R13
        
.loop_fraction:
        add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	TYPE rdx
	cmp rdx, T_FRACTION
	je .fraction_fraction
	
	.fraction_int:
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	push rax
	mov rax, R13
	mul rdx
	mov rdx, rax
	pop rax
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	cmp rcx, rbx
	jne .loop_fraction
	jmp .done_greater
	
	.fraction_fraction:
	mov rdx, qword [rbp + rcx*8]
	DENOMERATOR rdx
	push rax
	push R13
	mul rdx
	mov rdx, qword [rbp + rcx*8]
	NUMERATOR rdx
	push rax
	mov rax, R13
	mul rdx
	mov rdx, rax
	pop rax
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
        pop R13
	pop rax
	cmp rcx, rbx
	jne .loop_fraction
	jmp .done_greater

.not_greater:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define less_nasm "
B_less:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	je .done_less
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	DATA rax
	
.loop_less:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	sub rcx, 3
	cmp rax, rdx
	jge .not_less
	cmp rcx, rbx
	jne .loop_less
	
.done_less:
	mov rax, SOB_TRUE
	leave
	ret

.not_less:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define length_nasm "
B_length:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	cmp rax, SOB_NIL
	je .done_length
	
.loop_length:
	inc rcx
	CDR rax
	cmp rax, SOB_NIL
	jne .loop_length
	jmp .done_length
	
.pair_opt:
	cmp rax, SOB_NIL
	je .done_length
	
.loop_opt_length:
	inc rcx
	CDR_OPT rax
	cmp rax, SOB_NIL
	jne .loop_opt_length

.done_length:
	mov rax, rcx	
	shl rax, 4
	or rax, T_INTEGER
	leave
	ret
	
")

(define car_nasm "
B_car:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	CAR rax
	jmp .done_car
	
.pair_opt:
	CAR_OPT rax

.done_car:
	leave
	ret
	
")

(define cdr_nasm "
B_cdr:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	CDR rax
	jmp .done_cdr
	
.pair_opt:
	CDR_OPT rax

.done_cdr:
	leave
	ret
	
")

(define boolean?_nasm "
B_boolean?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_BOOL
	jne .not_boolean
	
.done_boolean:
	mov rax, SOB_TRUE
	leave
	ret

.not_boolean:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define char?_nasm "
B_char?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_CHAR
	jne .not_char
	
.done_char:
	mov rax, SOB_TRUE
	leave
	ret

.not_char:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define integer?_nasm "
B_integer?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .not_integer
	
.done_integer:
	mov rax, SOB_TRUE
	leave
	ret

.not_integer:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define string?_nasm "
B_string?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_STRING
	jne .not_string
	
.done_string:
	mov rax, SOB_TRUE
	leave
	ret

.not_string:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define pair?_nasm "
B_pair?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_PAIR
	jne .not_regular_pair
	
.done_pair:
	mov rax, SOB_TRUE
	leave
	ret

.not_regular_pair:
	cmp rbx, T_PAIR_OPT
	jne .not_pair
	jmp .done_pair
	
.not_pair:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define vector?_nasm "
B_vector?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_VECTOR
	jne .not_vector
	
.done_vector:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_vector:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define null?_nasm "
B_null?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_NIL
	jne .not_null
	
.done_null:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_null:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define zero?_nasm "
B_zero?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	cmp rbx, MAKE_LITERAL(T_INTEGER, 0)
	jne .not_zero
	
.done_zero:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_zero:
	mov rax, SOB_FALSE	
	leave
	ret
")

(define symbol?_nasm "
B_symbol?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_SYMBOL
	jne .not_symbol
	
.done_symbol:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_symbol:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define procedure?_nasm "
B_procedure?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne .not_procedure
	
.done_procedure:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_procedure:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define number?_nasm "
B_number?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .not_regular_number
	
.done_number:
	mov rax, SOB_TRUE
	leave
	ret

.not_regular_number:
	cmp rbx, T_FRACTION
	jne .not_number
	jmp .done_number
	
.not_number:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define rational?_nasm "
B_rational?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_FRACTION
	je .done_rational
	cmp rbx, T_INTEGER
	je .done_rational
	jmp .not_rational
	
.done_rational:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_rational:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define apply_nasm "
B_apply:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	
	mov rbx, qword [rbp + 5*8]
	mov rcx, rbx
	mov rdx, rbx
	mov R11, rax
	
	mov rax, [my_very_own_reverse]
	
	push SOB_NIL
	push rbx
	push 1
	
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	mov rbx, rax
	CLOSURE_CODE rbx
	call rbx
	
	mov R10, rax
	add rsp, 8
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8
	mov rax, R10
	mov rdx, rax
	
	mov R8, 0
	mov rcx, rax
	mov rdx, rax
	push SOB_NIL
	TYPE rcx
	cmp rcx, T_PAIR
	je .continue_reg
	cmp rcx, T_PAIR_OPT
	je .continue_opt
	cmp rcx, T_NIL
	je .done
	jmp ERROR_INVALID_ARGS
	
.continue_reg:
	cmp rdx, SOB_NIL
	je .done
	inc R8
	mov rcx, rdx
	CAR rcx
	push rcx
	CDR rdx
	jmp .continue_reg
	
.continue_opt:
	cmp rdx, SOB_NIL
	je .done
	inc R8
	mov rax, rdx
	CAR_OPT rax
	push rax
	CDR_OPT rdx
	mov rdx, rax
	jmp .continue_opt
        
	
.done:
	mov R11, qword [rbp + 4*8]
	mov rax, R11
	push R8
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	mov rbx, rax
	CLOSURE_CODE rbx
	call rbx
	
	mov R10, rax
	add rsp, 8
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8
	mov rax, R10
	
	leave
	ret
	
")

(define not_nasm "
B_not:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	cmp rbx, SOB_FALSE
	je .done_true
	jmp .done_false
	
.done_true:
	mov rax, SOB_TRUE
	leave
	ret
	
.done_false:
	mov rax, SOB_FALSE	
	leave
	ret
	
")

(define cons_nasm "
B_cons:
	push rbp
	mov rbp, rsp
	
	mov rdx, qword [rbp + 4*8]
	;;NEW_MALLOC 8, R8
	;;mov qword [R8], rdx
	mov R8, rdx
	mov rdx, qword [rbp + 5*8]
	;;NEW_MALLOC 8, R9
	;;mov qword [R9], rdx
	mov R9, rdx
	MAKE_LITERAL_PAIR_OPT R8, R9
	;;mov rax, qword [rax]
	test:
	leave
	ret
	
")

(define string_to_symbol_nasm "
B_string_to_symbol:
	push rbp
	mov rbp, rsp
	
	mov rdx, qword [rbp + 4*8]
	TYPE rdx
	cmp rdx, T_STRING
	jne ERROR_INVALID_ARGS
	
	mov R12, qword [rbp + 4*8]
	STRING_LENGTH R12
	mov R11, qword [rbp + 4*8]
	STRING_ELEMENTS R11
	
	mov R13, qword [symbol_table]
	
.loop:
	
	cmp R13, SOB_NIL
        je .not_found
        push R13
	CAR_OPT R13
	pop R13
	mov rdx, qword [rax]
	shr rdx, 4
	add rdx, start_of_data
	mov rdx, qword [rdx]
	
	mov rax, rdx
	STRING_LENGTH rax
	cmp rax, R12
	jne .next_iter
	
	mov R14, rdx
	STRING_ELEMENTS R14
	mov rcx, 0
	
	.compare_loop:
	mov rdx, 0
	mov rbx, 0
	mov dl, byte [R11 + rcx]
	mov bl, byte [R14 + rcx]
	cmp dl, bl
	jne .next_iter
	inc rcx
	cmp rcx, R12
	je .found
	jmp .compare_loop
	
	.next_iter:
	CDR_OPT R13
	mov R13, rax
	jmp .loop

	
.not_found:
        NEW_MALLOC 8, R15   ;;the string
        NEW_MALLOC R12, R14   ;;the elements
        
	mov rcx, 0
	
        .loop_copy:
        mov rdx, 0
	mov dl, byte [R11 + rcx]
	mov byte [R14 + rcx], dl
        inc rcx
        cmp rcx, R12
        jne .loop_copy

        mov rax, R12
        shl rax, 34
        sub R14, start_of_data
        shl R14, 4
        or rax, R14
        or rax, T_STRING
        mov qword [R15], rax
        
        NEW_MALLOC 8, R12   ;;the symbol
        
        sub R15, start_of_data
        shl R15, 4
        or R15, T_SYMBOL
        
        mov qword [R12], R15
        mov rdx, qword [symbol_table]
        MAKE_LITERAL_PAIR_OPT R12, rdx
        
        mov qword [symbol_table], rax
        
        mov rax, qword [R12]
        
        leave
        ret


.found:
        CAR_OPT R13
	mov rax, qword [rax]
        
	leave
	ret
	
")

(define symbol_to_string_nasm "
B_symbol_to_string:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_SYMBOL
	jne ERROR_INVALID_ARGS
	
	mov rbx, qword [rbp + 4*8]
	
	mov rcx, qword [symbol_table]
	
.loop:

        cmp rcx, SOB_NIL
        je .not_found
        mov rdx, rcx
	CAR_OPT rdx
	mov rdx, qword [rax]
	cmp rdx, rbx
	je .found
	CDR_OPT rcx
	mov rcx, rax
	jmp .loop
	
.found:
	shr rdx, 4
	add rdx, start_of_data
	mov rax, qword [rdx]
	
	leave
	ret
	
.not_found:	
	
        jmp ERROR_NOT_MAKE_SENSE
	
")

(define char_to_integer_nasm "
B_char_to_integer:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_CHAR
	jne ERROR_INVALID_ARGS
	
	mov rbx, rax
	DATA rbx
	cmp rbx, 0
	jl ERROR_INVALID_ARGS
	cmp rbx, 127
	jg .patch
	
.done:
	shl rbx, 4
	or rbx, T_INTEGER
	mov rax, rbx
		
	leave
	ret
	
.patch:
        mov rcx, rbx
        and rcx, 255
        sub rcx, 194
        mov rax, 64
        mul rcx
        shr rbx, 8
        add rbx, rax
	jmp .done
        
	
")

(define integer_to_char_nasm "
B_integer_to_char:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne ERROR_INVALID_ARGS
	
	mov rbx, rax
	DATA rbx
	cmp rbx, 0
	jl ERROR_INVALID_ARGS
	cmp rbx, 256
	jge ERROR_INVALID_ARGS
	
	xor rax, (T_CHAR ^ T_INTEGER)
		
	leave
	ret
	
")

(define numerator_nasm "
B_numerator:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	je .integer

.fraction:
	NUMERATOR rax
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	jmp .done
	
.integer:
	jmp .done
	
.done:
	leave
	ret
	
")

(define denominator_nasm "
B_denominator:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	je .integer


.fraction:
	DENOMERATOR rax
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	jmp .done
	
.integer:
	mov rax, 1
	shl rax, TYPE_BITS
	or rax, T_INTEGER
	jmp .done
	
.done:
	leave
	ret
	
")

(define set_car_nasm "
B_set_car:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	mov rdx, qword [rbp + 5*8]
	TYPE rbx
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	jmp .done

.pair_opt:
        DATA rax
        mov qword [rax], rdx
	
.done:
        mov rax, SOB_VOID
	leave
	ret
	
")

(define set_cdr_nasm "
B_set_cdr:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	mov rdx, qword [rbp + 5*8]
	TYPE rbx
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	jmp .done

.pair_opt:
        DATA rax
        mov qword [rax+8], rdx
	
.done:
        mov rax, SOB_VOID
	leave
	ret
	
")

(define vector_nasm "
B_vector:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 3*8]
	cmp rax, 0
	je .empty_vector
	
	mov rdx, 8
	mul rdx
	mov rcx, rax
	NEW_MALLOC rcx, rdx
	
	mov rcx, qword [rbp + 3*8]
	add rcx, 4
	mov rdi, 4
.loop:
        mov rax, qword [rbp + rdi*8]
        mov R10, 8
        NEW_MALLOC R10, R11
        mov qword [R11], rax
        sub rdi, 4
        mov qword [rdx + 8*rdi], R11
        add rdi, 5
        cmp rdi, rcx
        jne .loop
	
.done:
        mov rax, qword [rbp + 3*8]
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
.empty_vector:
        mov rax, qword [rbp + 3*8]
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
")

(define vector_length_nasm "
B_vector_length:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	VECTOR_LENGTH rax
	shl rax, 4
	or rax, T_INTEGER
	
.done:
	leave
	ret
	
")

(define vector_ref_nasm "
B_vector_ref:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	;VECTOR_REF rax, rbx, rcx
	
	mov rdx, rbx
	VECTOR_ELEMENTS rdx
	mov rax, rcx
	mov rbx, 8
	push rdx
	mul rbx
	pop rdx
	add rdx, rax
	mov rdx, qword [rdx]
	mov rdx, qword [rdx]
	
	mov rax, rdx
	
.done:
	leave
	ret
	
")

(define vector_set_nasm "
B_vector_set:
	push rbp
	mov rbp, rsp
	
	mov rax, 8
	NEW_MALLOC rax, R11
	mov rax, qword [rbp + 6*8]
	mov qword [R11], rax
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	VECTOR_ELEMENTS rbx
	push rbx
	mov rax, rcx
	mov rbx, 8
	mul rbx
	pop rbx
	add rbx, rax
	mov qword [rbx], R11
	
	mov rax, SOB_VOID
	
.done:
	leave
	ret
	
")

(define string_length_nasm "
B_string_length:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	STRING_LENGTH rax
	shl rax, 4
	or rax, T_INTEGER
	
.done:
	leave
	ret
	
")

(define string_ref_nasm "
B_string_ref:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	mov rdx, 0
	push rax
	mov rax, rbx
	STRING_ELEMENTS rax
	add rax, rcx
	mov dl, byte [rax]
	pop rax
	
	mov rax, rdx
	shl rax, 4
	or rax, T_CHAR
	
.done:
	leave
	ret
	
")

(define string_set_nasm "
B_string_set:
	push rbp
	mov rbp, rsp
	
	mov rdx, 0
	mov R11, qword [rbp + 6*8]
	DATA R11
	mov rdx, R11
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	
	push rax
	mov rax, rbx
	STRING_ELEMENTS rax
	add rax, rcx
	mov byte [rax], dl
	pop rax
	
	mov rax, SOB_VOID
	
.done:
	leave
	ret
	
")

(define make_string_nasm "
B_make_string:
        push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	DATA rbx
        
	cmp rbx, 0
	je .empty_list
	
	NEW_MALLOC rbx, rdx
	
	mov rsi, qword [rbp + 3*8]
	
	cmp rsi, 1
	jne .regular_args
	
	mov rcx, 0
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
	jmp .loop
	
.regular_args:
        mov rcx, qword [rbp + 5*8]
        DATA rcx
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
.loop:
        mov byte [rdx + rdi], cl
        inc rdi
        dec rbx
        cmp rbx, 0
        jne .loop
	
        mov rax, qword [rbp + 4*8]
        DATA rax
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_STRING
        
	leave
	ret
	
.empty_list:
        mov rax, 0
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_STRING
        
	leave
	ret
	
")

(define make_vector_nasm "
B_make_vector:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	DATA rbx
        
	cmp rbx, 0
	je .empty_vector
	
	mov rax, rbx
	mov rdx, 8
	mul rdx
	mov rcx, rax
	NEW_MALLOC rcx, rdx
	
	mov rsi, qword [rbp + 3*8]
	
	cmp rsi, 1
	jne .regular_args
	
	mov rcx, MAKE_LITERAL(T_INTEGER, 0)
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
	jmp .loop
	
.regular_args:
        mov rcx, qword [rbp + 5*8]
	mov rbx, qword [rbp + 4*8]
	DATA rbx
	mov rdi, 0
.loop:
        mov R10, 8
        push rbx
        NEW_MALLOC R10, R11
        pop rbx
        mov qword [R11], rcx
        mov qword [rdx + 8*rdi], R11
        inc rdi
        dec rbx
        cmp rbx, 0
        jne .loop
	
        mov rax, qword [rbp + 4*8]
        DATA rax
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
.empty_vector:
        mov rax, 0
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
")

(define built-ins '(length * / -
                    + = >
                    boolean? char? integer? string?
                    pair? vector? null? zero?
                    symbol? procedure? number? rational?
                    string->symbol symbol->string
                    char->integer integer->char
                    eq? apply not cons car cdr
                    numerator denominator set-car! set-cdr!
                    vector vector-length vector-ref
                    vector-set! string-length make-vector
                    string-ref string-set! make-string))

(define built-ins-code
	(string-append
	apply_nasm
	car_nasm
	cdr_nasm
	length_nasm
	multiply_nasm
	divide_nasm
	minus_nasm
	plus_nasm
	equal_nasm
	greater_nasm
	boolean?_nasm
	char?_nasm
	integer?_nasm
	string?_nasm
	pair?_nasm
	vector?_nasm
	null?_nasm
	zero?_nasm
	symbol?_nasm
	procedure?_nasm
	number?_nasm
	rational?_nasm
	not_nasm
	cons_nasm
	eq?_nasm
	string_to_symbol_nasm
	symbol_to_string_nasm
	char_to_integer_nasm
	integer_to_char_nasm
	numerator_nasm
	denominator_nasm
	set_car_nasm
	set_cdr_nasm
	vector_nasm
	vector_length_nasm
	vector_ref_nasm
	vector_set_nasm
	string_length_nasm
	string_ref_nasm
	string_set_nasm
	make_string_nasm
	make_vector_nasm))
	