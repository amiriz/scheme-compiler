(load "1-3/pc.scm")
        
(define <Whitespace>
  (new
   (*parser (range #\nul #\space))
   done))

(define <LineComment>
    (new
        (*parser (char #\;))
        (*parser <any>)
        (*parser <end-of-input>)
        (*parser (char #\newline))
        (*disj 2)
		*diff
		*star
        (*caten 2)
        (*parser <Whitespace>) *star
        (*parser (char #\newline))
        (*parser (delayed (lambda () <Comments>)))
        (*disj 2)
        (*caten 2) *star
        (*caten 2)
        done))

(define <InfixExpressionComment>
    (new
        (*parser (word "#;"))
        (*parser (delayed (lambda () <InfixExpression>)))
        (*caten 2)
        done))
        
(define <ExpressionComment>
    (new
        (*parser (word "#;"))
        (*parser (delayed (lambda () <sexpr>)))
        (*caten 2)
        done))
   
(define <Comments>
    (new
        (*parser <LineComment>)
        (*parser <ExpressionComment>)
        (*disj 2)
        done))
        
(define <CommentsInfix>
    (new
        (*parser <LineComment>)
        (*parser <InfixExpressionComment>)
        (*disj 2)
        done))

        
(define <Boolean>
  (new
   (*parser (word-ci "#t"))
   (*pack
    (lambda (x) #t))
   (*parser (word-ci "#f"))
   (*pack
    (lambda (x) #f))
   (*disj 2)
   
   done))

(define <CharPrefix>
  (new
   (*parser (char #\#))
   (*parser (char #\\))
   (*caten 2)
   (*pack-with 
    (lambda (x y) (string #\# #\\)))
   done))

(define <VisibleSimpleChar>
  (new
   (*parser (const (lambda (char) (char>=? char #\space))))
   done))

(define <NamedChar>
  (new
   (*parser (word-ci "lambda"))
	(*pack (lambda (x) #\λ))
   (*parser (word-ci "newline"))
	(*pack (lambda (x) #\newline))
   (*parser (word-ci "nul"))
	(*pack (lambda (x) #\nul))
   (*parser (word-ci "page"))
	(*pack (lambda (x) #\page))
   (*parser (word-ci "return"))
	(*pack (lambda (x) #\return))
   (*parser (word-ci "space"))
	(*pack (lambda (x) #\space))
   (*parser (word-ci "tab"))
	(*pack (lambda (x) #\tab))
   (*disj 7)
   done))

(define <HexChar>
  (new
   (*parser (range #\0 #\9))
   (*parser (range-ci #\a #\f))
   (*disj 2)
   done))

(define <HexUnicodeChar>
  (new
   (*parser (char #\x))
   (*parser <HexChar>) *plus
   (*caten 2)
   (*pack-with
    (lambda (x HxChar)(integer->char (string->number (list->string HxChar) 16))))
   done))

(define <Char>
  (new
   (*parser <CharPrefix>)
   (*parser <NamedChar>)
   (*parser <HexUnicodeChar>)
   (*parser <VisibleSimpleChar>)
   (*disj 3)
   (*caten 2)
   (*pack-with
    (lambda (pre char) `,pre char))
   done))

(define <Natural>
  (new
   (*parser (range #\0 #\9)) *plus
   (*pack
    (lambda (first+rest)
      (string->number
       (list->string first+rest))))
   done))

(define <Integer>
  (new
   (*parser (char #\+))
   (*parser (char #\-))
   (*parser <epsilon>)
   (*disj 3)
   (*parser <Natural>)
   (*caten 2)
   (*pack-with
    (lambda (sign natural)
        (if (equal? sign #\-)
            (* -1 natural)
            natural)))
   done))

(define <Fraction>
  (new
   (*parser <Integer>)
   (*parser (char #\/))
   (*parser <Natural>)
   (*caten 3)
   (*pack-with
    (lambda (numer slash denum)
        (/ numer denum)))
   done))

(define <InfixNumber>
  (new
   (*parser <Fraction>)
   (*parser <Integer>)
   (*disj 2)
   (*delayed (lambda () <SymbolChar>))
   (*delayed (lambda () <AritmeticOps>))
   *diff
   *not-followed-by
   done))
   
(define <Number>
  (new
   (*parser <Fraction>)
   (*parser <Integer>)
   (*disj 2)
   (*delayed (lambda () <SymbolChar>))
   ;(*delayed (lambda () <AritmeticOps>))
   ;*diff
   *not-followed-by
   done))

(define <BackslashDoubleQuote>
  (new
   (*parser (char #\\))
   (*parser (char #\"))
   (*disj 2)
   done))

(define <StringLiteralChar>
  (new
   (*parser <any-char>)
   (*parser <BackslashDoubleQuote>) *diff
   done))

(define <StringMetaChar>
 (new
    (*parser (word "\\\\"))
        (*pack (lambda (mc) #\\#\\))
    (*parser (word "\\\""))
        (*pack (lambda (mc) #\\#\"))
    (*parser (word-ci "\\t"))
        (*pack (lambda (mc) #\tab))
    (*parser (word-ci "\\f"))
        (*pack (lambda (mc) #\xC))
    (*parser (word-ci "\\n"))
        (*pack (lambda (mc) #\newline))
    (*parser (word-ci "\\r"))
        (*pack (lambda (mc) #\xD))
    (*disj 6)
    done))

(define <StringHexChar>
  (new
   (*parser (char #\\))
   (*parser (char-ci #\x))
   (*caten 2)
   (*parser <HexChar>) *star
   (*parser (char #\;))
   (*caten 3)
   (*pack-with
    (lambda (x HxChar dotComa) (integer->char (string->number (list->string HxChar) 16))))
   done))
(define <Whitespace>
  (new
   (*parser (range #\nul #\space))
   done))
(define <StringChar>
  (new
   (*parser <StringHexChar>)
   (*parser <StringMetaChar>)
   (*parser <StringLiteralChar>)
   (*disj 3)
   done))
   
(define <String>
  (new
   (*parser (char #\"))
   (*parser <StringChar>) *star
   (*parser (char #\"))
   (*caten 3)
   (*pack-with 
    (lambda (quotes1 str quotes2) (list->string str)))
   done))
   
(define <CharNoNumber>
    (new
        (*parser (range-ci #\a #\z))
        (*parser (char #\!))
        (*parser (char #\$))
        (*parser (char #\^))
        (*parser (char #\*))
        (*parser (char #\-))
        (*parser (char #\_))
        (*parser (char #\=))
        (*parser (char #\+))
        (*parser (char #\<))
        (*parser (char #\>))
        (*parser (char #\?))
        (*parser (char #\/))
        (*disj 13)
        done))

(define <SymbolChar>
  (new
   (*parser (range #\0 #\9))
   (*parser <CharNoNumber>)
   (*disj 2)
   done))

(define <Symbol>
  (new
   (*parser <SymbolChar>) *plus
   (*pack
    (lambda (symbol) (string->symbol (string-downcase (list->string symbol)))))
   done))
   
(define <SymbolThatStartsWithNumber>
    (new
        (*parser (range #\0 #\9)) *plus
        (*parser <CharNoNumber>) *plus
        (*parser <SymbolChar>) *star
        (*caten 3)
        (*pack-with
            (lambda (x y z) 
            (string->symbol (string-downcase (list->string (list (car x) (car y) (car z)))))))
        done))
   
   
(define <ProperList>
  (new
   (*parser (char #\())
   (*parser <Whitespace>) *star
   (*parser (delayed (lambda () <sexpr>)))
   (*parser <Whitespace>) *star
   (*caten 3)
   (*pack-with 
    (lambda (whites1 sexpr whites2) sexpr))
   *star
   (*parser (char #\)))
   (*caten 3)
   (*pack-with
    (lambda (open sexprs close) sexprs))
   done))
   
(define <ImproperList>
  (new
   (*parser (char #\())
   (*parser <Whitespace>) *star
   (*parser (delayed (lambda () <sexpr>))) 
   (*parser <Whitespace>) *star
   (*caten 3) 
   (*pack-with 
    (lambda (whites1 sexpr whites2) sexpr)) *plus
   (*parser <Whitespace>) *star
   (*parser (char #\.))
   (*parser <Whitespace>) *star
   (*parser (delayed (lambda () <sexpr>)))
   (*parser <Whitespace>) *star
   (*parser (char #\)))
   (*caten 8)
   (*pack-with 
    (lambda (open sexprs spaces1 dot spaces2 lastSexpr spaces3 close)
        (append sexprs lastSexpr)))
   done))
   
(define <Vector>
  (new
   (*parser (char #\#))
   (*parser <ProperList>) 
   (*caten 2)
   (*pack-with
    (lambda (hash lst) `#(,@lst) ))
   done))

(define <Quoted>
  (new
   (*parser (char #\'))
   (*parser (delayed (lambda () <sexpr>)))
   (*caten 2)
   (*pack-with
    (lambda (q sexpr) `',sexpr))
   done))

(define <QuasiQuoted>
  (new
   (*parser (char #\`))
   (*parser (delayed (lambda () <sexpr>)))
   (*caten 2)
   (*pack-with
    (lambda (q sexpr) (list 'quasiquote sexpr)))
   done))

(define <Unquoted>
  (new
   (*parser (char #\,))
   (*parser (delayed (lambda () <sexpr>)))
   (*caten 2)
   (*pack-with
    (lambda (uq sexpr) (list 'unquote sexpr)))
   done))

(define <UnuoteAndSpliced>
  (new
   (*parser (word ",@"))
   (*parser (delayed (lambda () <sexpr>)))
   (*caten 2)
   (*pack-with
    (lambda (uq sexpr) (list 'unquote-splicing sexpr)))
   done))

   
   
(define <CBNameSyntax1>
  (new
   (*parser (char #\@))
   (*parser (delayed (lambda () <sexpr>)))
   (*caten 2)
   (*pack-with
    (lambda (at sx) `(cbname ,sx)))
   done))

(define <CBNameSyntax2>
  (new
   (*parser (char #\{))
   (*parser (delayed (lambda () <sexpr>)))
   (*parser (char #\}))
   (*caten 3)
   (*pack-with
    (lambda (open sx close) `(cbname ,sx)))
   done))

(define <CBName>
  (new
   (*parser <CBNameSyntax1>)
   (*parser <CBNameSyntax2>)
   (*disj 2)
   done))

(define <InfixPrefixExtensionPrefix>
  (new
   (*parser (word "##"))
   (*parser (word "#%"))
   (*disj 2)
   done))

(define <AritmeticOps>
  (new
   (*parser (char #\+))
   (*parser (char #\-))
   (*parser (char #\*))
   (*parser (char #\^))
   (*parser (char #\/))
   (*parser (word "**"))
   (*disj 6)
   done))

(define <InfixSymbol>
  (new
   (*parser <SymbolChar>) 
   (*parser <AritmeticOps>) *diff
   *plus
   (*pack
    (lambda (symbol) (string->symbol (string-downcase (list->string symbol)))))
   done))

   
(define <InfixNeg>
    (new
        (*parser (char #\-))
        (*pack
            (lambda (op) (string->symbol (string op))))
            
        (*parser <Whitespace>) *star    
            
        (*parser (delayed (lambda () <InfixArrayAndFunc>)))
        (*parser (delayed (lambda () <InfixNumber>)))
        (*parser (delayed (lambda () <InfixSymbol>)))
        (*parser (delayed (lambda () <InfixSexprEscape>)))
        ;(*parser (delayed (lambda () <InfixExpression>)))
        (*disj 4)
        (*caten 3)
        (*pack-with
            (lambda (sign w1 exp)
                `(,sign ,exp)))
        done))

(define <PowerSymbol>
  (new
   (*parser (char #\^))
   (*parser (word "**"))
   (*disj 2)
   (*pack
    (lambda (sign) 'expt))
   done))

(define <InfixParen>
  (new
   (*parser (char #\())
   (*parser (delayed (lambda () <InfixExpression>)))
   (*parser (char #\)))
   (*caten 3)
   (*pack-with
    (lambda (open exp close) `(,@exp)))
   (*parser (delayed (lambda () <InfixNumber>)))
   (*parser (delayed (lambda () <InfixSymbol>)))
   (*disj 3)
   done))

(define <InfixSexprEscape>
  (new
   (*parser <Whitespace>) *star
   (*parser <InfixPrefixExtensionPrefix>)
   (*parser (delayed (lambda () <sexprWithArrayGet>)))
   (*caten 3)
   (*pack-with
    (lambda (w1 extension sexpr) sexpr))
   done))

(define <MathOperator>
    (new
        (*parser (char #\^))
        (*parser (char #\*))
        (*parser (char #\/))
        (*parser (char #\+))
        (*parser (char #\-))
        (*disj 5)
        (*pack
            (lambda (op) (string->symbol (string op))))
        done))
        
(define <Plus>
    (new
        (*parser (char #\+))
        (*pack
            (lambda (op) (string->symbol (string op))))
        done))
        
(define <Minus>
    (new
        (*parser (char #\-))
        (*pack
            (lambda (op) (string->symbol (string op))))
        done))
        
(define <Multiply>
    (new
        (*parser (char #\*))
        (*pack
            (lambda (op) (string->symbol (string op))))
        done))
        
(define <Divide>
    (new
        (*parser (char #\/))
        (*pack
            (lambda (op) (string->symbol (string op))))
        done))
   
(define <InfixMathOperation> 
    (new
        (*parser (delayed (lambda () <InfixExpression>)))
        (*parser <MathOperator>)
        (*parser (delayed (lambda () <InfixExpression>)))
        (*caten 3)
        done))
        
(define <InfixFiniteMathOperation> 
    (new
        (*parser <Number>)
        (*parser <MathOperator>)
        (*parser (delayed (lambda () <InfixExpression>)))
        (*caten 3)
           (*pack-with
            (lambda (expr1 op expr2)
            `(,op ,expr2 ,expr1)))
        done))



(define powFold
    (lambda (a b)
        (if (equal? b '()) a `(expt ,a ,b))))
        
        
(define <powInfix>
    (new
        
        (*parser (delayed (lambda () <InfixArrayAndFunc>)))
        (*parser <Whitespace>) *star
        (*caten 2)
        (*pack-with
            (lambda (expr w1) expr))
            
        (*parser <PowerSymbol>)
        (*parser <Whitespace>) *star
        (*parser <CommentsInfix>) *star
        (*parser <Whitespace>) *star
        (*parser (delayed (lambda () <InfixArrayAndFunc>)))
        (*parser <Whitespace>) *star
        (*parser <CommentsInfix>) *star
        (*parser <Whitespace>) *star
        (*caten 8)
        (*pack-with
            (lambda (div w1 c1 w2 expr w3 c2 w4) expr))
        *plus
        
        (*caten 2)
        (*pack-with
            (lambda (part1 part2) (fold-right powFold '() (append (list part1) part2))))
        
        (*parser (delayed (lambda () <InfixArrayAndFunc>)))
        (*parser (delayed (lambda () <InfixNeg>)))
        
        (*disj 3)
        done))        
        
 
        
(define mulDivFold
    (lambda (a b)
        (let*
            ((op (car b))
             (newB (cadr b)))
            (if (equal? op (string->symbol (string #\*)))
                `(* ,a ,newB)
                `(/ ,a ,newB)))))
        
(define <mulDivInfix>
    (new
        (*parser <powInfix>)
        (*parser <Whitespace>) *star
        (*caten 2)
        (*pack-with
            (lambda (expr w1) expr))
            
        (*parser <Multiply>)
        (*parser <Divide>)
        (*disj 2)
        (*parser <Whitespace>) *star
        (*parser <CommentsInfix>) *star
        (*parser <Whitespace>) *star
        (*parser <powInfix>)
        (*parser <Whitespace>) *star
        (*parser <CommentsInfix>) *star
        (*parser <Whitespace>) *star
        (*caten 8)
        (*pack-with
            (lambda (op w1 c1 w2 expr w3 c2 w4) `(,op ,expr)))
        *plus
        
        (*caten 2)
        (*pack-with
            (lambda (part1 part2) (fold-left mulDivFold part1 part2)))
        
        (*parser <powInfix>)
        
        (*disj 2)
        done))
        
        
(define plusMinusFold
    (lambda (a b)
        (let*
            ((op (car b))
             (newB (cadr b)))
            (if (equal? op (string->symbol (string #\+)))
                `(+ ,a ,newB)
                `(- ,a ,newB)))))
        
; ## 1 + #; 2 3 ---> match (+ 1 3)
        
(define <plusMinusInfix>
    (new
        
        (*parser <mulDivInfix>)
        (*parser <Whitespace>) *star
        (*caten 2)
        (*pack-with
            (lambda (expr w1) expr))
            
        (*parser <Plus>)
        (*parser <Minus>)
        (*disj 2)
        (*parser <Whitespace>) *star
        (*parser <CommentsInfix>) *star
        (*parser <Whitespace>) *star
        (*parser <mulDivInfix>)
        (*parser <Whitespace>) *star
        (*parser <CommentsInfix>) *star
        (*parser <Whitespace>) *star
        (*caten 8)
        (*pack-with
            (lambda (op w1 c1 w2 expr w3 c2 w4) `(,op ,expr)))
        *plus
        
        (*caten 2)
        (*pack-with
            (lambda (part1 part2) (fold-left plusMinusFold part1 part2)))
        
        (*parser <mulDivInfix>)
        
        (*disj 2)
        done))
        

        

		
(define <InfixArgList>
  (new
   (*parser <Whitespace>) *star
   (*parser (delayed (lambda () <InfixExpression>)))
   (*pack 
	(lambda (expr)
		expr))
   (*parser (char #\,))
   (*parser <Whitespace>) *star
   (*parser (delayed (lambda () <InfixExpression>))) 
   (*caten 3)
   (*pack-with 
	(lambda (coma-space w1 expr)
		expr))
	*star
   (*caten 3)
   (*pack-with 
	(lambda (w1 first rest)
		(append (list first) rest)))
   (*parser <epsilon>)
   (*disj 2)
   done))
		
(define <InfixFuncallNoLeftRecursion>
   (new
	(*parser (char #\())
	(*parser <InfixArgList>)
	(*parser (char #\)))
	(*parser (delayed (lambda () <InfixFuncallNoLeftRecursion>)))
	(*caten 4)
	(*pack-with 
	(lambda (paren1 arglist paren2 func)
		`(,@arglist)))
	(*parser <epsilon>)
	(*disj 2)
	done))
	
(define <InfixFuncall>
    (new
    (*parser <InfixSymbol>)
    (*parser (char #\())
    (*parser <InfixArgList>)
    (*parser (char #\)))
    (*caten 4)
    (*pack-with 
	(lambda (name paren1 arglist paren2)
		`(,name ,@arglist)))
    (*parser <Number>)
    (*parser <InfixSymbol>)
    (*parser <InfixSexprEscape>)
    (*disj 4)
    done))


   
(define <MathArithmetics> <plusMinusInfix>)


(define <InfixArrayGetNoLeftRecursion>
   (new
	(*parser (char #\[))
	(*parser (delayed (lambda () <InfixExpression>)))
	(*parser (char #\]))
	(*parser (delayed (lambda () <InfixArrayGetNoLeftRecursion>)))
	(*caten 4)
	(*pack-with 
	(lambda (paren1 index paren2 rec)
		`(vector-ref ,rec ,index)))
	(*parser <epsilon>)
	(*disj 2)
	done))
	
(define fun1
    (lambda (arrayName index rec)
        (letrec ((recFind (lambda (rec)
                    (if (equal? rec '())
                        (list 'vector-ref arrayName index)
                        (list (list-ref rec 0) (recFind (list-ref rec 1)) (list-ref rec 2))))))
        (recFind rec))))

(define <InfixArrayGet>
    (new
        (*parser (delayed (lambda () <InfixFuncall>)))
        (*parser <InfixSymbol>)
        (*disj 2)
        (*parser (char #\[))
	(*parser (delayed (lambda () <InfixExpression>)))
	(*parser (char #\]))
	(*parser <InfixArrayGetNoLeftRecursion>)
	(*caten 5)
	(*pack-with 
            (lambda (array paren1 index paren2 rec)
                (fun1 array index rec)))
        (*parser (delayed (lambda () <InfixFuncall>)))
        (*disj 2)
        done))
        

(define foldFunc
    (lambda (a b)
        (if (equal? (car b) #\[)
            (list 'vector-ref a (car (cdr b)))
            (append (list a) (car (cdr b))))))
        
(define <InfixArrayAndFunc>
    (new
        (*parser <InfixSexprEscape>)
    
        (*parser <InfixParen>)
        (*parser <InfixNumber>)
        (*parser <InfixSymbol>)
        (*disj 3)
        
        (*parser <Whitespace>) *star
        
        (*parser (char #\[))
        (*delayed (lambda () <InfixExpression>))
        (*parser (char #\]))
        (*caten 3)
        ;*plus
        
        (*parser (char #\())
        (*parser <Whitespace>) *star
        (*delayed (lambda () <InfixArgList>))
        (*parser <Whitespace>) *star
        (*parser (char #\)))
        (*caten 5)
        (*pack-with
            (lambda (open w1 arglist w2 close)
                (list #\( arglist #\))))
         ;*star

        (*disj 2)
        *star
        
        (*caten 3)
        (*pack-with 
            (lambda (name w1 arrayOrFunc)
                (fold-left foldFunc name arrayOrFunc)))
        
        (*disj 2)
        done))

(define <InfixExpression>
  (new
   (*parser <Whitespace>) *star
   (*parser <CommentsInfix>) *maybe
   
   (*parser <MathArithmetics>)
   (*parser <InfixNeg>)
   (*parser <InfixSexprEscape>)
   ;(*parser <MathArithmetics>)
   ;(*pack (lambda (expr) (display "Math") expr))
      ;(*parser <MathArithmetics>)
   ;(*pack (lambda (expr) (display "Math") expr))
      ;(*parser <MathArithmetics>)
   ;(*pack (lambda (expr) (display "Math") expr))
      ;(*parser <MathArithmetics>)
      
      
      
   ;(*pack (lambda (expr) (display "Math") expr))
   (*disj 3)
   
   (*parser <Whitespace>) *star
   (*parser <CommentsInfix>) *maybe
   
   (*caten 5)
   (*pack-with
    (lambda (w1 c1 expr w2 c2) expr))
   done))

(define <InfixExtension>
  (new
   (*parser <InfixPrefixExtensionPrefix>)
   (*parser <Whitespace>) *star
   (*parser <CommentsInfix>) *maybe
   (*parser <Whitespace>) *star
   (*parser <InfixExpression>)
   (*parser <Whitespace>) *star
   (*parser <CommentsInfix>) *maybe
   (*parser <Whitespace>) *star
   (*caten 8)
   (*pack-with
    (lambda (prefix w1 comments1 w2 expr w3 comments2 w4) expr))
   done))
   
   
(define <ArrayGet>
    (new
        ;(*parser <InfixSexprEscape>)
    
        (*parser <Symbol>)
        
        (*parser <Whitespace>) *star
        
        (*parser (char #\[))
        (*delayed (lambda () <InfixExpression>))
        (*parser (char #\]))
        (*caten 3)
        *plus
        
        (*caten 3)
        (*pack-with 
            (lambda (name w1 array)
                (fold-left foldFunc name array)))
      
        done))

(define <sexprWithArrayGet>
  (new
   (*parser <Comments>) *maybe
   (*parser <Whitespace>) *star
   (*parser <ArrayGet>)
   (*parser <ProperList>)
   (*parser <ImproperList>)
   (*parser <Vector>)
   (*parser <Quoted>)
   (*parser <QuasiQuoted>)
   (*parser <UnuoteAndSpliced>)
   (*parser <Unquoted>)
   (*parser <CBName>)
   (*parser <InfixExtension>)
   (*parser <Boolean>)
   ;(*parser <SymbolThatStartsWithNumber>)
   (*parser <Number>)
   (*parser <Char>)
   (*parser <String>)
   (*parser <Symbol>)
   (*disj 15)
   (*parser <Whitespace>) *star
   (*parser <Comments>) *maybe
   (*caten 5)
   (*pack-with
    (lambda (comments1 white1 sexpr white2 comments2)
        sexpr))
   done))

(define <sexpr>
  (new
   (*parser <Comments>) *maybe
   (*parser <Whitespace>) *star
   (*parser <ProperList>)
   (*parser <ImproperList>)
   (*parser <Vector>)
   (*parser <Quoted>)
   (*parser <QuasiQuoted>)
   (*parser <UnuoteAndSpliced>)
   (*parser <Unquoted>)
   (*parser <CBName>)
   (*parser <InfixExtension>)
   (*parser <Boolean>)
   ;(*parser <SymbolThatStartsWithNumber>)
   (*parser <Number>)
   (*parser <Char>)
   (*parser <String>)
   (*parser <Symbol>)
   (*disj 14)
   (*parser <Whitespace>) *star
   (*parser <Comments>) *maybe
   (*caten 5)
   (*pack-with
    (lambda (comments1 white1 sexpr white2 comments2)
        sexpr))
   done))

;(define <sexpr> <sexpr1>)


;(test-string <sexpr> "##func(a ,b,c,d,e,fgh)")
;(test-string <sexpr> "## a[0] + a[a[0]] * #% (+ 5 7 4) * ## a[a[0]]")
;(test-string <sexpr> "##1 + #%2 * ##a[0]")

(test-string <sexpr> "2^3")
(test-string <sexpr> "'(#\\lambda (Param)
    ;add one
        ##Param + 1)")






;;; <InfixExpression>
;;; 1.Need to make sure that minus and div associate correctly
;;; 2.Need to make sure Seder Peulot is correct.
