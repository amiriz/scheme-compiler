(load "1-3/sexpr-parser.scm")
(load "1-3/tag-parser.scm")
(load "1-3/semantic-analyzer.scm")
(load "meir.scm")

(define pipeline
    (lambda (s)
        ((star <sexpr>) s
            (lambda (m r)
                (map (lambda (e)
                    (annotate-tc
                        (pe->lex-pe
                            (box-set
                                (remove-applic-lambda-nil
                                    (parse e))))))
        m))
    (lambda (f) 'fail))))

(define file->list
    (lambda (in-file)
        (let ((in-port (open-input-file in-file)))
            (letrec ((run
                        (lambda ()
                            (let ((ch (read-char in-port)))
                                (if (eof-object? ch)
                                    (begin
                                        (close-input-port in-port)
                                        '())
                                    (cons ch (run)))))))
                (run)))))
                
(define compile-scheme-file
    (lambda (in-file out-file)
		(let* ((in-string (pipeline (file->list in-file)))
			   (action (make-const-table in-string))
			   (out-string (string-append
                                            prologue1
                                            (code-gen-const-table)
                                            prologue2
                                            (code-gen in-string)
                                            epilogue)))
			(string->file out-string out-file))))

    
(define string->file
    (lambda (string out-name)
        (display string (open-output-file out-name))))

(define parse1-3
	(lambda (in-file)
		(pipeline (file->list in-file))))
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		

(define const-table '())

(define const-counter 0) 
(define fvar-counter 0) 
(define if3-counter 0)
(define or-counter 0)
(define lambda-counter 0)
(define lambda-level 0)
(define opt-counter 0)
		
(define prologue1 (string-append scheme.s "     

section .bss

extern exit, printf, scanf, malloc

section .text 
     align 16 
     global main\n\n"
))

(define prologue2 (string-append
    "section .text\n\n"
    "main:\n\n"
    "\tpush 0\n"
    "\tpush 0\n"
    "\tNEW_MALLOC 8, rbx\n"     
    "\tmov qword [rbx], -1\n"   
    "\tmov rax, rbx\n"
    "\tNEW_MALLOC 8, rbx\n"
    "\tmov qword [rbx], rax\n"
    "\tpush rbx\n"
    "\tpush 0\n"
    "\tpush -1\n"
    ))

(define epilogue "

    push rax
    call write_sob_if_not_void
    add rsp, 1*8

    pop    rbp
        
    push 0
    call exit
    
error_apply_non_closure_text:
    dq \"attempt to apply non procedure\", 10, 0

ERROR_APPLY_NON_CLOSURE:
    PRINTFF error_apply_non_closure_text
    push -1
    call exit

")

(define const-void?
	(lambda (v)
		(eq? v (void))))

(define find-name-in-const-table
    (lambda (value)
        (caar (filter (lambda (pair) (equal? value (cadr pair))) const-table))))
        
(define find-name-in-symbol-table
    (lambda (value)
        "4838"))

(define code-gen-const
    (lambda (expr)
        (let* ((value (cadr expr))
               (name (find-name-in-const-table value)))
            (string-append "\tmov rax, [" name "]\n")
        )))
		
(define code-gen-if3
    (lambda (expr)
        (let* ((test (list (list-ref expr 1)))
                (then (list (list-ref expr 2)))
                (els (list (list-ref expr 3)))
                (test-code (code-gen test))
                (then-code (code-gen then))
                (else-code (code-gen els))
                (if-count (format "~a" if3-counter))
                (if-code
                    (string-append
                        test-code
                        "\tcmp rax, SOB_FALSE\n"
                        "\tje ELSE" if-count "\n"
                        "\tTHEN" if-count ":\n\t"
                        then-code
                        "\tjmp END_IF" if-count "\n"
                        "\tELSE" if-count ":\n\t"
                        else-code
                        "\tEND_IF" if-count ":\n\n")))
                        
                    (set! if3-counter (+ if3-counter 1))
                    if-code)))
			
(define code-gen-seq
    (lambda (expr)
        (let* ((exprs (cadr expr)))
            (fold-left
                (lambda (acc x) (string-append acc (code-gen (list x))))
                ""
                exprs))))
				
(define code-gen-or
    (lambda (expr)
        (let* ((exprs (cadr expr))
               (or-count (format "~a" or-counter))
               (or-code
                    (string-append
                        "\tmov rax, SOB_FALSE\n"
                        (fold-left
                            (lambda (acc x)
                                (string-append
                                    acc
                                    (code-gen (list x))
                                    "\tcmp rax, SOB_FALSE\n"
                                    "\tjne END_OR" or-count "\n"
                                    ))
                            ""
                            exprs)
                        "\tEND_OR" or-count ":\n\n")))
            (set! or-counter (+ or-counter 1))
                    or-code)))
            
(define code-gen-lambda-simple
    (lambda (expr)
        (let* ((lambda-count (format "~a" lambda-counter))
               (params (list-ref expr 1))
               (body (list (list-ref expr 2)))
               (n (length params))
               (m lambda-level)
               (rbx_malloc (number->string (* 8 (+ m 1))))
               (rcx_malloc (number->string (* 8 n)))
               (lambda-code
                (begin (set! lambda-counter (+ lambda-counter 1))
                    (string-append
                        "\tNEW_MALLOC " rbx_malloc ", rbx\n"
                        "\tpush rbx\n"
                        "\tNEW_MALLOC " rcx_malloc ", rcx\n"
                        "\tNEW_MALLOC 16, rax\n"
                        "\tpop rbx\n"
                        
                        "\tmov rdx, 0\n"
                        (fold-left
                            (lambda (acc x)
                            (string-append
                                acc
                                "\tmov rdi, " (find-name-in-symbol-table x) "\n"
                                "\tmov qword [rcx+rdx*8], rdi\n"
                                "\tinc rdx\n"
                            ))
                            ""
                            params)                                            
                        
                        "\tmov qword [rbx], rcx\n"
                        
                        "\tmov rdx, 0\n"
                        "\tLOOP_ENV" lambda-count ":\n"
                        "\tcmp rdx, " (number->string m) "\n"
                        "\tje LOOP_ENV_END" lambda-count "\n"
                        
                        "\tcmp rsp, rbp\n"
                        "\tjne EXTRA_ARGS_ON_STACK_" lambda-count "\n"
                        "\tmov rdi, qword [rsp + 2*8]\n"
                        "\tjmp EXTRA_ARGS_ON_STACK_" lambda-count ".CONT\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ":\n"
                        "\tmov rdi, qword [rsp]\n"
                        "\tadd rdi, 3\n"
                        "\tmov rdi, qword [rsp + rdi*8]\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ".CONT:\n"
                        "\tmov rdi, qword [rdi + rdx*8]\n"
                        "\tmov rsi, rdx\n"
                        "\tinc rsi\n"
                        "\tmov qword [rbx + rsi*8], rdi\n"
                        
                        "\tinc rdx\n"
                        "\tjmp LOOP_ENV" lambda-count "\n"
                        "\tLOOP_ENV_END" lambda-count ":\n"
                        
                        "\tmov rdx, B" lambda-count "\n"
                        "\tMAKE_LITERAL_CLOSURE rax, rbx, rdx\n"
                        "\tmov rax, [rax]\n"
                        "\tjmp L" lambda-count "\n"
                        
                        "\tB" lambda-count ":\n"
                        "\tpush rbp\n"
                        "\tmov rbp, rsp\n"
                        
                        "\tmov rcx, 4\n"
                        "\tLOOP_FILL_ENV_" lambda-count ":\n"
                        "\tGET_FROM_STACK rbx, rcx\n"
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tsub rcx, 4\n"
                        "\tmov qword [rdx+rcx*8], rbx\n"
                        "\tadd rcx, 5\n"
                        "\tcmp rcx, " (number->string (+ n 4)) "\n"
                        "\tjne LOOP_FILL_ENV_" lambda-count "\n"
                        
                        
                        (code-gen body)
                        "\tleave\n"
                        "\tret\n"
                        "\tL" lambda-count ":\n\n"
                        
                        
                        ))))
                    
            (set! lambda-level (- lambda-level 1))
            lambda-code)))
            
            
(define code-gen-applic
    (lambda (expr)
        (let* ((proc (list (list-ref expr 1)))
               (args (list-ref expr 2))
               (m (number->string (length args)))
               (reversed-args (reverse args))
               (applic-code
                    (string-append
                        (fold-left
                            (lambda (acc x)
                            (string-append
                                acc
                                (code-gen (list x))
                                "\tpush rax\n"
                            ))
                            ""
                            reversed-args) 
                        
                        "\tpush " m "\n"
                        
                        (code-gen proc)
                        "\tpush rax\n"
                        "\tTYPE rax\n"
                        "\tcmp rax, T_CLOSURE\n"
                        "\tjne ERROR_APPLY_NON_CLOSURE\n"
                        "\tpop rax\n"
                        
                        "\tmov rbx, rax\n"
                        "\tCLOSURE_ENV rbx\n"
                        "\tpush rbx\n"
                        "\tCLOSURE_CODE rax\n"
                        "\tcall rax\n"
                        
                        "\tadd rsp, 8*2\n"
                        "\tadd rsp, 8*" m "\n")))

            applic-code)))
            
(define code-gen-lambda-opt
    (lambda (expr)
        (let* ((lambda-count (format "~a" lambda-counter))
               (params (list-ref expr 1))
               (p-list (list-ref expr 2))
               (body (list (list-ref expr 3)))
               (n (length params))
               (m lambda-level)
               (sobNil (find-name-in-const-table '()))
               (rbx_malloc (number->string (* 8 (+ m 1))))
               (rcx_malloc (number->string (+ (* 8 n) 1)))
               (lambda-code
                (begin (set! lambda-counter (+ lambda-counter 1))
                    (string-append
                        "\tNEW_MALLOC " rbx_malloc ", rbx\n"
                        "\tpush rbx\n"
                        "\tNEW_MALLOC " rcx_malloc ", rcx\n"
                        "\tNEW_MALLOC 16, rax\n"
                        "\tpop rbx\n"
                        
                        "\tmov rdx, 0\n"
                        (fold-left
                            (lambda (acc x)
                            (string-append
                                acc
                                "\tmov rdi, " (find-name-in-symbol-table x) "\n"
                                "\tmov qword [rcx+rdx*8], rdi\n"
                                "\tinc rdx\n"
                            ))
                            ""
                            params)
                            
                        "\tmov rdi, " (find-name-in-symbol-table p-list) "\n"
                        "\tmov qword [rcx+rdx*8], rdi\n"
                        
                        "\tmov qword [rbx], rcx\n"
                        
                        "\tmov rdx, 0\n"
                        "\tLOOP_ENV" lambda-count ":\n"
                        "\tcmp rdx, " (number->string m) "\n"
                        "\tje LOOP_ENV_END" lambda-count "\n"
                        
                        "\tcmp rsp, rbp\n"
                        "\tjne EXTRA_ARGS_ON_STACK_" lambda-count "\n"
                        "\tmov rdi, qword [rsp + 2*8]\n"
                        "\tjmp EXTRA_ARGS_ON_STACK_" lambda-count ".CONT\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ":\n"
                        "\tmov rdi, qword [rsp]\n"
                        "\tadd rdi, 3\n"
                        "\tmov rdi, qword [rsp + rdi*8]\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ".CONT:\n"
                        "\tmov rdi, qword [rdi + rdx*8]\n"
                        "\tmov rsi, rdx\n"
                        "\tinc rsi\n"
                        "\tmov qword [rbx + rsi*8], rdi\n"
                        
                        "\tinc rdx\n"
                        "\tjmp LOOP_ENV" lambda-count "\n"
                        "\tLOOP_ENV_END" lambda-count ":\n"
                        
                        "\tmov rdx, B" lambda-count "\n"
                        "\tMAKE_LITERAL_CLOSURE rax, rbx, rdx\n"
                        "\tmov rax, [rax]\n"
                        "\tjmp L" lambda-count "\n"
                        
                        
                        
                        "\tB" lambda-count ":\n"
                        "\tpush rbp\n"
                        "\tmov rbp, rsp\n"
                        
                        "\tmov rcx, 4\n"
                        "\tLOOP_FILL_ENV_" lambda-count ":\n"
                        "\tGET_FROM_STACK rbx, rcx\n"
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tsub rcx, 4\n"
                        "\tmov qword [rdx+rcx*8], rbx\n"
                        "\tadd rcx, 5\n"
                        "\tcmp rcx, " (number->string (+ n 4)) "\n"
                        "\tjne LOOP_FILL_ENV_" lambda-count "\n"
                        
                        "\tGET_FROM_STACK rcx, 3\n"
                        "\tsub rcx, " (number->string n) "\n"
                        
                        ;;This is for lambda-opt that attempts creating T_PAIR
                        "\tcmp rcx, 0\n"
                        "\tje NO_LOOP_FILL_ENV_OPT_" lambda-count "\n"
                        
                        "\tFILL_ENV_OPT_" lambda-count ":\n"
                        "\tmov rax, rcx\n"
                        "\tadd rax, 3\n"
                        "\tadd rax, " (number->string n) "\n"
                        "\tGET_FROM_STACK rdx, rax\n"
                        "\tNEW_MALLOC 8, rdi\n"
                        "\tmov qword [rdi], rdx\n"
                        "\tMAKE_LITERAL_PAIR_OPT rdi, " sobNil "\n"
                        
                        

                        "\tdec rcx\n"
                        "\tcmp rcx, 0\n"
                        "\tje PUT_LIST_IN_ENV_" lambda-count "\n"
                        
                        "\tLOOP_MAKE_PAIRS_" lambda-count ":\n"
                        "\tpush rax\n"
                        "\tmov rax, rcx\n"
                        "\tadd rax, 4\n"
                        "\tadd rax, " (number->string n) "\n"
                        "\tGET_FROM_STACK rdx, rax\n"
                        "\tNEW_MALLOC 8, rdi\n"
                        "\tmov qword [rdi], rdx\n"
                        "\tpop rax\n"
                        "\tmov rdx, rax\n"
                        "\tMAKE_LITERAL_PAIR_OPT rdi, rdx\n"
                        "\tdec rcx\n"
                        "\tcmp rcx, 0\n"
                        "\tjne LOOP_MAKE_PAIRS_" lambda-count "\n"
                        "\tjmp PUT_LIST_IN_ENV_" lambda-count "\n"

                        
                        "\tNO_LOOP_FILL_ENV_OPT_" lambda-count ":\n"
                        "\tNEW_MALLOC 8, rcx\n"
                        "\tmov qword [rcx], SOB_NIL\n"
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tmov rax, qword [rcx]\n"
                        "\tmov qword [rdx + 8*" (number->string n) "], rax\n"
                        "\tjmp DDD" lambda-count "\n"
                        
                        
                        "\tPUT_LIST_IN_ENV_" lambda-count ":\n"
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tmov rax, qword [rax]\n"
                        "\tmov qword [rdx + 8*" (number->string n) "], rax\n"
                        
                        "\tDDD" lambda-count ":\n"
                        
                        
                        (code-gen body)
                        "\tleave\n"
                        "\tret\n"
                        "\tL" lambda-count ":\n\n"
                        
                        
                        ))))
                    
            (set! lambda-level (- lambda-level 1))
            lambda-code)))
            
(define code-gen-pvar
    (lambda (expr)
        (let* ((index (number->string (list-ref expr 2))))
            (string-append
                "\tmov rbx, qword [rsp + 2*8]\n"
                "\tmov rbx, qword [rbx]\n"
                "\tmov rax, qword [rbx + " index "*8]\n"
                ;"\tmov rax, qword [rax]\n"
            ))))
            
(define code-gen-bvar
    (lambda (expr)
        (let* ((index (number->string (list-ref expr 3)))
               (major (number->string (list-ref expr 2))))
            (string-append
                "\tmov rbx, qword [rsp + 2*8]\n"
                "\tmov rbx, qword [rbx + " major "*8]\n"
                "\tmov rax, qword [rbx + " index "*8]\n"
            ))))
 
(define code-gen
    (lambda (expr)
        (fold-left
            (lambda (acc x)
                (string-append 
                    acc
                    (let* ((tag (car x)))
                        (cond
                            ((equal? tag 'const) (code-gen-const x))
                            ((equal? tag 'if3) (code-gen-if3 x))
                            ((equal? tag 'seq) (code-gen-seq x))
                            ((equal? tag 'or) (code-gen-or x))
                            ((equal? tag 'pvar) (code-gen-pvar x))
                            ((equal? tag 'bvar) (code-gen-bvar x))
                            ((equal? tag 'lambda-simple)
                                (begin
                                    (set! lambda-level (+ lambda-level 1))
                                    (code-gen-lambda-simple x)))
                            ((equal? tag 'lambda-opt)
                                (begin
                                    (set! lambda-level (+ lambda-level 1))
                                    (code-gen-lambda-opt x)))
                            ((equal? tag 'applic) (code-gen-applic x))
                            ((equal? tag 'tc-applic) (code-gen-applic x))
                            (else 1)))))
			""
			expr)))


(define dedupe  
    (lambda (e)
        (if (null? e) 
            '()
            (cons (car e) 
                  (dedupe 
                    (filter 
                        (lambda (x) 
                            (not (equal? x (car e)))) 
                        (cdr e)))))))

         
(define get-const-list
    (lambda (expr)
        (if (null? expr)
            (list '())
            (if (vector? (car expr))
                (append (list (car expr)) (get-const-vector (car expr)) (list (cdr expr)) (get-const-list (cdr expr)))
                (if (list? (car expr))
                    (append (list (car expr)) (get-const-list (car expr)) (list (cdr expr)) (list expr) (get-const-list (cdr expr)))
                    (append (list (car expr)) (list (cdr expr)) (list expr) (get-const-list (cdr expr))))))))
            
            
(define get-const-vector
    (lambda (expr)
        (let* ((lst (vector->list expr)))
            (if (null? lst)
                (list '())
                (if (vector? (car lst))
                    (append (list (car lst)) (get-const-vector (car lst)) (get-const-vector (list->vector (cdr lst))))
                    (if (list? (car lst))
                        (append (list (car lst)) (get-const-list (car lst)) (get-const-vector (list->vector (cdr lst))))
                    (append (list (car lst)) (get-const-vector (list->vector (cdr lst))))))))))
            
(define make-const-table-dup-nonTag
    (lambda (expr)
        (if (or (not (list? expr)) (empty? expr))
            '()
            (if (equal? (car expr) 'const)
                (if (vector? (cadr expr))
                    (append (list (cadr expr)) (get-const-vector (cadr expr)))
                    (if (list? (cadr expr))
                        (get-const-list (cadr expr))
                        (cdr expr)))
                    (fold-left (lambda (acc x) (append acc (make-const-table-dup-nonTag x))) '() expr)))))
                
(define make-const-table
    (lambda (expr)
        (set! const-counter 0)
        (set! const-table (fold-left 
            (lambda (acc x)
                (let ((name (string-append "c" (number->string const-counter))))
                    (set! const-counter (+ const-counter 1))
                    (append acc (list (list name x)))))
            '()
            (dedupe (append (make-const-table-dup-nonTag expr) (list '() #f #t (void))))))))
                
(define make-globalVar-table-dup-nonTag
    (lambda (expr)
        (if (or (not (list? expr)) (empty? expr))
            '()
            (if (and (equal? (car expr) 'fvar) (= (length expr) 2))
                (cdr expr)
                (fold-left (lambda (acc x) (append acc (make-globalVar-table-dup-nonTag x))) '() expr)))))
                
                
(define make-globalVar-table
    (lambda (expr)
        (fold-left 
            (lambda (acc x)
                (let ((name (string-append "fvar" (number->string fvar-counter))))
                    (set! fvar-counter (+ fvar-counter 1))
                    (append acc (list (list name x)))))
            '()
            (dedupe (make-globalVar-table-dup-nonTag expr)))))

(define helper '())
            
(define code-gen-const-table
    (lambda ()
        (let* ((prologue "section .data\nstart_of_data:\n"))
            (string-append
                prologue
                (fold-left
                    (lambda (acc x) 
                        (let* ((name (car x))
                               (value (cadr x)))
                            (cond
                                ((const-void? value)
                                    (string-append acc name ":\n\t dq SOB_VOID\n"))
                                ((boolean? value)
                                    (let ((bool -1))
                                        (if value (set! bool 1) (set! bool 0))
                                        (string-append acc name ":\n\t dq MAKE_LITERAL(T_BOOL, " (format "~a" bool) ")\n")))
                            
                                ((number? value)
                                (string-append acc name ":\n\t dq MAKE_LITERAL(T_INTEGER, " (number->string value) ")\n"))
                                
                                ((string? value)
                                (string-append acc name ":\n\t MAKE_LITERAL_STRING \"" value "\"\n"))
                                
                                ((char? value)
                                (string-append acc name ":\n\t dq MAKE_LITERAL(T_CHAR, '" (string value) "')\n"))
                                
                                ((null? value)
                                (string-append acc name ":\n\t dq SOB_NIL\n"))
                                
                                ((list? value)
                                    (let* ((my-name (find-name-in-const-table value))
                                           (car-name (find-name-in-const-table (car value)))
                                           (cdr-name (find-name-in-const-table (cdr value))))
                                        (string-append acc name ":\n\t dq MAKE_LITERAL_PAIR(" car-name ", " cdr-name ")\n")))
                                        
                                ((vector? value)
                                    (let* ((my-name (find-name-in-const-table value))
                                           (args-names (string-append (fold-left 
                                                        (lambda (vecAcc x)
                                                            (let* ((name (find-name-in-const-table x)))
                                                                (string-append vecAcc name ", ")))
                                               ""
                                               (vector->list value)) "\n")))
                                    (string-append acc my-name ":\n\t MAKE_LITERAL_VECTOR " args-names)))
                                    
                                )))
                    ""
                    const-table)
                "\n"))))
            

;;;;;;;;;;;;;;;;TEST AREA;;;;;;;;;;;;;;;;;;;                          
               
(define code2 (pipeline (file->list "test.scm")))
;(code-gen-const-table (make-const-table code2))
code2
;(make-const-table code2)

;(code-gen-const-table (parse1-3 "test.scm"))

;code2

;(make-globalVar-table code2)

;(parse1-3 "1-3/sexpr-parser.scm")	

;(make-const-table (parse1-3 "test.scm"))
;(make-globalVar-table (parse1-3 "test.scm"))


			

            
;(define code '((applic (lambda-simple (x y) (const 2)) ((const 3) (const 3)))))

;(dedupe (make-const-table code))
          
                
            
;(define const-table '((c1 1) (c2 2)))

;(code-gen-const-table const-table)

;c1:
;    db 1
;
;c2:
;    db 2
