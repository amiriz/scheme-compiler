section .data
        STACK_OVERFLOW:	 DB "Error: Operand Stack Overflow", 10, 0                   ; message if overflow error
        INSUFFICIENT:	 DB "Error: Insufficient Number of Arguments on Stack", 10, 0; message if Insufficient error
        LARGE_EXPONENT:  DB "exponent too large", 10, 0
        CALC:            DB ">>calc: ", 0 
        PRINTLN:         DB "", 10, 0
        ITERATION:       DB "iteration", 10, 0
        
;------------------------------------------------- macros ------------------------------------------------------

%macro ADD_5_IF_ODD 2                  ; add 5 to %2 if %1 contain an odd digit    
    cmp %1, 1                          ; if (%1 contains 1)
    je %%ADD_5
    cmp %1, 3                          ; if (%1 contains 3)
    je %%ADD_5
    cmp %1, 5                          ; if (%1 contains 5)
    je %%ADD_5
    cmp %1, 7                          ; if (%1 contains 7)
    je %%ADD_5
    cmp %1, 9                          ; if (%1 contains 9)
    je %%ADD_5
    jmp %%FINISH_ADD_5
    
    %%ADD_5:
        add %2, 5                      ; %2 <- %2 + 5
    
    %%FINISH_ADD_5:
%endmacro

%macro PRINT_EMPTY_LINE 0              ; print empty line    
    pushad                             ; save registers of after C function
    push PRINTLN                       ; push arg to print
    call printf                        ; function call
    add esp, (4 * 1)                   ; esp <- esp + 4
    popad                              ; pop registers after C function
%endmacro 

%macro CLEAR_STACK 0                   ; clear the stack from junc 
    mov dword [STACK], 0
    mov dword [STACK+4], 0
    mov dword [STACK+8], 0
    mov dword [STACK+12], 0
    mov dword [STACK+16], 0
%endmacro    

%macro CHECK_IF_INSUFFICIENT 1         ; gets the number of the elements in the stack (the arg must be STACK_OFFSET!)
    cmp %1, 0                          ; if (stack.size > 0)
    jne %%END_INSUFFICIENT
    pushad                             ; save registers of after C function
    push INSUFFICIENT                  ; the actual string for fprintf
    push dword [stderr]                ; the stream for fprintf
    call fprintf                       ; function call
    add esp, (4 * 2)                   ; esp <- esp + 8
    popad                              ; pop registers after C function
    popad                              ; Restore upper function registers
    mov esp, ebp                       ; Function exit code
    pop ebp
    ret

    %%END_INSUFFICIENT:
%endmacro 

%macro CHECK_IF_INSUFFICIENT_2 1       ; gets the number of the elements in the stack (the arg must be STACK_OFFSET!)
    cmp %1, 8                          ; if (stack.size >= 2)
    jge %%END_INSUFFICIENT_2
    pushad                             ; save registers of after C function
    push INSUFFICIENT                  ; the actual string for fprintf
    push dword [stderr]                ; the stream for fprintf
    call fprintf                       ; function call
    add esp, (4 * 2)                   ; esp <- esp + 8
    popad                              ; pop registers after C function
    popad                              ; Restore upper function registers
    mov esp, ebp                       ; Function exit code
    pop ebp
    ret

    %%END_INSUFFICIENT_2:
%endmacro 
   
%macro EXPONENT_TOO_LARGE 0            ; prints the error message
    pushad                             ; save registers of after C function
    push LARGE_EXPONENT                ; the actual string for fprintf
    push dword [stderr]                ; the stream for fprintf
    call fprintf                       ; function call
    add esp, (4 * 2)                   ; esp <- esp + 8
    popad                              ; pop registers after C function
%endmacro
   
%macro CHECK_IF_STACK_OVERFLOW 2       ; gets the number of the elements in the stack (the arg must be STACK_OFFSET!) and a boolean (0 for my_calc function and 1 for other functions))
    cmp %1, 20                         ; if (stack.size < 5)
    jl %%LESS_THAN_FIVE_ELEMENTS
    pushad                             ; save registers of after C function
    push STACK_OVERFLOW                ; the actual string for fprintf
    push dword [stderr]                ; the stream for fprintf
    call fprintf                       ; function call
    add esp, (4 * 2)                   ; esp <- esp + 8
    popad                              ; pop registers after C function
    
    cmp %2, 0                          ; if (the macro was called not from my_calc)
    je GET_NEW_LINE        
    popad                              ; Restore upper function registers
    mov esp, ebp                       ; Function exit code
    pop ebp
    ret
    
    %%LESS_THAN_FIVE_ELEMENTS:
%endmacro
        
%macro NEW_MALLOC 2                    ; gets 2 args (allocate size, register of pointer)
    push ecx                           ; save register before malloc
    push edx                           ; save register before malloc
    push esi                           ; save register before malloc
    push eax                           ; save register before malloc
    push %1                            ; push allocate size
    call malloc                        ; function call
    add esp, (4 * 1)                   ; esp <- esp + 4
    mov %2, eax                        ; arg2 <- the return pointer of malloc
    pop eax                            ; pop register after malloc 
    pop esi                            ; pop register after malloc
    pop edx                            ; pop register after malloc
    pop ecx                            ; pop register after malloc
%endmacro 

%macro PRINT 2                         ; gets format and an argument
    pushad                             ; save registers of after C function
    push %2                            ; push arg to print
    push %1                            ; push print format
    call printf                        ; function call
    add esp, (4 * 2)                   ; esp <- esp + 8
    popad                              ; pop registers after C function
%endmacro

%macro FGETS 3                         ; (buffer, bufferSize, stream) (gets a new line)
    pushad                             ; save registers of after C function
    push dword %3                      ; third arg of fgets (stream)
    push dword %2                      ; second arg of fgets (size of the buffer)
    push dword %1                      ; first arg of fgets (pointer to the buffer)
    call fgets                         ; function call
    add esp, (4 * 3)                   ; esp <- esp + 12
    popad                              ; pop registers after C function
%endmacro

;---------------------------------------------- end of macros ----------------------------------------------------       

section .rodata
        SIZE_OF_STACK: equ 20                          ; the size of the stuck in bytes
        SIZE_OF_BLOCK: equ 9                           ; the size of a block in bytes
        INT_FORMAT:      DB "%d", 0                    ; format int
        STRING_FORMAT:   DB "%s", 0                    ; format string
        HEXA_FORMAT:     DB "%x", 0                    ; hexa format (print only 2 digits)
        
section .bss

    LINE:               resb 82                        ; buffer to save the input lines
    STACK:              resb SIZE_OF_STACK             ; buffer to the save the stack
    STACK_OFFSET:       resb 4                         ; counter to save number of elements in the stack
    OPERATION_COUNTER:  resb 1                         ; counter to save number of operations in my_calc
    EVEN_OR_ODD_INPUT:  resb 4                         ; 1 for even input, 0 for odd input
    DIV_COUNTER:        resb 4                         ; counter to know much "devide by 2" to make
    DIV_DEVIDER:        resb 4                         ; will contain the number 2
    PRINT_BUT_DONT_POP: resb 4                         ; flag for using POP_AND_PRINT with out poping (debug)
    
section .text 
     align 16 
     global main 
     extern printf 
     extern fprintf 
     extern malloc 
     extern free
     extern fgets 
     extern stderr 
     extern stdin 
     extern stdout 

main:
        push    ebp
        mov     ebp, esp                               ; Entry code - set up ebp and esp
        pushad                                         ; Save registers
        call my_calc
        mov ecx, 0                                     ; ecx <- 0
        mov cl, byte [esp - 4]                         ; cl <- operation number of my_calc !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! not good!!!!!!!!!!!!
        PRINT INT_FORMAT ,ecx                          ; print the number of operations
        PRINT_EMPTY_LINE
        
        popad                                          ; Restore registers
        mov esp, ebp                                   ; Function exit code
        pop ebp
        ret
            
CALL_POP_AND_PRINT:
    call POP_AND_PRINT
    jmp GET_NEW_LINE
    
CALL_ADDITION:
    call ADDITION
    jmp GET_NEW_LINE
    
CALL_DUPLICATE:
    call DUPLICATE
    jmp GET_NEW_LINE
    
CALL_SHIFT_RIGHT:
    call SHIFT_RIGHT
    jmp GET_NEW_LINE
    
CALL_SHIFT_LEFT:
    call SHIFT_LEFT
    jmp GET_NEW_LINE

my_calc:
    push ebp
    mov ebp, esp                                   ; Entry code - set up ebp and esp
    pushad                                         ; Save registers
    mov byte [OPERATION_COUNTER], 0                ; [OPERATION_COUNTER] <- 0
    mov byte [STACK_OFFSET], 0                     ; [STACK_OFFSET] <- 0
    mov byte [PRINT_BUT_DONT_POP], 0               ; PRINT_BUT_DONT_POP flag <- 0
    CLEAR_STACK                                    ; clear the cells in the stack
                        
    GET_NEW_LINE:
        inc dword [OPERATION_COUNTER]              ; [OPERATION_COUNTER] ++
        PRINT STRING_FORMAT, CALC
        FGETS LINE, 82, [stdin]                    ; LINE <- new line from stdin
        
    READ_FIRST_CHAR_AND_JUMP:
        cmp byte [LINE], 'q'                              
        je QUIT
        cmp byte [LINE], '+'
        je CALL_ADDITION
        cmp byte [LINE], 'p'
        je CALL_POP_AND_PRINT
        cmp byte [LINE], 'd'
        je CALL_DUPLICATE
        cmp byte [LINE], 'r'
        je CALL_SHIFT_RIGHT
        cmp byte [LINE], 'l'
        je CALL_SHIFT_LEFT

        jmp NUMBER_INPUT
        
    NUMBER_INPUT:
        mov edx, 0 
        CHECK_IF_STACK_OVERFLOW byte [STACK_OFFSET], edx
        mov edx, LINE
        
        EVEN_CHECKER:
            cmp byte [edx], 10
            je EVEN_INPUT
            inc edx
            jmp ODD_CHECKER
            
        ODD_CHECKER:
            cmp byte [edx], 10
            je ODD_INPUT
            inc edx
            jmp EVEN_CHECKER
            
            EVEN_INPUT:
                mov byte [EVEN_OR_ODD_INPUT], 1
                jmp AFTER_EVEN_OR_ODD_CHECK
            
            ODD_INPUT:
                mov byte [EVEN_OR_ODD_INPUT], 0
        
        AFTER_EVEN_OR_ODD_CHECK:
        mov ebx, 0                                 ; ebx <- 0
        mov esi, 0                                 ; esi <- 0 (for the SKIP_FIRST_BLOCK)
        mov ecx, dword [STACK_OFFSET]              ; ecx <- [STACK_OFFSET]
        mov edx, LINE                              ; edx <- pointer to the start of LINE
        dec edx
        
        ZERO_CLEANER:
            inc edx                                ; [edx] <- next digit from the input 
            cmp byte [edx], '0'                    ; check if [edx] points on 0 
            je ZERO_CLEANER                        ; repit this zero cleaning
            cmp byte [edx], 10                     ; check if the input number is 0
            je MAKE_ZERO_LIST                      ; make a list with one "00,00000000,00000000" block 
            jmp MAKE_NUMBER_LIST                   ; make a regular number list
        
        MAKE_ZERO_LIST:            
            dec edx                                ; [edx] <- the last zero in the buffer
            NEW_MALLOC SIZE_OF_BLOCK, edi          ; edi <- pointer to the start of the block 
            mov byte [edi], 0                      ; [edi] <- 0 (to the value byte)   
            mov dword [edi + 1], 0                 ; make a null next_pointer        
            mov dword [edi + 5], STACK             ; [edi + 5] (prev_pointer of the new zero block) <- bottom of the stack
            add [edi + 5], ecx                     ; [edi + 5] (prev_pointer of the new zero block) <- top of the stack 
            mov dword [STACK + ecx], edi           ; [STACK + ecx] (top of the stack) <- edi (pointer to the new zero block)                                 
            jmp INC_STACK_OFFSET                   ; inc the stack offset and move to the next input
            
        MAKE_NUMBER_LIST:
            cmp byte [edx], 10                     ; check if the current digit is \n (even input)
            je INC_STACK_OFFSET                    ; inc the stack offset and move to the next input
            
            NEW_MALLOC SIZE_OF_BLOCK, edi          ; edi <- pointer to the start of the new block 
            mov eax, edi                           ; eax <- data of the new block (pointer) 
            inc eax                                ; eax <- next_pointer of the new block
            
            ;make arrows:
            mov [eax], ebx                         ; [eax] (next_poiner of the new block) <- data of the old block (pointer) (arrow 1)
            mov dword [edi + 5], STACK             ; [edi + 5] (prev_pointer of the new block) <- bottom of the stack
            add [edi + 5], ecx                     ; [edi + 5] (prev_pointer of the new block) <- top of the stack (arrow 2)
            mov dword [STACK + ecx], edi           ; top of the stack (pointer) <- data of the new block (arrow 3)
            cmp esi, 0                             ; if (this is first block)
            je SKIP_FIRST_BLOCK
            mov [ebx + 5], edi                     ; [ebx + 5] (prev_poiner of the old block) <- data of the new block (pointer) (arrow 4)
            
            
            SKIP_FIRST_BLOCK:
                mov esi, 1                         ; esi <- 1
                mov eax, 0                         ; eax <- 0
                mov al, byte [edx]                 ; al <- the first digit form LINE
                sub al, 30h                        ; make first digit appear in number ascii (not hexa)
                cmp byte [edx + 1], 10             ; check if the next digit is \n (odd input)
                je SKIP_SHL                        ; jump because [al] is already ok   
                cmp byte [EVEN_OR_ODD_INPUT], 0
                je SKIP_SHL
                sub byte [edx + 1], 30h            ; make second digit appear in number ascii (not hexa)
                shl al, 4                          ; move the digit into the msb
                add al, byte [edx + 1]             ; al <- al + the second digit form LINE
            
            SKIP_SHL:
                mov byte [edi], al                 ; first byte of the block <- the digits
                mov ebx, edi                       ; ebx <- pointer to the current block 
                cmp byte [edx + 1], 10             ; check again if the socond digit is \n (odd input)
                je INC_STACK_OFFSET
                cmp byte [EVEN_OR_ODD_INPUT], 1
                je MOVE_2_DIGITS
                mov byte [EVEN_OR_ODD_INPUT], 1
                add edx, 1                         ; [edx] <- next digit from the line 
                jmp MAKE_NUMBER_LIST
                
                MOVE_2_DIGITS:
                add edx, 2                         ; [edx] <- next digit from the line 
                jmp MAKE_NUMBER_LIST
        
        INC_STACK_OFFSET:
            add dword [STACK_OFFSET], 4            ; [STACK_OFFSET]++
            jmp GET_NEW_LINE
                    
ADDITION:
    push ebp
    mov ebp, esp                                   ; Entry code - set up ebp and esp
    pushad                                         ; Save registers
    CHECK_IF_INSUFFICIENT_2 byte [STACK_OFFSET]
    mov ecx, STACK                                 ; ecx <- pointer to the bottom of the stack 
    add ecx, [STACK_OFFSET]                        ; ecx <- pointer to the top of the stack 
    sub ecx, 4                                     ; ecx <- pointer to below of the top of the stack (last list)
    mov ebx, dword [ecx]                           ; ebx <- pointer to the start of the last list
    sub ecx, 4                                     ; ecx <- pointer to "2 below" of the top of the stack ("before last" list)
    mov ecx, dword [ecx]                           ; ecx <- pointer to the start of the "before last" list
    mov eax, 0                                     ; al (sum) <- 0
    mov edx, 0                                     ; dl (carry) <- 0
    
    SUM_TILL_THE_END_OF_A_LIST:
        mov eax, 0
        mov al, byte [ecx]                         ; al (sum) <- data of the block in list 1 ("before last" list)
        add al, dl                                 ; al (sum) <- al + dl (carry)
        daa                                        ; decimal adjust to the sum
        add al, byte [ebx]                         ; al (sum) <- al + data of the block in list 2 (last list)
        daa                                        ; decimal adjust to the sum
        mov dl, 0                                  ; dl (carry) <- 0
        jnc NO_CARRY 
        mov dl, 1                                  ; dl (carry) <- 1
        
        NO_CARRY:
            mov byte [ecx], al                     ; [ecx] (data of the block in list 1) <- al (sum) 
            cmp dword [ecx + 1], 0                 ; if (the block in list 1 is the last block in the list)
            je LIST_1_ENDS                         ; check the second list
            jmp LIST_1_CONTINUE
        
            LIST_1_ENDS:
                cmp dword [ebx + 1], 0             ; if (the block in list 2 is the last block in the list)
                je FINISH_SUMMING_CHECK_CARRY      ; stop summing blocks data and check the carry
                jmp ONLY_LIST_2_CONTINUE           ; list 2 continue
            
            LIST_1_CONTINUE:
                cmp dword [ebx + 1], 0             ; if (the block in list 2 is the last block in the list)
                je ROLL_CARRY_TILL_THE_END         ; stop summing blocks data and check the carry
                
                mov ecx, dword [ecx + 1]           ; current block in list 1 <- next block of list 1
                mov ebx, dword [ebx + 1]           ; current block in list 2 <- next block of list 2
                jmp SUM_TILL_THE_END_OF_A_LIST     ; sum the next blocks of the lists
    
    ONLY_LIST_2_CONTINUE:
        mov ebx, dword [ebx + 1]                   ; current block in list 2 <- next block of list 2
        mov dword [ecx + 1], ebx                   ; [ecx + 1] (next_pointer of the last block in list 1) <- ebx (current block of list 2)
        mov dword [ebx + 5], ecx                   ; [ebx + 5] (prev_pointer of the current block in list 2) <- ecx (current block of list 1)
        
    ROLL_CARRY_TILL_THE_END:
        mov ecx, dword [ecx + 1]                   ; current block <- next block
        mov al, byte [ecx]                         ; al <- [ecx] (the digits of current block) 
        add al, dl                                 ; al (the digits of current block) <- al + dl (carry)
        daa                                        ; decimal adjust to the sum
        mov byte [ecx], al                         ; [ecx] (real data of current block) <- al (data + carry)
        jnc NO_NEW_CARRY                           ; if (there is no new carry from the addition above - go to NO_NEW_CARRY)
        cmp dword [ecx + 1], 0                     ; if (current block is the last block)
        je FINISH_SUMMING_CHECK_CARRY           
        jmp ROLL_CARRY_TILL_THE_END
        
        NO_NEW_CARRY:
        mov dl, 0                                  ; dl (carry) <- 0
        cmp dword [ecx + 1], 0                     ; if (current block is the last block)
        je FINISH_SUMMING_CHECK_CARRY
        jmp ROLL_CARRY_TILL_THE_END            
        
    FINISH_SUMMING_CHECK_CARRY:
        sub dword [STACK_OFFSET], 4                ; stack offset <- stack offset - 4
        cmp dl, 0                                  ; if (there is no carry)
        je FINISH_ADDING                           ; finish
        
        NEW_MALLOC SIZE_OF_BLOCK, edi              ; edi <- pointer to the start of the new block 
        mov byte [edi], 1                          ; [edi] <- 1 (to the value byte)   
        mov dword [edi + 1], 0                     ; make a zero next_pointer        
        mov dword [edi + 5], ecx                   ; [edi + 5] (prev_pointer of the new block) <- last block of the list 
        mov dword [ecx + 1], edi                   ; [ecx + 5] (next_pointer of the last block in the list) <- edi (pointer to the new block)                                 
        
        FINISH_ADDING:
        popad                                      ; Restore registers
        mov esp, ebp                               ; Function exit code
        pop ebp
        ret
    
DUPLICATE:
    push ebp
    mov ebp, esp                                   ; Entry code - set up ebp and esp
    pushad                                         ; Save registers
    CHECK_IF_INSUFFICIENT byte [STACK_OFFSET] 
    mov edx, 1                                     ; edx <- 1
    CHECK_IF_STACK_OVERFLOW byte [STACK_OFFSET], edx
    mov esi, STACK                                 ; esi <- pointer to the bottom of the stack 
    add esi, [STACK_OFFSET]                        ; esi <- pointer to the top of the stack 
    sub esi, 4                                     ; esi <- pointer to below of the top of the stack (upper list)
    mov edx, dword [esi]                           ; edx <- pointer the first block in the (below of the) top of the stack 
    add esi, 4                                     ; esi <- pointer to the top of the stack 
    
    mov eax, 0                                     ; eax <- 0
    NEW_MALLOC SIZE_OF_BLOCK, edi                  ; edi <- pointer to the start of the new block 
    mov al, byte [edx]                             ; al <- the digits of list 1's block
    mov byte [edi], al                             ; [edi] (the new block) <- al
    mov dword [esi], edi                           ; [esi] (top of the stack) <- edi (pointer to the start of the block)
    mov dword [edi + 5], esi                       ; [edi + 5] (the prev_pointer of the new block) <- pointer the the top of the stack
    
    COPY_NEW_BLOCK:
        mov eax, 0                                 ; eax <- 0
        cmp dword [edx + 1], 0                     ; if (the next_pointer of the block in list 1 is 0)
        je NO_MORE_BLOCKS_TO_COPY
        mov edx, dword [edx + 1]                   ; edx <- next block from list 1
        NEW_MALLOC SIZE_OF_BLOCK, ebx              ; ebx <- pointer to the start of the new block 
        mov al, byte [edx]                         ; al <- the digits of list 1's block
        mov byte [ebx], al                         ; [ebx] (the new block) <- al
        mov dword [edi + 1], ebx                   ; [edi + 1] (the next_pointer of the previous block) <- pointer to the current block
        mov dword [ebx + 5], edi                   ; [ebx + 5] (the prev_pointer of the current block) <- edi (the previous block)
        mov edi, ebx                               ; edi <- ebx (for edi to save a pointer to the old block in the next iteration)
        jmp COPY_NEW_BLOCK
        
    NO_MORE_BLOCKS_TO_COPY:
    mov byte [ebx + 1], 0                          ; [ebx + 1] (next_pointer of the last block in the new list) <- 0
    add dword [STACK_OFFSET], 4                    ; [STACK_OFFSET]++    
    popad                                          ; Restore registers
    mov esp, ebp                                   ; Function exit code
    pop ebp
    ret
        
POP_AND_PRINT:
    push ebp
    mov ebp, esp                                   ; Entry code - set up ebp and esp
    pushad                                         ; Save registers
    CHECK_IF_INSUFFICIENT byte [STACK_OFFSET] 
    mov esi, STACK                                 ; esi <- pointer to the bottom of the stack 
    add esi, [STACK_OFFSET]                        ; esi <- pointer to the top of the stack 
    sub esi, 4                                     ; esi <- pointer to below of the top of the stack (last list)
    mov edx, dword [esi]                           ; edx <- pointer to the start of the list
    
    GO_TO_LIST_END:
        cmp dword [edx + 1], 0                     ; if (current block is the last block)
        je PRINT_FIRST                             ; start to print
        mov edx, dword [edx + 1]                   ; edx <- pointer to the next block
        jmp GO_TO_LIST_END                         ; move to the next block

    PRINT_FIRST:
        mov ecx, 0                                 ; ecx <- 0
        mov cl, byte [edx]                         ; cl <- digits of current block
        PRINT HEXA_FORMAT, ecx                     ; print digits of current block
        
        mov edx, dword [edx + 5]                   ; edx <- pointer to the previous block
        mov edi, dword [edx]                       ; edi <- previous block
        cmp edi, dword [esi]                       ; if (current block is the first block)
        je POP                                     ; finish the print and pop the list from the stack


    PRINT_TILL_THE_START_OF_LIST:
        mov ecx, 0                                 ; ecx <- 0
        mov cl, byte [edx]                         ; cl <- digits of current block
        shr cl, 4                                  ; cl <- the left nibble only
        PRINT HEXA_FORMAT, ecx                     ; print digits of current block
        mov ecx, 0                                 ; ecx <- 0
        mov cl, byte [edx]                         ; cl <- digits of current block
        shl cl, 4                                  ; cl <- the right nibble only * 10
        shr cl, 4                                  ; cl <- the right nibble only 
        PRINT HEXA_FORMAT, ecx                     ; print digits of current block
        
        mov edx, dword [edx + 5]                   ; edx <- pointer to the previous block
        mov edi, dword [edx]                       ; edi <- previous block
        cmp edi, dword [esi]                       ; if (current block is the first block)
        je POP                                     ; finish the print and pop the list from the stack
        jmp PRINT_TILL_THE_START_OF_LIST           ; mov the the previous block
        
    POP:
        PRINT_EMPTY_LINE
        cmp [PRINT_BUT_DONT_POP] ,dword 1          ; call this function by debug
        je SKIP_POP
        sub dword [STACK_OFFSET], 4                ; stack offset <- stack offset - 4
        SKIP_POP:
        mov [PRINT_BUT_DONT_POP] ,dword 0
        
    popad                                          ; Restore registers
    mov esp, ebp                                   ; Function exit code
    pop ebp
    ret       

SHIFT_RIGHT:
    push ebp
    mov ebp, esp                                   ; Entry code - set up ebp and esp
    pushad                                         ; Save registers
    CHECK_IF_INSUFFICIENT_2 byte [STACK_OFFSET]
    mov esi, STACK                                 ; esi <- pointer to the bottom of the stack 
    add esi, [STACK_OFFSET]                        ; esi <- pointer to the top of the stack 
    sub esi, 4                                     ; esi <- pointer to below of the top of the stack (upper list)
    mov edx, dword [esi]                           ; edx <- pointer the first block in the (below of the) top of the stack  
    cmp dword [edx + 1], 0                         ; if (edx is the the last block in the list)
    je LAST_BLOCK_SR
    EXPONENT_TOO_LARGE                             ; prints the error massage
    jmp FINISH_SHIFT_RIGHT
    
    LAST_BLOCK_SR:
        mov ecx, 0                                 ; ecx <- 0 (for the devision of the blocks)
        mov dl, byte [edx]                         ; dl <- the digits of the block (the exponent)
        mov byte [DIV_COUNTER], dl                 ; [DIV_COUNTER] <- dl
        mov edx, 0                                 ; edx <- 0
        sub dword [STACK_OFFSET], 4                ; [STACK_OFFSET]--
        sub esi, 4                                 ; esi (top of the stack) <- "below" the top of the stack 
        
        DIV_LIST_BY_2:
            mov edx, 0                             ; edx <- 0
            cmp byte [DIV_COUNTER], 0              ; if (the exponent is 0)
            je FINISH_SHIFT_RIGHT
            mov edi, dword [esi]                   ; edi <- pointer the first block in the (below the) top of the stack  
            mov byte [DIV_DEVIDER], 2              ; [DIV_DEVIDER] <- 2
            
            GO_TO_LIST_END_SR:
                cmp dword [edi + 1], 0             ; if (current block is the last block)
                je MSB_ZERO_CLEANER                ; start deviding the blocks          
                mov edi, dword [edi + 1]           ; edi <- pointer to the next block
                jmp GO_TO_LIST_END_SR              ; move to the next block
            
                MSB_ZERO_CLEANER:
                    cmp byte [edi], 0              ; if (the digits of the block isn't "00")
                    jne DIV_BLOCK_BY_2  
                    mov edi, dword [edi + 5]       ; edi <- previous block
                    mov dword [edi + 1], 0         ; [edi + 1] (next_poiner of the block) <- 0
                    jmp MSB_ZERO_CLEANER
                    
                DIV_BLOCK_BY_2:
                    ;left digit:
                    mov cl, byte [edi]             ; cl <- digits of the block
                    shr cl, 4                      ; cl <- only the right digit (the right nibble of the byte)
                    mov dh, 0                      ; dh (the addition to the next digit if this one is odd) <- 0
                    ADD_5_IF_ODD cl, dh            ; add 5 to dh if cl contain an odd digit
                    mov eax, 0                     ; eax <- 0
                    mov al, cl                     ; al <- cl
                    div byte [DIV_DEVIDER]         ; al <- ax (the digit) / 2
                    mov cl, al                     ; cl <- al
                    add cl, dl                     ; cl <- cl + dl (the possible addition of 5)
                    ;right digit:
                    mov bl, byte [edi]             ; bl <- digits of the block
                    shl bl, 4                      ; bl <- only the left digit (but bounded to the left)
                    shr bl, 4                      ; bl <- only the left digit (good)
                    mov dl, 0                      ; dl <- 0
                    ADD_5_IF_ODD bl, dl            ; add 5 to dl if bl contain an odd digit
                    mov eax, 0                     ; eax <- 0
                    mov al, bl                     ; al <- bl
                    div byte [DIV_DEVIDER]         ; al <- ax (the digit) / 2
                    mov bl, al                     ; bl <- al
                    add bl, dh                     ; bl <- bl + dh (the possible addition of 5)
                    ;after digits:
                    shl cl, 4                      ; cl <- cl bounded to the left
                    add cl, bl                     ; cl <- cl + bl
                    mov byte [edi], cl             ; [edi] (the digits in the block) <- the digits devided by 2
                    mov edi, dword [edi + 5]       ; edi <- pointer to the previous block
                    cmp esi, edi                   ; if (current blocl isn't the first block in the list)
                    jne DIV_BLOCK_BY_2
                    dec byte [DIV_COUNTER]         ; [DIV_COUNTER]--
                    jmp DIV_LIST_BY_2
        
    FINISH_SHIFT_RIGHT:
    popad                                          ; Restore registers
    mov esp, ebp                                   ; Function exit code
    pop ebp
    ret
    
SHIFT_LEFT:
    push ebp
    mov ebp, esp                                   ; Entry code - set up ebp and esp
    pushad                                         ; Save registers
    CHECK_IF_INSUFFICIENT_2 byte [STACK_OFFSET]
    mov esi, STACK                                 ; esi <- pointer to the bottom of the stack 
    add esi, [STACK_OFFSET]                        ; esi <- pointer to the top of the stack 
    sub esi, 4                                     ; esi <- pointer to below of the top of the stack (upper list)
    mov edx, dword [esi]                           ; edx <- pointer the first block in the (below of the) top of the stack  
    cmp dword [edx + 1], 0                         ; if (edx is the the last block in the list)
    je LAST_BLOCK
    EXPONENT_TOO_LARGE                             ; prints the error massage
    jmp FINISH_SHIFT_LEFT
    
    LAST_BLOCK:
        xor eax, eax
        mov bl, byte [edx]                         ; dl <- the digits of the block (the exponent)
        shr bl, 4
        mov al, 10
        mul bl
        mov bl, byte [edx]
        shl bl, 4 
        shr bl, 4
        add al ,bl
        mov dl, al
        sub dword [STACK_OFFSET], 4                ; [STACK_OFFSET]--
        
        DUP_ADD:
            cmp dl, 0                              ; if (dl == 0)
            je FINISH_SHIFT_LEFT
            call DUPLICATE
            call ADDITION
            PRINT STRING_FORMAT, ITERATION
            dec dl                                 ; dl <- dl - 1
            jmp DUP_ADD
    
    FINISH_SHIFT_LEFT:
        popad                                      ; Restore registers
        mov esp, ebp                               ; Function exit code
        pop ebp
        ret

QUIT:
    mov eax, 0                                     ; eax <- 0
    mov al, byte [OPERATION_COUNTER]               ; al <- return value (number of operations)
    push eax
    add esp, (4 * 1)
    
    popad                                          ; Restore registers
    mov esp, ebp                                   ; Function exit code
    pop ebp
    ret       
                 
END:    
    popad                                          ; Restore registers
    mov esp, ebp                                   ; Function exit code
    pop ebp
    ret