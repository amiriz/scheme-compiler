
(define code-gen-lambda-opt
    (lambda (expr)
        (let* ((lambda-count (format "~a" lambda-counter))
               (params (list-ref expr 1))
               (p-list (list-ref expr 2))
               (body (list (list-ref expr 3)))
               (n (length params))
               (m lambda-level)
               (sobNil (find-name-in-const-table '()))
               (rbx_malloc (number->string (* 8 (+ m 1))))
               (rcx_malloc (number->string (+ (* 8 n) 1)))
               (lambda-code
                (begin (set! lambda-counter (+ lambda-counter 1))
                    (string-append
                        "\tNEW_MALLOC " rbx_malloc ", rbx\n"
                        "\tpush rbx\n"
                        "\tNEW_MALLOC " rcx_malloc ", rcx\n"
                        "\tNEW_MALLOC 16, rax\n"
                        "\tpop rbx\n"
                        
                        "\tmov rdx, 0\n"
                        (fold-left
                            (lambda (acc x)
                            (string-append
                                acc
                                "\tmov rdi, " (find-name-in-symbol-table x) "\n"
                                "\tmov qword [rcx+rdx*8], rdi\n"
                                "\tinc rdx\n"
                            ))
                            ""
                            params)
                            
                        "\tmov rdi, " (find-name-in-symbol-table p-list) "\n"
                        "\tmov qword [rcx+rdx*8], rdi\n"
                        
                        "\tmov qword [rbx], rcx\n"
                        
                        "\tmov rdx, 0\n"
                        "\tLOOP_ENV" lambda-count ":\n"
                        "\tcmp rdx, " (number->string m) "\n"
                        "\tje LOOP_ENV_END" lambda-count "\n"
                        
                        "\tcmp rsp, rbp\n"
                        "\tjne EXTRA_ARGS_ON_STACK_" lambda-count "\n"
                        "\tmov rdi, qword [rsp + 2*8]\n"
                        "\tjmp EXTRA_ARGS_ON_STACK_" lambda-count ".CONT\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ":\n"
                        "\tmov rdi, qword [rsp]\n"
                        "\tadd rdi, 3\n"
                        "\tmov rdi, qword [rsp + rdi*8]\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ".CONT:\n"
                        "\tmov rdi, qword [rdi + rdx*8]\n"
                        "\tmov rsi, rdx\n"
                        "\tinc rsi\n"
                        "\tmov qword [rbx + rsi*8], rdi\n"
                        
                        "\tinc rdx\n"
                        "\tjmp LOOP_ENV" lambda-count "\n"
                        "\tLOOP_ENV_END" lambda-count ":\n"
                        
                        "\tmov rdx, B" lambda-count "\n"
                        "\tMAKE_LITERAL_CLOSURE rax, rbx, rdx\n"
                        "\tmov rax, [rax]\n"
                        "\tjmp L" lambda-count "\n"
                        
                        "\tB" lambda-count ":\n"
                        "\tpush rbp\n"
                        "\tmov rbp, rsp\n"
                        
                        "\tmov rcx, 4\n"
                        "\tLOOP_FILL_ENV_" lambda-count ":\n"
                        "\tGET_FROM_STACK rbx, rcx\n"
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tsub rcx, 4\n"
                        "\tmov qword [rdx+rcx*8], rbx\n"
                        "\tadd rcx, 5\n"
                        "\tcmp rcx, " (number->string (+ n 4)) "\n"
                        "\tjne LOOP_FILL_ENV_" lambda-count "\n"
                        
                        "\tGET_FROM_STACK rcx, 3\n"
                        "\tsub rcx, " (number->string n) "\n"
                        
                        ;;This is for lambda-opt that attempts creating T_PAIR
                        "\tcmp rcx, 0\n"
                        "\tje NO_LOOP_FILL_ENV_OPT_" lambda-count "\n"
                        
                        "\tmov rbx, rcx\n"
                        "\tadd rbx, 3\n"
                        "\tadd rbx, " (number->string n) "\n"
                        "\tGET_FROM_STACK rdx, rbx\n"
                        "\tMAKE_LITERAL_PAIR_OPT rdx, " sobNil "\n"
                        ;"\tNEW_MALLOC 8, rbx\n"
                        ;"\tmov qword [rbx], rax\n"
                        ;"\tmov rax, rbx\n"
                        
                        "\tdec rcx\n"
                        "\tcmp rcx, 0\n"
                        "\tje NO_LOOP_FILL_ENV_OPT_" lambda-count "\n"
                        
                        "\tLOOP_MAKE_PAIRS_" lambda-count ":\n"
                        "\tmov rbx, rcx\n"
                        "\tadd rbx, 3\n"
                        "\tadd rbx, " (number->string n) "\n"
                        "\tGET_FROM_STACK rdx, rbx\n"
                        "\tMAKE_LITERAL_PAIR_OPT rdx, rax\n"
                        ;"\tNEW_MALLOC 8, rbx\n"
                        ;"\tmov qword [rbx], rax\n"
                        ;"\tmov rax, rbx\n"
                        "\tdec rcx\n"
                        "\tcmp rcx, 0\n"
                        "\tjne LOOP_MAKE_PAIRS_" lambda-count "\n"

                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tmov qword [rdx + 8*" (number->string n) "], rax\n"
                        ;;End
                        
                        ;;This is for lambda-opt taht works with mallocing the list
;;                         "\tmov rax, 8\n"
;;                         "\tmul rcx\n"
;;                         "\tNEW_MALLOC rax, rbx\n"
;;                         
;;                         "\tmov rdx, 0\n"
;;                         "\tLOOP_FILL_ENV_OPT_" lambda-count ":\n"
;;                         "\tadd rdx, " (number->string (+ 4 n)) "\n"
;;                         "\tGET_FROM_STACK rax, rdx\n"
;;                         "\tsub rdx, " (number->string (+ 4 n)) "\n"
;;                         "\tmov qword [rbx + 8*rdx], rax\n"
;;                         "\tinc rdx\n"
;;                         "\tcmp rdx, rcx\n"
;;                         "\tjne LOOP_FILL_ENV_OPT_" lambda-count "\n"
                         "\tNO_LOOP_FILL_ENV_OPT_" lambda-count ":\n"
                        ;;End
                        
                        
                        
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tmov qword [rdx + 8*" (number->string n) "], rbx\n"
                        
                        
                        (code-gen body)
                        "\tleave\n"
                        "\tret\n"
                        "\tL" lambda-count ":\n\n"
                        
                        
                        ))))
                    
            (set! lambda-level (- lambda-level 1))
            lambda-code)))
            
