(define multiply_nasm "
        NEW_MALLOC 16, rax
	mov rdx, B_multiply
	mov rbx, 300776
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_multiply
B_multiply:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 0
	mov rax, 1
	cmp rcx, rbx
	je .done_multiply
	
.loop_multiply:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_multiply
	DATA rdx
	mul rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_multiply
	jmp .done_multiply
	
.fractions_multiply:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .mul_frac_to_frac

.mul_frac_to_int:
        DATA rdx
        mul rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	jmp .loop_fraction
	
.mul_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R13, rax
	pop rax
	mul R11
	sub rcx, 3
	cmp rcx, rbx
	je .done_multiply_fraction
	jmp .loop_fraction
	
.done_multiply:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
.done_multiply_fraction:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:

        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
L_multiply:
	mov qword [multiply], rax
	
")

(define divide_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_divide:
	cmp rdx, 1
	je LOOP_ENV_END_divide
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_divide
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_divide.CONT
EXTRA_ARGS_ON_STACK_divide:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_divide.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_divide
LOOP_ENV_END_divide:
	mov rdx, B_divide
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_divide
B_divide:
	push rbp
	mov rbp, rsp
	
	mov R13, 1
	mov rbx, qword [rbp + 3*8]
	mov rcx, 1
        cmp rcx, rbx
	je .one_arg_divide
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .first_fraction
	DATA rax
	jmp .loop_fraction
	
.first_fraction:
        mov rdx, rax
	NUMERATOR rax
	mov R13, rdx
	DENOMERATOR R13
	jmp .loop_fraction
	
.loop_divide:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_divide
	DATA rdx
	mov R11, rdx
	mov rdx, 0
	div R11
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_divide
	jmp .done_divide_fraction
	
.fractions_divide:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	push rax
	mov rax, R13
	mul R11
	mov R13, rax
	pop rax
	sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .div_frac_to_frac

.div_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mov rdx, 0
        mul R13
        mov R13, rax
        pop rax
        sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	jmp .loop_fraction
	
.div_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R11
	mov R13, rax
	pop rax
	mul R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_divide_fraction
	jmp .loop_fraction
	
.done_divide_fraction:

        mov rbx, R13
        shl rbx, 4
        cmp rbx, 0
        jg .no_switch
        
        push rax
        mov rax, R13
        mov rdx, -1
        mul rdx
        mov R13, rax
        pop rax
        mov rdx, -1
        mul rdx
        
.no_switch:

        mov rbx, rax
        shl rbx, 4
        cmp rbx, 0
        jge .positive
        
.negative:
        mov rdx, -1
        mul rdx
        push -1
        jmp .cont

.positive:
        push 1
        
.cont:
        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        pop rbx
        mul rbx
        

        cmp R13, 1
        je .done_divide
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret

.one_arg_divide:
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .only_fraction
	
.only_integer:
        mov rax, 1
        mov R13, qword [rbp + 4*8]
        DATA R13
        jmp .done_divide_fraction
        
.only_fraction:
        mov rax, qword [rbp + 4*8]
        mov R13, rax
        NUMERATOR R13
        DENOMERATOR rax
        jmp .done_divide_fraction

.done_divide:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret

L_divide:
	mov qword [divide], rax
	
")

(define minus_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_minus:
	cmp rdx, 1
	je LOOP_ENV_END_minus
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_minus
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_minus.CONT
EXTRA_ARGS_ON_STACK_minus:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_minus.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_minus
LOOP_ENV_END_minus:
	mov rdx, B_minus
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_minus
B_minus:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .first_fraction
	DATA rax
	cmp rcx, rbx
	je .one_arg_minus
	jmp .loop_minus
	
.first_fraction:
        mov rdx, rax
	NUMERATOR rax
	mov R13, rdx
	DENOMERATOR R13
	jmp .loop_fraction
	
.loop_minus:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_minus
	DATA rdx
	sub rax, rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_minus
	jmp .done_minus
	
.fractions_minus:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	sub rax, R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .sub_frac_to_frac

.sub_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mul R13
        mov rdx, rax
        pop rax
        sub rax, rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	jmp .loop_fraction
	
.sub_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R14, rax
	mov rax, R11
	mul R13
	mov R15, rax
	pop rax
	mul R12
	sub rax, R15
	mov R13, R14
	sub rcx, 3
	cmp rcx, rbx
	je .done_minus_fraction
	jmp .loop_fraction
	
.done_minus_fraction:
        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret
    

.one_arg_minus:
	mov rbx, 0
	sub rbx, rax
	mov rax, rbx

.done_minus:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret

L_minus:
	mov qword [minus], rax
	
")

(define plus_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_plus:
	cmp rdx, 1
	je LOOP_ENV_END_plus
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_plus
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_plus.CONT
EXTRA_ARGS_ON_STACK_plus:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_plus.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_plus
LOOP_ENV_END_plus:
	mov rdx, B_plus
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_plus
B_plus:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 3*8]
	mov rcx, 0
	mov rax, 0
	cmp rcx, rbx
	je .done_plus
	
.loop_plus:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .fractions_plus
	DATA rdx
	add rax, rdx
	sub rcx, 3
	cmp rcx, rbx
	jne .loop_plus
	jmp .done_plus
	
.fractions_plus:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	mul R12
	add rax, R11
	mov R13, R12
	sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	
.loop_fraction:
        add rcx, 4
        mov rdx, qword [rbp + rcx*8]
        mov R11, rdx
	TYPE R11
	cmp R11, T_FRACTION
	je .add_frac_to_frac

.add_frac_to_int:
        DATA rdx
        push rax
        mov rax, rdx
        mul R13
        mov rdx, rax
        pop rax
        add rax, rdx
        sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	jmp .loop_fraction
	
.add_frac_to_frac:
        mov R11, rdx
	NUMERATOR R11
	mov R12, rdx
	DENOMERATOR R12
	push rax
	mov rax, R13
	mul R12
	mov R14, rax
	mov rax, R11
	mul R13
	mov R15, rax
	pop rax
	mul R12
	add rax, R15
	mov R13, R14
	sub rcx, 3
	cmp rcx, rbx
	je .done_plus_fraction
	jmp .loop_fraction
        
	
	
.done_plus:
	shl rax, 4
	or rax, T_INTEGER
	
	leave
	ret
	
.done_plus_fraction:
        push rax
        
        push rax
        push R13
        call gcd
        add rsp, 16
        mov R11, rax
        
        mov rax, R13
        mov rdx, 0
        div R11
        mov R13, rax
        
        pop rax
        mov rdx, 0
        div R11
        
        shl rax, 4
        or rax, T_INTEGER
        shl R13, 4
        or R13, T_INTEGER
        MAKE_LITERAL_FRACTION rax, R13
	
	leave
	ret

L_plus:
	mov qword [plus], rax
	
")

(define equal_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_equal:
	cmp rdx, 1
	je LOOP_ENV_END_equal
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_equal
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_equal.CONT
EXTRA_ARGS_ON_STACK_equal:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_equal.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_equal
LOOP_ENV_END_equal:
	mov rdx, B_equal
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_equal
B_equal:
	push rbp
	mov rbp, rsp
	mov rcx, 4
LOOP_FILL_ENV_equal:
	GET_FROM_STACK rbx, rcx
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	sub rcx, 4
	mov qword [rdx+rcx*8], rbx
	add rcx, 5
	cmp rcx, 6
	jne LOOP_FILL_ENV_equal
	
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx]
	mov rax, qword [rbx + 0*8]
	mov rcx, rax
	TYPE rcx
	
	mov rbx, qword [rbp + 2*8]
	mov rbx, qword [rbx]
	mov rbx, qword [rbx + 1*8]
	mov rdx, rbx
	TYPE rdx
	
	cmp rcx, rdx
	jne .not_equals
	
	DATA rax
	DATA rbx
	
	cmp rax, rbx
	jne .not_equals
	
	mov rax, SOB_TRUE
	leave
	ret

.not_equals:

	mov rax, SOB_FALSE	
	leave
	ret
	
L_equal:
	mov qword [equal], rax
	
")

(define eq?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_eq?:
	cmp rdx, 1
	je LOOP_ENV_END_eq?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_eq?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_eq?.CONT
EXTRA_ARGS_ON_STACK_eq?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_eq?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_eq?
LOOP_ENV_END_eq?:
	mov rdx, B_eq?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_eq?
B_eq?:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rdx, qword [rbp + 4*8]
	TYPE rdx
	mov rcx, qword [rbp + 5*8]
	TYPE rcx
	cmp rcx, rdx
	jne .not_eq
	
	mov rdx, qword [rbp + 4*8]
	DATA rdx
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	cmp rcx, rdx
	jne .not_eq
	
	mov rax, SOB_TRUE
	leave
	ret

.not_eq:

	mov rax, SOB_FALSE	
	leave
	ret
	
L_eq?:
	mov qword [eq?], rax
	
")

(define greater_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_greater:
	cmp rdx, 1
	je LOOP_ENV_END_greater
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_greater
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_greater.CONT
EXTRA_ARGS_ON_STACK_greater:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_greater.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_greater
LOOP_ENV_END_greater:
	mov rdx, B_greater
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_greater
B_greater:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	je .done_greater
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	mov rdx, rax
	TYPE rdx
	cmp rdx, T_FRACTION
	je .fraction
	DATA rax
	
.loop_greater:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	TYPE rdx
	cmp rdx, T_FRACTION
	je .int_fraction
	
	.int_int:
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	cmp rcx, rbx
	jne .loop_greater
	jmp .done_greater
	
	.int_fraction:
	mov rdx, qword [rbp + rcx*8]
	DENOMERATOR rdx
	push rax
	mul rdx
	mov rdx, qword [rbp + rcx*8]
	NUMERATOR rdx
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	pop rax
	cmp rcx, rbx
	jne .loop_greater
	
.done_greater:
	mov rax, SOB_TRUE
	leave
	ret
	
.fraction:
        mov R13, rax
        NUMERATOR rax
        DENOMERATOR R13
        
.loop_fraction:
        add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	TYPE rdx
	cmp rdx, T_FRACTION
	je .fraction_fraction
	
	.fraction_int:
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	push rax
	mov rax, R13
	mul rdx
	mov rdx, rax
	pop rax
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
	cmp rcx, rbx
	jne .loop_fraction
	jmp .done_greater
	
	.fraction_fraction:
	mov rdx, qword [rbp + rcx*8]
	DENOMERATOR rdx
	push rax
	push R13
	mul rdx
	mov rdx, qword [rbp + rcx*8]
	NUMERATOR rdx
	push rax
	mov rax, R13
	mul rdx
	mov rdx, rax
	pop rax
	sub rcx, 3
	cmp rax, rdx
	jle .not_greater
        pop R13
	pop rax
	cmp rcx, rbx
	jne .loop_fraction
	jmp .done_greater

.not_greater:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_greater:
	mov qword [greater], rax
	
")

(define less_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_less:
	cmp rdx, 1
	je LOOP_ENV_END_less
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_less
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_less.CONT
EXTRA_ARGS_ON_STACK_less:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_less.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_less
LOOP_ENV_END_less:
	mov rdx, B_less
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_less
B_less:
	push rbp
	mov rbp, rsp
	mov rcx, 4
	
	mov rbx, qword [rbp + 3*8]
	cmp rbx, 1
	je .done_less
	mov rcx, 1
	mov rax, qword [rbp + 4*8]
	DATA rax
	
.loop_less:
	add rcx, 4
	mov rdx, qword [rbp + rcx*8]
	DATA rdx
	sub rcx, 3
	cmp rax, rdx
	jge .not_less
	cmp rcx, rbx
	jne .loop_less
	
.done_less:
	mov rax, SOB_TRUE
	leave
	ret

.not_less:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_less:
	mov qword [less], rax
	
")

(define length_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_length:
	cmp rdx, 1
	je LOOP_ENV_END_length
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_length
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_length.CONT
EXTRA_ARGS_ON_STACK_length:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_length.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_length
LOOP_ENV_END_length:
	mov rdx, B_length
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_length
B_length:
	push rbp
	mov rbp, rsp
	mov rcx, 4
LOOP_FILL_ENV_length:
	GET_FROM_STACK rbx, rcx
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	sub rcx, 4
	mov qword [rdx+rcx*8], rbx
	add rcx, 5
	cmp rcx, 5
	jne LOOP_FILL_ENV_length
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	cmp rax, SOB_NIL
	je .done_length
	
.loop_length:
	inc rcx
	CDR rax
	cmp rax, SOB_NIL
	jne .loop_length
	jmp .done_length
	
.pair_opt:
	cmp rax, SOB_NIL
	je .done_length
	
.loop_opt_length:
	inc rcx
	CDR_OPT rax
	cmp rax, SOB_NIL
	jne .loop_opt_length

.done_length:
	mov rax, rcx	
	shl rax, 4
	or rax, T_INTEGER
	leave
	ret
	
L_length:
	mov qword [length], rax
	
")

(define car_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_car:
	cmp rdx, 1
	je LOOP_ENV_END_car
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_car
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_car.CONT
EXTRA_ARGS_ON_STACK_car:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_car.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_car
LOOP_ENV_END_car:
	mov rdx, B_car
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_car
B_car:
	push rbp
	mov rbp, rsp
	mov rcx, 4
LOOP_FILL_ENV_car:
	GET_FROM_STACK rbx, rcx
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	sub rcx, 4
	mov qword [rdx+rcx*8], rbx
	add rcx, 5
	cmp rcx, 5
	jne LOOP_FILL_ENV_car
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	CAR rax
	jmp .done_car
	
.pair_opt:
	CAR_OPT rax

.done_car:
	leave
	ret
	
L_car:
	mov qword [car], rax
	
")

(define cdr_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_cdr:
	cmp rdx, 1
	je LOOP_ENV_END_cdr
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_cdr
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_cdr.CONT
EXTRA_ARGS_ON_STACK_cdr:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_cdr.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_cdr
LOOP_ENV_END_cdr:
	mov rdx, B_cdr
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_cdr
B_cdr:
	push rbp
	mov rbp, rsp
	mov rcx, 4
LOOP_FILL_ENV_cdr:
	GET_FROM_STACK rbx, rcx
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	sub rcx, 4
	mov qword [rdx+rcx*8], rbx
	add rcx, 5
	cmp rcx, 5
	jne LOOP_FILL_ENV_cdr
	
	mov rcx, 0
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	
	CDR rax
	jmp .done_cdr
	
.pair_opt:
	CDR_OPT rax

.done_cdr:
	leave
	ret
	
L_cdr:
	mov qword [cdr], rax
	
")

(define boolean?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_boolean?:
	cmp rdx, 1
	je LOOP_ENV_END_boolean?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_boolean?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_boolean?.CONT
EXTRA_ARGS_ON_STACK_boolean?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_boolean?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_boolean?
LOOP_ENV_END_boolean?:
	mov rdx, B_boolean?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_boolean?
B_boolean?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_BOOL
	jne .not_boolean
	
.done_boolean:
	mov rax, SOB_TRUE
	leave
	ret

.not_boolean:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_boolean?:
	mov qword [boolean?], rax
	
")

(define char?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_char?:
	cmp rdx, 1
	je LOOP_ENV_END_char?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_char?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_char?.CONT
EXTRA_ARGS_ON_STACK_char?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_char?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_char?
LOOP_ENV_END_char?:
	mov rdx, B_char?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_char?
B_char?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_CHAR
	jne .not_char
	
.done_char:
	mov rax, SOB_TRUE
	leave
	ret

.not_char:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_char?:
	mov qword [char?], rax
	
")

(define integer?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_integer?:
	cmp rdx, 1
	je LOOP_ENV_END_integer?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_integer?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_integer?.CONT
EXTRA_ARGS_ON_STACK_integer?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_integer?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_integer?
LOOP_ENV_END_integer?:
	mov rdx, B_integer?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_integer?
B_integer?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .not_integer
	
.done_integer:
	mov rax, SOB_TRUE
	leave
	ret

.not_integer:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_integer?:
	mov qword [integer?], rax
	
")

(define string?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_string?:
	cmp rdx, 1
	je LOOP_ENV_END_string?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_string?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_string?.CONT
EXTRA_ARGS_ON_STACK_string?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_string?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_string?
LOOP_ENV_END_string?:
	mov rdx, B_string?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_string?
B_string?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_STRING
	jne .not_string
	
.done_string:
	mov rax, SOB_TRUE
	leave
	ret

.not_string:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_string?:
	mov qword [string?], rax
	
")

(define pair?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_pair?:
	cmp rdx, 1
	je LOOP_ENV_END_pair?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_pair?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_pair?.CONT
EXTRA_ARGS_ON_STACK_pair?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_pair?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_pair?
LOOP_ENV_END_pair?:
	mov rdx, B_pair?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_pair?
B_pair?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_PAIR
	jne .not_regular_pair
	
.done_pair:
	mov rax, SOB_TRUE
	leave
	ret

.not_regular_pair:
	cmp rbx, T_PAIR_OPT
	jne .not_pair
	jmp .done_pair
	
.not_pair:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_pair?:
	mov qword [pair?], rax
	
")

(define vector?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_vector?:
	cmp rdx, 1
	je LOOP_ENV_END_vector?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_vector?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_vector?.CONT
EXTRA_ARGS_ON_STACK_vector?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_vector?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_vector?
LOOP_ENV_END_vector?:
	mov rdx, B_vector?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_vector?
B_vector?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_VECTOR
	jne .not_vector
	
.done_vector:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_vector:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_vector?:
	mov qword [vector?], rax
	
")

(define null?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_null?:
	cmp rdx, 1
	je LOOP_ENV_END_null?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_null?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_null?.CONT
EXTRA_ARGS_ON_STACK_null?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_null?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_null?
LOOP_ENV_END_null?:
	mov rdx, B_null?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_null?
B_null?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_NIL
	jne .not_null
	
.done_null:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_null:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_null?:
	mov qword [null?], rax
	
")

(define zero?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_zero?:
	cmp rdx, 1
	je LOOP_ENV_END_zero?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_zero?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_zero?.CONT
EXTRA_ARGS_ON_STACK_zero?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_zero?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_zero?
LOOP_ENV_END_zero?:
	mov rdx, B_zero?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_zero?
B_zero?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	cmp rbx, MAKE_LITERAL(T_INTEGER, 0)
	jne .not_zero
	
.done_zero:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_zero:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_zero?:
	mov qword [zero?], rax
	
")

(define symbol?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_symbol?:
	cmp rdx, 1
	je LOOP_ENV_END_symbol?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_symbol?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_symbol?.CONT
EXTRA_ARGS_ON_STACK_symbol?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_symbol?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_symbol?
LOOP_ENV_END_symbol?:
	mov rdx, B_symbol?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_symbol?
B_symbol?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_SYMBOL
	jne .not_symbol
	
.done_symbol:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_symbol:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_symbol?:
	mov qword [symbol?], rax
	
")

(define procedure?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_procedure?:
	cmp rdx, 1
	je LOOP_ENV_END_procedure?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_procedure?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_procedure?.CONT
EXTRA_ARGS_ON_STACK_procedure?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_procedure?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_procedure?
LOOP_ENV_END_procedure?:
	mov rdx, B_procedure?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_procedure?
B_procedure?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne .not_procedure
	
.done_procedure:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_procedure:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_procedure?:
	mov qword [procedure?], rax
	
")

(define number?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_number?:
	cmp rdx, 1
	je LOOP_ENV_END_number?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_number?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_number?.CONT
EXTRA_ARGS_ON_STACK_number?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_number?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_number?
LOOP_ENV_END_number?:
	mov rdx, B_number?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_number?
B_number?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_INTEGER
	jne .not_regular_number
	
.done_number:
	mov rax, SOB_TRUE
	leave
	ret

.not_regular_number:
	cmp rbx, T_FRACTION
	jne .not_number
	jmp .done_number
	
.not_number:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_number?:
	mov qword [number?], rax
	
")

(define rational?_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_rational?:
	cmp rdx, 1
	je LOOP_ENV_END_rational?
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_rational?
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_rational?.CONT
EXTRA_ARGS_ON_STACK_rational?:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_rational?.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_rational?
LOOP_ENV_END_rational?:
	mov rdx, B_rational?
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_rational?
B_rational?:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_FRACTION
	je .done_rational
	cmp rbx, T_INTEGER
	je .done_rational
	jmp .not_rational
	
.done_rational:
	mov rax, SOB_TRUE
	leave
	ret
	
.not_rational:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_rational?:
	mov qword [rational?], rax
	
")

(define apply_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_apply:
	cmp rdx, 1
	je LOOP_ENV_END_apply
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_apply
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_apply.CONT
EXTRA_ARGS_ON_STACK_apply:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_apply.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_apply
LOOP_ENV_END_apply:
	mov rdx, B_apply
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_apply
B_apply:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_CLOSURE
	jne ERROR_APPLY_NON_CLOSURE
	
	mov rbx, qword [rbp + 5*8]
	mov rcx, rbx
	mov rdx, rbx
	mov R9, rax
	
	mov rax, [reverse]
	
	push SOB_NIL
	push rbx
	push 1
	
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	mov rbx, rax
	CLOSURE_CODE rbx
	call rbx
	
	mov R10, rax
	add rsp, 8
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8
	mov rax, R10
	mov rdx, rax
	
	mov R8, 0
	mov rcx, rax
	mov rdx, rax
	push SOB_NIL
	TYPE rcx
	cmp rcx, T_PAIR
	je .continue_reg
	cmp rcx, T_PAIR_OPT
	je .continue_opt
	jmp ERROR_INVALID_ARGS
	
.continue_reg:
	cmp rdx, SOB_NIL
	je .done
	inc R8
	mov rcx, rdx
	CAR rcx
	push rcx
	CDR rdx
	jmp .continue_reg
	
.continue_opt:
	cmp rdx, SOB_NIL
	je .done
	inc R8
	mov rax, rdx
	CAR_OPT rax
	push rax
	CDR_OPT rdx
	mov rdx, rax
	jmp .continue_opt
	
.done:
	mov R9, qword [rbp + 4*8]
	mov rax, R9
	push R8
	mov rbx, rax
	CLOSURE_ENV rbx
	push rbx
	mov rbx, rax
	CLOSURE_CODE rbx
	call rbx
	
	mov R10, rax
	add rsp, 8
	pop rax
	mov rbx, 8
	mul rbx
	add rsp, rax
	add rsp, 8
	mov rax, R10
	
	leave
	ret
	
L_apply:
	mov qword [apply], rax
	
")

(define not_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_not:
	cmp rdx, 1
	je LOOP_ENV_END_not
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_not
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_not.CONT
EXTRA_ARGS_ON_STACK_not:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_not.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_not
LOOP_ENV_END_not:
	mov rdx, B_not
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_not
B_not:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	cmp rbx, SOB_FALSE
	je .done_true
	jmp .done_false
	
.done_true:
	mov rax, SOB_TRUE
	leave
	ret
	
.done_false:
	mov rax, SOB_FALSE	
	leave
	ret
	
L_not:
	mov qword [not], rax
	
")

(define cons_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_cons:
	cmp rdx, 1
	je LOOP_ENV_END_cons
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_cons
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_cons.CONT
EXTRA_ARGS_ON_STACK_cons:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_cons.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_cons
LOOP_ENV_END_cons:
	mov rdx, B_cons
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_cons
B_cons:
	push rbp
	mov rbp, rsp
	
	mov rdx, qword [rbp + 4*8]
	;;NEW_MALLOC 8, R8
	;;mov qword [R8], rdx
	mov R8, rdx
	mov rdx, qword [rbp + 5*8]
	;;NEW_MALLOC 8, R9
	;;mov qword [R9], rdx
	mov R9, rdx
	MAKE_LITERAL_PAIR_OPT R8, R9
	;;mov rax, qword [rax]
	test:
	leave
	ret
	
L_cons:
	mov qword [cons], rax
	
")

(define string_to_symbol_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_string_to_symbol:
	cmp rdx, 1
	je LOOP_ENV_END_string_to_symbol
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_string_to_symbol
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_string_to_symbol.CONT
EXTRA_ARGS_ON_STACK_string_to_symbol:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_string_to_symbol.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_string_to_symbol
LOOP_ENV_END_string_to_symbol:
	mov rdx, B_string_to_symbol
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_string_to_symbol
B_string_to_symbol:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_STRING
	jne ERROR_INVALID_ARGS
	
	mov rax, qword [rbp + 4*8]
	inc rax
		
	leave
	ret
	
L_string_to_symbol:
	mov qword [string_to_symbol], rax
	
")

(define symbol_to_string_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_symbol_to_string:
	cmp rdx, 1
	je LOOP_ENV_END_symbol_to_string
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_symbol_to_string
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_symbol_to_string.CONT
EXTRA_ARGS_ON_STACK_symbol_to_string:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_symbol_to_string.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_symbol_to_string
LOOP_ENV_END_symbol_to_string:
	mov rdx, B_symbol_to_string
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_symbol_to_string
B_symbol_to_string:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	TYPE rbx
	cmp rbx, T_SYMBOL
	jne ERROR_INVALID_ARGS
	
	mov rax, qword [rbp + 4*8]
	dec rax
		
	leave
	ret
	
L_symbol_to_string:
	mov qword [symbol_to_string], rax
	
")

(define char_to_integer_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_char_to_integer:
	cmp rdx, 1
	je LOOP_ENV_END_char_to_integer
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_char_to_integer
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_char_to_integer.CONT
EXTRA_ARGS_ON_STACK_char_to_integer:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_char_to_integer.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_char_to_integer
LOOP_ENV_END_char_to_integer:
	mov rdx, B_char_to_integer
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_char_to_integer
B_char_to_integer:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_CHAR
	jne ERROR_INVALID_ARGS
	
	mov rbx, rax
	DATA rbx
	cmp rbx, 0
	jl ERROR_INVALID_ARGS
	cmp rbx, 256
	jge ERROR_INVALID_ARGS
	
	xor rax, (T_CHAR ^ T_INTEGER)
		
	leave
	ret
	
L_char_to_integer:
	mov qword [char_to_integer], rax
	
")

(define integer_to_char_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_integer_to_char:
	cmp rdx, 1
	je LOOP_ENV_END_integer_to_char
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_integer_to_char
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_integer_to_char.CONT
EXTRA_ARGS_ON_STACK_integer_to_char:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_integer_to_char.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_integer_to_char
LOOP_ENV_END_integer_to_char:
	mov rdx, B_integer_to_char
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_integer_to_char
B_integer_to_char:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	TYPE rbx
	cmp rbx, T_INTEGER
	jne ERROR_INVALID_ARGS
	
	mov rbx, rax
	DATA rbx
	cmp rbx, 0
	jl ERROR_INVALID_ARGS
	cmp rbx, 256
	jge ERROR_INVALID_ARGS
	
	xor rax, (T_CHAR ^ T_INTEGER)
		
	leave
	ret
	
L_integer_to_char:
	mov qword [integer_to_char], rax
	
")

(define numerator_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_numerator:
	cmp rdx, 1
	je LOOP_ENV_END_numerator
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_numerator
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_numerator.CONT
EXTRA_ARGS_ON_STACK_numerator:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_numerator.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_numerator
LOOP_ENV_END_numerator:
	mov rdx, B_numerator
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_numerator
B_numerator:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	NUMERATOR rax
	shl rax, TYPE_BITS
	or rax, T_INTEGER

	leave
	ret
	
L_numerator:
	mov qword [numerator], rax
	
")

(define denominator_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_denominator:
	cmp rdx, 1
	je LOOP_ENV_END_denominator
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_denominator
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_denominator.CONT
EXTRA_ARGS_ON_STACK_denominator:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_denominator.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_denominator
LOOP_ENV_END_denominator:
	mov rdx, B_denominator
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_denominator
B_denominator:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	DENOMERATOR rax
	shl rax, TYPE_BITS
	or rax, T_INTEGER

	leave
	ret
	
L_denominator:
	mov qword [denominator], rax
	
")

(define set_car_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_set_car:
	cmp rdx, 1
	je LOOP_ENV_END_set_car
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_set_car
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_set_car.CONT
EXTRA_ARGS_ON_STACK_set_car:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_set_car.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_set_car
LOOP_ENV_END_set_car:
	mov rdx, B_set_car
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_set_car
B_set_car:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	mov rdx, qword [rbp + 5*8]
	TYPE rbx
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	jmp .done

.pair_opt:
        DATA rax
        mov qword [rax], rdx
        mov rax, SOB_VOID
	
.done:
	leave
	ret
	
L_set_car:
	mov qword [set_car], rax
	
")

(define set_cdr_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_set_cdr:
	cmp rdx, 1
	je LOOP_ENV_END_set_cdr
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_set_cdr
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_set_cdr.CONT
EXTRA_ARGS_ON_STACK_set_cdr:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_set_cdr.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_set_cdr
LOOP_ENV_END_set_cdr:
	mov rdx, B_set_cdr
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_set_cdr
B_set_cdr:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	mov rbx, rax
	mov rdx, qword [rbp + 5*8]
	TYPE rbx
	cmp rbx, T_PAIR_OPT
	je .pair_opt
	jmp .done

.pair_opt:
        DATA rax
        mov qword [rax+8], rdx
        mov rax, SOB_VOID
	
.done:
	leave
	ret
	
L_set_cdr:
	mov qword [set_cdr], rax
	
")

(define vector_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_vector:
	cmp rdx, 1
	je LOOP_ENV_END_vector
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_vector
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_vector.CONT
EXTRA_ARGS_ON_STACK_vector:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_vector.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_vector
LOOP_ENV_END_vector:
	mov rdx, B_vector
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_vector
B_vector:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 3*8]
	cmp rax, 0
	je .empty_vector
	
	mov rdx, 8
	mul rdx
	mov rcx, rax
	NEW_MALLOC rcx, rdx
	
	mov rcx, qword [rbp + 3*8]
	add rcx, 4
	mov rdi, 4
.loop:
        mov rax, qword [rbp + rdi*8]
        mov R10, 8
        NEW_MALLOC R10, R11
        mov qword [R11], rax
        sub rdi, 4
        mov qword [rdx + 8*rdi], R11
        add rdi, 5
        cmp rdi, rcx
        jne .loop
	
.done:
        mov rax, qword [rbp + 3*8]
        shl rax, 34
        sub rdx, start_of_data
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
.empty_vector:
        mov rax, qword [rbp + 3*8]
        shl rax, 34
        mov rdx, 0
        shl rdx, 4
        or rax, rdx
        or rax, T_VECTOR
        
	leave
	ret
	
L_vector:
	mov qword [vector], rax
	
")

(define vector_length_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_vector_length:
	cmp rdx, 1
	je LOOP_ENV_END_vector_length
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_vector_length
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_vector_length.CONT
EXTRA_ARGS_ON_STACK_vector_length:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_vector_length.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_vector_length
LOOP_ENV_END_vector_length:
	mov rdx, B_vector_length
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_vector_length
B_vector_length:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	VECTOR_LENGTH rax
	shl rax, 4
	or rax, T_INTEGER
	
.done:
	leave
	ret
	
L_vector_length:
	mov qword [vector_length], rax
	
")

(define vector_ref_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_vector_ref:
	cmp rdx, 1
	je LOOP_ENV_END_vector_ref
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_vector_ref
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_vector_ref.CONT
EXTRA_ARGS_ON_STACK_vector_ref:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_vector_ref.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_vector_ref
LOOP_ENV_END_vector_ref:
	mov rdx, B_vector_ref
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_vector_ref
B_vector_ref:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	;VECTOR_REF rax, rbx, rcx
	
	mov rdx, rbx
	VECTOR_ELEMENTS rdx
	mov rax, rcx
	mov rbx, 8
	push rdx
	mul rbx
	pop rdx
	add rdx, rax
	mov rdx, qword [rdx]
	mov rdx, qword [rdx]
	
	mov rax, rdx
	
.done:
	leave
	ret
	
L_vector_ref:
	mov qword [vector_ref], rax
	
")

(define vector_set_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_vector_set:
	cmp rdx, 1
	je LOOP_ENV_END_vector_set
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_vector_set
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_vector_set.CONT
EXTRA_ARGS_ON_STACK_vector_set:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_vector_set.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_vector_set
LOOP_ENV_END_vector_set:
	mov rdx, B_vector_set
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_vector_set
B_vector_set:
	push rbp
	mov rbp, rsp
	
	mov rax, 8
	NEW_MALLOC rax, R11
	mov rax, qword [rbp + 6*8]
	mov qword [R11], rax
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	VECTOR_ELEMENTS rbx
	push rbx
	mov rax, rcx
	mov rbx, 8
	mul rbx
	pop rbx
	add rbx, rax
	mov qword [rbx], R11
	
	mov rax, SOB_VOID
	
.done:
	leave
	ret
	
L_vector_set:
	mov qword [vector_set], rax
	
")

(define string_length_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_string_length:
	cmp rdx, 1
	je LOOP_ENV_END_string_length
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_string_length
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_string_length.CONT
EXTRA_ARGS_ON_STACK_string_length:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_string_length.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_string_length
LOOP_ENV_END_string_length:
	mov rdx, B_string_length
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_string_length
B_string_length:
	push rbp
	mov rbp, rsp
	
	mov rax, qword [rbp + 4*8]
	STRING_LENGTH rax
	shl rax, 4
	or rax, T_INTEGER
	
.done:
	leave
	ret
	
L_string_length:
	mov qword [string_length], rax
	
")

(define string_ref_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_string_ref:
	cmp rdx, 1
	je LOOP_ENV_END_string_ref
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_string_ref
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_string_ref.CONT
EXTRA_ARGS_ON_STACK_string_ref:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_string_ref.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_string_ref
LOOP_ENV_END_string_ref:
	mov rdx, B_string_ref
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_string_ref
B_string_ref:
	push rbp
	mov rbp, rsp
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	mov rdx, 0
	push rax
	mov rax, rbx
	STRING_ELEMENTS rax
	add rax, rcx
	mov dl, byte [rax]
	pop rax
	
	mov rax, rdx
	shl rax, 4
	or rax, T_CHAR
	
.done:
	leave
	ret
	
L_string_ref:
	mov qword [string_ref], rax
	
")

(define string_set_nasm "
	NEW_MALLOC 16, rbx
	push rbx
	NEW_MALLOC 16, rcx
	NEW_MALLOC 16, rax
	pop rbx
	mov rdx, 0
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov rdi, 4838
	mov qword [rcx+rdx*8], rdi
	inc rdx
	mov qword [rbx], rcx
	mov rdx, 0
LOOP_ENV_string_set:
	cmp rdx, 1
	je LOOP_ENV_END_string_set
	cmp rsp, rbp
	jne EXTRA_ARGS_ON_STACK_string_set
	mov rdi, qword [rsp + 2*8]
	jmp EXTRA_ARGS_ON_STACK_string_set.CONT
EXTRA_ARGS_ON_STACK_string_set:
	mov rdi, qword [rsp]
	add rdi, 3
	mov rdi, qword [rsp + rdi*8]
EXTRA_ARGS_ON_STACK_string_set.CONT:
	mov rdi, qword [rdi + rdx*8]
	mov rsi, rdx
	inc rsi
	mov qword [rbx + rsi*8], rdi
	inc rdx
	jmp LOOP_ENV_string_set
LOOP_ENV_END_string_set:
	mov rdx, B_string_set
	MAKE_LITERAL_CLOSURE rax, rbx, rdx
	mov rax, [rax]
	jmp L_string_set
B_string_set:
	push rbp
	mov rbp, rsp
	
	mov rdx, 0
	mov R11, qword [rbp + 6*8]
	DATA R11
	mov rdx, R11
	
	mov rbx, qword [rbp + 4*8]
	mov rcx, qword [rbp + 5*8]
	DATA rcx
	
	
	push rax
	mov rax, rbx
	STRING_ELEMENTS rax
	add rax, rcx
	mov byte [rax], dl
	pop rax
	
	mov rax, SOB_VOID
	
.done:
	leave
	ret
	
L_string_set:
	mov qword [string_set], rax
	
")

(define built-ins '(length multiply divide minus
					plus equal greater less
					boolean? char? integer? string?
					pair? vector? null? zero?
					symbol? procedure? number? rational?
					string_to_symbol symbol_to_string
					char_to_integer integer_to_char
					eq? apply not cons car cdr
					numerator denominator set_car set_cdr
					vector vector_length vector_ref
					vector_set string_length
					string_ref string_set))

(define built-ins-code
	(string-append
	apply_nasm
	car_nasm
	cdr_nasm
	length_nasm
	multiply_nasm
	divide_nasm
	minus_nasm
	plus_nasm
	equal_nasm
	greater_nasm
	less_nasm
	boolean?_nasm
	char?_nasm
	integer?_nasm
	string?_nasm
	pair?_nasm
	vector?_nasm
	null?_nasm
	zero?_nasm
	symbol?_nasm
	procedure?_nasm
	number?_nasm
	rational?_nasm
	not_nasm
	cons_nasm
	eq?_nasm
	string_to_symbol_nasm
	symbol_to_string_nasm
	char_to_integer_nasm
	integer_to_char_nasm
	numerator_nasm
	denominator_nasm
	set_car_nasm
	set_cdr_nasm
	vector_nasm
	vector_length_nasm
	vector_ref_nasm
	vector_set_nasm
	string_length_nasm
	string_ref_nasm
	string_set_nasm))
	
(define fill_env_multiply_old_stuff_not_in_use_anymore "
	mov rcx, 4
LOOP_FILL_ENV_multiply:
	GET_FROM_STACK rbx, rcx
	GET_FROM_STACK rdx, 2
	mov rdx, qword [rdx]
	sub rcx, 4
	mov qword [rdx+rcx*8], rbx
	add rcx, 5
	cmp rcx, 6
	jne LOOP_FILL_ENV_multiply")