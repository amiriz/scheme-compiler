(load "1-3/sexpr-parser.scm")
(load "1-3/tag-parser.scm")
(load "1-3/semantic-analyzer.scm")
(load "meir.scm")
(load "built-ins.scm")

(define pipeline
    (lambda (s)
        ((star <sexpr>) s
            (lambda (m r)
                (map (lambda (e)
                    (annotate-tc
                        (pe->lex-pe
                            (box-set
                                (remove-applic-lambda-nil
                                    (parse e))))))
        m))
    (lambda (f) 'fail))))

(define file->list
    (lambda (in-file)
        (let ((in-port (open-input-file in-file)))
            (letrec ((run
                        (lambda ()
                            (let ((ch (read-char in-port)))
                                (if (eof-object? ch)
                                    (begin
                                        (close-input-port in-port)
                                        '())
                                    (cons ch (run)))))))
                (run)))))
				
(define helper-func 
	(lambda ()
		(let ((ch (read-char in-port)))
			(if (eof-object? ch)
				(begin
					(close-input-port in-port)
					'())
				(cons ch (run))))))
                
(define compile-scheme-file
    (lambda (in-file out-file)
		(let* ((in-string (file->list in-file))
			   (built-ins-scheme (file->list "built-ins-scheme.scm"))
			   (built-ins-scheme-pipelined (pipeline built-ins-scheme))
			   (pipelined (pipeline in-string))
			   (action1 (make-const-table (append built-ins-scheme-pipelined pipelined)))
			   (action2 (make-global-table (append built-ins-scheme-pipelined pipelined)))
			   (action3 (make-symbol-table pipelined))
			   (out-string (string-append
                                            prologue1
                                            (code-gen-const-table)
                                            (code-gen-string-table)
                                            (code-gen-global-table)
                                            prologue2
                                            get-set
                                            (set-up)
                                            (code-gen built-ins-scheme-pipelined)
                                            (reverse-fix)
                                            (code-gen-and-print pipelined)
                                            epilogue)))
			(string->file out-string out-file))))

    
(define string->file
    (lambda (string out-name)
        (display string (open-output-file out-name))))

(define parse1-3
	(lambda (in-file)
		(pipeline (file->list in-file))))
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;		

(define const-table '())
(define global-table '())
(define symbol-table '())
(define string-table '())

(define const-counter 0) 
(define global-counter 0) 
(define string-counter 0)
(define fvar-counter 0) 
(define if3-counter 0)
(define or-counter 0)
(define lambda-counter 0)
(define lambda-level 0)
(define opt-counter 0)


(define prologue1 (string-append scheme.s "     

section .bss

extern exit, printf, scanf, malloc

section .text 
     align 16 
     global main\n\n"
))

(define prologue2 (string-append
    "section .text\n\n"
    "main:\n\n"
    "\tpush 0\n"
    "\tpush 0\n"
    "\tNEW_MALLOC 8, rbx\n"     
    "\tmov qword [rbx], -1\n"   
    "\tmov rax, rbx\n"
    "\tNEW_MALLOC 8, rbx\n"
    "\tmov qword [rbx], rax\n"
    "\tpush rbx\n"
    "\tpush 0\n"
    "\tpush -1\n"
    "\tmov rbp, rsp\n"
    "\n"
    "\tjmp L_main_is_ready\n"
	
	built-ins-code
    ))
    
(define write "

    push rax
    call write_sob_if_not_void
    add rsp, 1*8
    
    ")

(define epilogue "

    pop    rbp
        
    push 0
    call exit
    
error_apply_non_closure_text:
    dq \"Exception: attempt to apply non procedure\", CHAR_NEWLINE, 0
	
error_variable_not_bound_text:
    dq \"Exception: variable _ is not bound\", CHAR_NEWLINE, 0
	
error_invalid_args:
    dq \"Exception: invalid args to proc\", CHAR_NEWLINE, 0
    
error_not_make_sense:
    dq \"Exception: something doesnt make sense!\", CHAR_NEWLINE, 0
	
	
ERROR_NOT_MAKE_SENSE:
    PRINTFF error_not_make_sense
    push -1
    call exit
	
ERROR_INVALID_ARGS:
    PRINTFF error_invalid_args
    push -1
    call exit
	
ERROR_VARIABLE_NOT_BOUND:
    PRINTFF error_variable_not_bound_text
    push -1
    call exit

ERROR_APPLY_NON_CLOSURE:
    PRINTFF error_apply_non_closure_text
    push -1
    call exit

")

(define get-set "
L_main_is_ready:

")

(define const-void?
	(lambda (v)
		(eq? v (void))))

(define find-name-in-const-table
    (lambda (value)
        ;(display value)
        ;(newline)
        (let ((found (filter (lambda (pair) (equal? value (cadr pair))) const-table)))
            (if (null? found)
                '()
                (caar found)))))
        
(define find-in-global-table
    (lambda (name)
;;         (display name)
;;         (newline)
        (caar (filter (lambda (pair) (equal? name (cadr pair))) global-table))))
        
(define find-name-in-symbol-table
    (lambda (value)
        "4838"))
        
(define find-name-in-string-table
    (lambda (value)
        ;(display value)
        ;(newline)
        (let ((found (filter (lambda (pair) (equal? value (cadr pair))) string-table)))
            (if (null? found)
                '()
                (caar found)))))
        


(define code-gen-const
    (lambda (expr)
        (let* ((value (cadr expr))
               (name (find-name-in-const-table value)))
            (string-append "\tmov rax, [" name "]\n")
        )))
		
(define code-gen-if3
    (lambda (expr)
        (let* ((test (list (list-ref expr 1)))
                (then (list (list-ref expr 2)))
                (els (list (list-ref expr 3)))
                (test-code (code-gen test))
                (then-code (code-gen then))
                (else-code (code-gen els))
                (if-count (format "~a" if3-counter))
                (if-code
                    (string-append
                        test-code
                        "\tcmp rax, SOB_FALSE\n"
                        "\tje ELSE" if-count "\n"
                        "\tTHEN" if-count ":\n\t"
                        then-code
                        "\tjmp END_IF" if-count "\n"
                        "\tELSE" if-count ":\n\t"
                        else-code
                        "\tEND_IF" if-count ":\n\n")))
                        
                    (set! if3-counter (+ if3-counter 1))
                    if-code)))
			
(define code-gen-seq
    (lambda (expr)
        (let* ((exprs (cadr expr)))
            (fold-left
                (lambda (acc x) (string-append acc (code-gen (list x))))
                ""
                exprs))))
				
(define code-gen-or
    (lambda (expr)
        (let* ((exprs (cadr expr))
               (or-count (format "~a" or-counter))
               (action1 (set! or-counter (+ or-counter 1)))
               (or-code
                    (string-append
                        "\tmov rax, SOB_FALSE\n"
                        (fold-left
                            (lambda (acc x)
                                (string-append
                                    acc
                                    (code-gen (list x))
                                    "\tcmp rax, SOB_FALSE\n"
                                    "\tjne END_OR" or-count "\n"
                                    ))
                            ""
                            exprs)
                        "\tEND_OR" or-count ":\n\n")))
                    or-code)))
            
(define code-gen-lambda-simple
    (lambda (expr)
        (let* ((lambda-count (format "~a" lambda-counter))
               (params (list-ref expr 1))
               (body (list (list-ref expr 2)))
               (n (length params))
               (m lambda-level)
               (rbx_malloc (number->string (* 8 m)))
               (rcx_malloc (number->string (* 8 n)))
			   (code_for_copying_stack_to_env0
				(if (string? m)
					(string-append
						"\tmov rbx, qword [rbp + 2*8]\n"
						"\tmov rbx, qword [rbx]\n"
						"\tmov rcx, qword [rbp + 3*8]\n"
						"\tcmp rcx, 0\n"
						"\tje .done_copying\n"
						"\tadd rcx, 3\n"
						"\tmov R12, 3\n"
						"\tmov R13, 0\n"
						
						".loop_copying:\n"
						"\tinc R12\n"
						"\tmov rdx, qword [rbp + R12*8]\n"
						"\tmov qword [rbx + R13*8], rdx\n"
						"\tinc R13\n"
						"\tcmp rcx, R12\n"
						"\tjne .loop_copying\n"
						
						".done_copying:\n"
					)
					""))
               (lambda-code
                (begin (set! lambda-counter (+ lambda-counter 1))
                    (string-append
					
			code_for_copying_stack_to_env0
					
                        "\tNEW_MALLOC " rbx_malloc ", rbx\n"
                        "\tpush rbx\n"
                        
                        "\tmov rax, qword [rbp + 3*8]\n"
                        "\tmov rbx, 8\n"
                        "\tmul rbx\n"
                        "\tmov rbx, rax\n"
                        
                        "\tNEW_MALLOC rbx, rcx\n"
                        "\tNEW_MALLOC 16, rax\n"
                        "\tpop rbx\n"                            
                        
                        "\tmov qword [rbx], rcx\n"
                        
                        "\tmov rdx, 0\n"
                        "\tLOOP_ENV" lambda-count ":\n"
                        "\tcmp rdx, " (number->string (- m 1)) "\n"
                        "\tje LOOP_ENV_END" lambda-count "\n"
                        
                        "\tmov rdi, qword [rbp + 2*8]\n"
                        "\tjmp EXTRA_ARGS_ON_STACK_" lambda-count ".CONT\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ":\n"
                        "\tmov rdi, qword [rsp]\n"
                        "\tadd rdi, 3\n"
                        "\tmov rdi, qword [rsp + rdi*8]\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ".CONT:\n"
                        "\tmov rdi, qword [rdi + rdx*8]\n"
                        "\tmov rsi, rdx\n"
                        "\tinc rsi\n"
                        "\tmov qword [rbx + rsi*8], rdi\n"
                        
                        "\tinc rdx\n"
                        "\tjmp LOOP_ENV" lambda-count "\n"
                        "\tLOOP_ENV_END" lambda-count ":\n"
                                            
                        "\tmov R15, rbx\n"
                        "\tmov rdi, qword [rbp + 3*8]\n"
                        "\tmov rcx, 0\n"
                        "\tcmp rcx, rdi \n"
                        "\tje LOOP_FILL_ENV_" lambda-count ".done\n"
                        
                        "LOOP_FILL_ENV_" lambda-count ":\n"
                        
                        "\tadd rcx, 4\n"
                        "\tmov rdx, qword [rbx]\n"
                        "\tmov rsi, qword [rbp + rcx*8]\n"
                        "\tsub rcx, 4\n"
                        "\tmov qword [rdx+rcx*8], rsi\n"
                        "\tinc rcx\n"
                        "\tcmp rcx, rdi\n"
                        "\tjne LOOP_FILL_ENV_" lambda-count "\n"
                        
                        ".done:\n"
                        
                        "\tmov rbx, R15\n"
                        "\tmov rdx, B" lambda-count "\n"
                        "\tMAKE_LITERAL_CLOSURE rax, rbx, rdx\n"
                        "\tmov rax, [rax]\n"
                        "\tjmp L" lambda-count "\n"
                        
                        "B" lambda-count ":\n"
						
                        "\tpush rbp\n"
                        "\tmov rbp, rsp\n"
                        
                        (code-gen body)
                        
                        "\tleave\n"
                        "\tret\n"
                        
                        "L" lambda-count ":\n\n"
                        
                        
                        ))))
                    
            (set! lambda-level (- lambda-level 1))
            lambda-code)))
            
            
(define code-gen-lambda-opt
    (lambda (expr)
        (let* ((lambda-count (format "~a" lambda-counter))
               (params (list-ref expr 1))
               (p-list (list-ref expr 2))
               (body (list (list-ref expr 3)))
               (n (length params))
               (m lambda-level)
               (sobNil (find-name-in-const-table '()))
               (rbx_malloc (number->string (* 8 m)))
               (rcx_malloc (number->string (+ (* 8 n) 1)))
               (lambda-code
                (begin (set! lambda-counter (+ lambda-counter 1))
                    (string-append
								
                        "\tNEW_MALLOC " rbx_malloc ", rbx\n"
                        "\tpush rbx\n"
                        
                        "\tmov rax, qword [rbp + 3*8]\n"
                        "\tmov rbx, 8\n"
                        "\tmul rbx\n"
                        "\tmov rbx, rax\n"
                        
                        "\tNEW_MALLOC rbx, rcx\n"
                        "\tNEW_MALLOC 16, rax\n"
                        "\tpop rbx\n"                            
                        
                        "\tmov qword [rbx], rcx\n"
                        
                        "\tmov rdx, 0\n"
                        "\tLOOP_ENV" lambda-count ":\n"
                        "\tcmp rdx, " (number->string (- m 1)) "\n"
                        "\tje LOOP_ENV_END" lambda-count "\n"
                        
                        "\tmov rdi, qword [rbp + 2*8]\n"
                        "\tjmp EXTRA_ARGS_ON_STACK_" lambda-count ".CONT\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ":\n"
                        "\tmov rdi, qword [rsp]\n"
                        "\tadd rdi, 3\n"
                        "\tmov rdi, qword [rsp + rdi*8]\n"
                        
                        "\tEXTRA_ARGS_ON_STACK_" lambda-count ".CONT:\n"
                        "\tmov rdi, qword [rdi + rdx*8]\n"
                        "\tmov rsi, rdx\n"
                        "\tinc rsi\n"
                        "\tmov qword [rbx + rsi*8], rdi\n"
                        
                        "\tinc rdx\n"
                        "\tjmp LOOP_ENV" lambda-count "\n"
                        "\tLOOP_ENV_END" lambda-count ":\n"
                                            
                        "\tmov R15, rbx\n"
                        "\tmov rdi, qword [rbp + 3*8]\n"
                        "\tmov rcx, 0\n"
                        "\tcmp rcx, rdi \n"
                        "\tje LOOP_FILL_ENV_" lambda-count ".done\n"
                        
                        "LOOP_FILL_ENV_" lambda-count ":\n"
                        
                        "\tadd rcx, 4\n"
                        "\tmov rdx, qword [rbx]\n"
                        "\tmov rsi, qword [rbp + rcx*8]\n"
                        "\tsub rcx, 4\n"
                        "\tmov qword [rdx+rcx*8], rsi\n"
                        "\tinc rcx\n"
                        "\tcmp rcx, rdi\n"
                        "\tjne LOOP_FILL_ENV_" lambda-count "\n"
                        
                        ".done:\n"
                        
                        "\tmov rbx, R15\n"
                        "\tmov rdx, B" lambda-count "\n"
                        "\tMAKE_LITERAL_CLOSURE rax, rbx, rdx\n"
                        "\tmov rax, [rax]\n"
                        "\tjmp L" lambda-count "\n"
                        
                        
                        
                        "B" lambda-count ":\n"
                        "\tpush rbp\n"
                        "\tmov rbp, rsp\n"
                        
                        "\tGET_FROM_STACK rcx, 3\n"
                        "\tsub rcx, " (number->string n) "\n"
                        
                        ;;This is for lambda-opt that attempts creating T_PAIR
                        "\tcmp rcx, 0\n"
                        "\tje NO_LOOP_FILL_ENV_OPT_" lambda-count "\n"
                        
                        "\tFILL_ENV_OPT_" lambda-count ":\n"
                        "\tmov rax, rcx\n"
                        "\tadd rax, 3\n"
                        "\tadd rax, " (number->string n) "\n"
                        "\tGET_FROM_STACK rdx, rax\n"
                        ;"\tNEW_MALLOC 8, rdi\n"
                        ;"\tmov qword [rdi], rdx\n"
                        ;"\tMAKE_LITERAL_PAIR_OPT rdi, " sobNil "\n"
						"\tmov rdi, SOB_NIL\n"
						"\tMAKE_LITERAL_PAIR_OPT rdx, rdi\n"
                        
                        

                        "\tdec rcx\n"
                        "\tcmp rcx, 0\n"
                        "\tje PUT_LIST_IN_ENV_" lambda-count "\n"
                        
                        "LOOP_MAKE_PAIRS_" lambda-count ":\n"
                        "\tpush rax\n"
                        "\tmov rax, rcx\n"
                        "\tadd rax, 4\n"
                        "\tadd rax, " (number->string n) "\n"
                        "\tGET_FROM_STACK rdx, rax\n"
                        ;"\tNEW_MALLOC 8, rdi\n"
                        ;"\tmov qword [rdi], rdx\n"
                        "\tpop rax\n"
                        ;"\tmov rdx, rax\n"
                        ;"\tMAKE_LITERAL_PAIR_OPT rdi, rdx\n"
						"\tpush rcx\n"
						"\tmov rcx, rax\n"
						"\tMAKE_LITERAL_PAIR_OPT rdx, rcx\n"
						"\tpop rcx\n"
                        "\tdec rcx\n"
                        "\tcmp rcx, 0\n"
                        "\tjne LOOP_MAKE_PAIRS_" lambda-count "\n"
                        "\tjmp PUT_LIST_IN_ENV_" lambda-count "\n"

                        
                        "\tNO_LOOP_FILL_ENV_OPT_" lambda-count ":\n"
                        "\tNEW_MALLOC 8, rcx\n"
                        "\tmov qword [rcx], SOB_NIL\n"
                        "\tGET_FROM_STACK rdx, 2\n"
                        "\tmov rdx, qword [rdx]\n"
                        "\tmov rax, qword [rcx]\n"
                        "\tmov qword [rdx + 8*" (number->string n) "], rax\n"
                        "\tjmp DDD" lambda-count "\n"
                        
                        
                        "PUT_LIST_IN_ENV_" lambda-count ":\n"
                        ;"\tGET_FROM_STACK rdx, 2\n"
                        ;"\tmov rdx, qword [rdx]\n"
                        ;"\tmov qword [rdx + 8*" (number->string n) "], rax\n"
                        "\tmov rdx, rbp\n"
                        "\tadd rdx, " (number->string (* 8 (+ n 4))) "\n"
                        "\tmov qword [rdx], rax\n"
                        
                        "\tDDD" lambda-count ":\n"
                        
                        
                        (code-gen body)
                        "\tleave\n"
                        "\tret\n"
                        "L" lambda-count ":\n\n"
                        
                        
                        ))))
                    
            (set! lambda-level (- lambda-level 1))
            lambda-code)))
            
            
(define code-gen-applic
    (lambda (expr)
        (let* ((proc (list (list-ref expr 1)))
               (args (list-ref expr 2))
               (m (number->string (length args)))
               (reversed-args (reverse args))
               (applic-code
                    (string-append
					
                        "\tpush SOB_NIL\n"
					
                        (fold-left
                            (lambda (acc x)
                                        (string-append
                                            acc
                                            (code-gen (list x))
                                            "\tpush rax\n"
                                        ))
                            ""
                            reversed-args) 
                        
                        "\tpush " m "\n"
                        
                        (code-gen proc)
                        "\tpush rax\n"
                        "\tTYPE rax\n"
                        "\tcmp rax, T_CLOSURE\n"
                        "\tjne ERROR_APPLY_NON_CLOSURE\n"
                        "\tpop rax\n"
                        
                        "\tmov rbx, rax\n"
                        "\tCLOSURE_ENV rbx\n"
                        "\tpush rbx\n"
                        "\tCLOSURE_CODE rax\n"
                        "\tcall rax\n"
                        
						;;clean stack when done
                        "\tmov R10, rax\n"
                        "\tadd rsp, 8*1\n"
						"\tpop rax\n"
						"\tmov rbx, 8\n"
						"\tmul rbx\n"
                        "\tadd rsp, rax\n"
					    "\tadd rsp, 8*1\n"
						"\tmov rax, R10\n"
						)))

            applic-code)))
			
(define code-gen-tc-applic
    (lambda (expr)
        (let* ((proc (list (list-ref expr 1)))
               (args (list-ref expr 2))
               (m (number->string (length args)))
               (reversed-args (reverse args))
               (applic-code
                    (string-append

						"\tpush SOB_NIL\n"
					
                        (fold-left
                            (lambda (acc x)
                            (string-append
                                acc
                                (code-gen (list x))
                                "\tpush rax\n"
                            ))
                            ""
                            reversed-args) 
                        
                        "\tpush " m "\n"
                        
                        (code-gen proc)
                        "\tpush rax\n"
                        "\tTYPE rax\n"
                        "\tcmp rax, T_CLOSURE\n"
                        "\tjne ERROR_APPLY_NON_CLOSURE\n"
                        "\tpop rax\n"
                        
                        "\tmov rbx, rax\n"
                        "\tCLOSURE_ENV rbx\n"
                        "\tpush rbx\n"
                        "\tCLOSURE_CODE rax\n"
						
						;;Copy new frame to old frame in stack
						"\tmov rdx, qword [rbp+3*8]\n"
						"\tadd rdx, 5\n"
						
						"\tmov rcx, " m "\n"
						"\tadd rcx, 3\n"
						
						"\tmov rdi, rbp\n"
						"\tsub rdi, 8\n"
						
						;;save old return address R8 and rbp R9
						"\tmov R8, qword [rbp + 1*8]\n"
						"\tmov R9, qword [rbp + 0*8]\n"
						"\tmov R10, rax\n"
						
						".COPY:\n"
						
						"\tCOPY_STACK rdi, rdx\n"
						"\tsub rdi, 8\n"
						"\tdec rcx\n"
						"\tcmp rcx, 0\n"
						"\tjne .COPY\n"
						;;until here...
						
						"\tmov rax, rdx\n"
						"\tinc rax\n"
						"\tmov rcx, 8\n"
						"\tmul rcx\n"
						"\tadd rdi, rax\n"
						"\tmov rsp, rdi\n"
						;"\tsub rsp, 8*" m "\n"
						;"\tsub rsp, 8*3\n"
						
						"\tpush R8\n"
						"\tmov rbp, R9\n"
						"\tmov rax, R10\n"
						
                        "\tjmp rax\n"		
						)))

            applic-code)))			

        
			
(define code-gen-get-loc-of-vars
	(lambda (expr)
		(let* ((tag (car expr)))
		  (cond
			((equal? tag 'pvar) (code-gen-pvar-memory-location expr))
			((equal? tag 'bvar) (code-gen-bvar-memory-location expr))
			((equal? tag 'fvar) (code-gen-fvar-memory-location expr))
			(else 'code-gen-get-loc-of-vars-failed)))))
			
(define code-gen-pvar-memory-location
    (lambda (expr)
        (let* ((index (list-ref expr 2))
			   (stack-loc (+ 4 index)))
            (string-append
				"\tmov rax, rbp\n"
				"\tadd rax, " (number->string (* stack-loc 8)) "\n"
            ))))
		
(define code-gen-fvar-memory-location
    (lambda (expr)
        (let* ((name (cadr expr))
               (gname (find-in-global-table name)))
            (string-append
				"\tmov rax, " gname "\n"
            ))))
		
(define code-gen-bvar-memory-location
    (lambda (expr)
        (let* ((index (list-ref expr 3))
               (major (number->string (list-ref expr 2))))
            (string-append
                "\tmov rbx, qword [rbp + 2*8]\n"
                "\tmov rbx, qword [rbx + " major "*8]\n"
                "\tmov rax, rbx\n"
				"\tmov rbx, " (number->string (* index 8)) "\n"
				"\tadd rax, rbx\n"
            ))))
            
			
(define code-gen-pvar
    (lambda (expr)
        (let* ((index (list-ref expr 2))
			   (stack-loc (number->string (+ 4 index))))
            (string-append
                "\tmov rax, qword [rbp + " stack-loc "*8]\n"
				
				; "\tmov rbx, qword [rbp + 2*8]\n"
                ; "\tmov rbx, qword [rbx]\n"
                ; "\tmov rax, qword [rbx + " (number->string index) "*8]\n"
            ))))
			
      
(define code-gen-bvar
    (lambda (expr)
        (let* ((index (number->string (list-ref expr 3)))
               (major (number->string (list-ref expr 2))))
            (string-append
                "\tmov rbx, qword [rbp + 2*8]\n"
                "\tmov rbx, qword [rbx + " major "*8]\n"
                "\tmov rax, qword [rbx + " index "*8]\n"
            ))))
			
			
(define code-gen-fvar
	(lambda (expr)
		(let* ((var (cadr expr))
                       ;(action1 (display var))
                       ;(action1 (newline))
                       (gname (find-in-global-table var)))
                    (string-append
                        "\tmov rax, qword [" gname "]\n"
                    ))))

		
(define code-gen-box
	(lambda (expr)
		(let* ((value-to-box (cdr expr))
			   (value-to-box-code (code-gen value-to-box)))
		(string-append
			value-to-box-code
			"\tNEW_MALLOC 8, rdi\n"
			"\tmov qword [rdi], rax\n"
			"\tmov rax, rdi\n"
		))))
		
(define code-gen-box-set
	(lambda (expr)
		(let* ((from (list (list-ref expr 2)))
			   (from-code (code-gen from))
			   (to (list-ref expr 1))
			   (to-code (code-gen-get-loc-of-vars to)))
			(string-append
				from-code
				"\tmov rdx, rax\n"
				to-code
				"\tmov rax, qword [rax]\n"
				"\tmov qword [rax], rdx\n"
				"\tmov rax, SOB_VOID\n"
			))))
			
(define code-gen-box-get
	(lambda (expr)
		(let* ((from (list-ref expr 1))
			   (from-code (code-gen-get-loc-of-vars from)))
			(string-append
				from-code
				"\tmov rax, qword [rax]\n"
				"\tmov rax, qword [rax]\n"
			))))
			
(define code-gen-set
	(lambda (expr)
		(let* ((from (list (list-ref expr 2)))
			   (from-code (code-gen from))
			   (to (list-ref expr 1))
			   (to-code (code-gen-get-loc-of-vars to)))
			(string-append
				from-code
				"\tmov rdx, rax\n"
				to-code
				"\tmov qword [rax], rdx\n"
				"\tmov rax, SOB_VOID\n"
			))))
			
			
(define code-gen-define
	(lambda (expr)
		(let* ((var (find-in-global-table (cadr (list-ref expr 1))))
			   (value (list (list-ref expr 2)))
			   (value-code (code-gen value)))
			(string-append
				value-code
				"\tmov qword [" var "], rax\n"
				"\rmov rax, SOB_VOID\n"
			))))
			   
			
 
(define code-gen
    (lambda (expr)
        (fold-left
            (lambda (acc x)
                (string-append 
                    acc
                    (let* ((tag (car x)))
                        (cond
                            ((equal? tag 'const) (code-gen-const x))
                            ((equal? tag 'if3) (code-gen-if3 x))
                            ((equal? tag 'seq) (code-gen-seq x))
                            ((equal? tag 'or) (code-gen-or x))
							((equal? tag 'set) (code-gen-set x))
                            ((equal? tag 'pvar) (code-gen-pvar x))
                            ((equal? tag 'bvar) (code-gen-bvar x))
							((equal? tag 'fvar) (code-gen-fvar x))
							((equal? tag 'box) (code-gen-box x))
							((equal? tag 'box-set) (code-gen-box-set x))
							((equal? tag 'box-get) (code-gen-box-get x))
                            ((equal? tag 'lambda-simple)
                                (begin
                                    (set! lambda-level (+ lambda-level 1))
                                    (code-gen-lambda-simple x)))
                            ((equal? tag 'lambda-opt)
                                (begin
                                    (set! lambda-level (+ lambda-level 1))
                                    (code-gen-lambda-opt x)))
                            ((equal? tag 'applic) (code-gen-applic x))
                            ((equal? tag 'tc-applic) (code-gen-tc-applic x))
							((equal? tag 'define) (code-gen-define x))
                            (else 6760133)))
                ))
			""
			expr)))
			
(define code-gen-and-print
    (lambda (expr)
        (fold-left
            (lambda (acc x)
                (string-append 
                    acc
                    (let* ((tag (car x)))
                        (cond
                            ((equal? tag 'const) (code-gen-const x))
                            ((equal? tag 'if3) (code-gen-if3 x))
                            ((equal? tag 'seq) (code-gen-seq x))
                            ((equal? tag 'or) (code-gen-or x))
							((equal? tag 'set) (code-gen-set x))
                            ((equal? tag 'pvar) (code-gen-pvar x))
                            ((equal? tag 'bvar) (code-gen-bvar x))
							((equal? tag 'fvar) (code-gen-fvar x))
							((equal? tag 'box) (code-gen-box x))
							((equal? tag 'box-set) (code-gen-box-set x))
							((equal? tag 'box-get) (code-gen-box-get x))
                            ((equal? tag 'lambda-simple)
                                (begin
                                    (set! lambda-level (+ lambda-level 1))
                                    (code-gen-lambda-simple x)))
                            ((equal? tag 'lambda-opt)
                                (begin
                                    (set! lambda-level (+ lambda-level 1))
                                    (code-gen-lambda-opt x)))
                            ((equal? tag 'applic) (code-gen-applic x))
                            ((equal? tag 'tc-applic) (code-gen-tc-applic x))
							((equal? tag 'define) (code-gen-define x))
                            (else 6760133)))
                    write
                ))
			""
			expr)))


(define dedupe  
    (lambda (e)
        (if (null? e) 
            '()
            (cons (car e) 
                  (dedupe 
                    (filter 
                        (lambda (x) 
                            (not (equal? x (car e)))) 
                        (cdr e)))))))

         
(define get-const-list
    (lambda (expr)
        (if (null? expr)
            (list '())
            (if (vector? (car expr))
                (append (list (car expr)) (get-const-vector (car expr)) (list (cdr expr)) (get-const-list (cdr expr)))
                (if (list? (car expr))
                    (append (list (car expr)) (get-const-list (car expr)) (list (cdr expr)) (list expr) (get-const-list (cdr expr)))
                    (if (pair? (car expr))
                        (append (list (car expr)) (get-const-pair (car expr)) (list (cdr expr)) (get-const-list (cdr expr)))
                        (if (number? (car expr))
                            (append (map string->number (string-split (number->string (car expr)) #\/)) expr (list expr) (get-const-list (cdr expr)))
                            (append (list (car expr)) (list (cdr expr)) (list expr) (get-const-list (cdr expr))))))))))
            
            
(define get-const-vector
    (lambda (expr)
        (let* ((lst (vector->list expr)))
            (if (null? lst)
                (list '())
                (if (vector? (car lst))
                    (append (list (car lst)) (get-const-vector (car lst)) (get-const-vector (list->vector (cdr lst))))
                    (if (list? (car lst))
                        (append (list (car lst)) (get-const-list (car lst)) (get-const-vector (list->vector (cdr lst))))
                        (if (pair? (car lst))
                            (append (list (car lst)) (get-const-pair (car lst)) (get-const-vector (list->vector (cdr lst))))
                            (if (number? (car lst))
                                (append (map string->number (string-split (number->string (car lst)) #\/)) lst  (get-const-vector (list->vector (cdr lst))))
                                (append (list (car lst)) (get-const-vector (list->vector (cdr lst))))))))))))
					
(define get-const-pair
    (lambda (expr)
        (if (null? expr)
            (list '())
            (if (not (or (vector? expr) (list? expr) (pair? expr)))
                (if (number? expr)
                    (append (map string->number (string-split (number->string expr) #\/)) (list expr))
                    (list expr))
                (if (vector? (car expr))
                    (append (list (car expr)) (get-const-vector (car expr)) (list (cdr expr)) (get-const-pair (cdr expr)))
                    (if (list? (car expr))
                        (append (list (car expr)) (get-const-list (car expr)) (list (cdr expr)) (list expr) (get-const-pair (cdr expr)))
                        (if (pair? (car expr))
                            (append (list (car expr)) (get-const-pair (car expr)) (list (cdr expr)) (get-const-pair (cdr expr)))
                            (if (number? (car expr))
                                (append (map string->number (string-split (number->string (car expr)) #\/)) (list (car expr)) (list expr) (get-const-pair (cdr expr)))
                                (append (list (car expr)) (list (cdr expr)) (list expr) (get-const-pair (cdr expr)))))))))))
      
      
(define make-const-table-dup-nonTag
    (lambda (expr)
        (if (or (not (list? expr)) (empty? expr))
            '()
            (if (equal? (car expr) 'const)
                (if (vector? (cadr expr))
                    (append (list (cadr expr)) (get-const-vector (cadr expr)))
                    (if (list? (cadr expr))
                        (get-const-list (cadr expr))
                        (if (number? (cadr expr))
                            (append (map string->number (string-split (number->string (cadr expr)) #\/)) (cdr expr))
                            (if (pair? (cadr expr))
                                    (append (list (cadr expr)) (get-const-pair (cadr expr)))
                                    (cdr expr)))))
                (fold-left (lambda (acc x) (append acc (make-const-table-dup-nonTag x))) '() expr)))))
                
(define make-const-table
    (lambda (expr)
        (set! const-counter 0)
        (set! const-table (fold-left 
            (lambda (acc x)
                (let ((name (string-append "c" (number->string const-counter))))
                    (set! const-counter (+ const-counter 1))
                    (append acc (list (list name x)))))
            '()
            (dedupe (append (make-const-table-dup-nonTag expr) (list '() #f #t (void))))))))
               
;;Old			   
(define make-globalVar-table-dup-nonTag
    (lambda (expr)
        (if (or (not (list? expr)) (empty? expr))
            '()
            (if (and (equal? (car expr) 'fvar) (= (length expr) 2))
                (cdr expr)
                (fold-left (lambda (acc x) (append acc (make-globalVar-table-dup-nonTag x))) '() expr)))))
                
;;Old                
(define make-globalVar-table
    (lambda (expr)
        (fold-left 
            (lambda (acc x)
                (let ((name (string-append "fvar" (number->string fvar-counter))))
                    (set! fvar-counter (+ fvar-counter 1))
                    (append acc (list (list name x)))))
            '()
            (dedupe (make-globalVar-table-dup-nonTag expr)))))

;;New
(define make-global-table-dup-nonTag
    (lambda (expr)
        (if (or (not (list? expr)) (empty? expr))
            '()
            (if (or (and (equal? (car expr) 'define) (= (length expr) 3)) (and (equal? (car expr) 'set) (equal? (caadr expr) 'fvar) (= (length expr) 3)))
                (append
                        (cdr (list-ref expr 1))
                        (fold-left (lambda (acc x) (append acc (make-global-table-dup-nonTag x))) '() expr)
                )
                (fold-left (lambda (acc x) (append acc (make-global-table-dup-nonTag x))) '() expr)))))
			
;;New			
(define make-global-table
    (lambda (expr)
        (set! global-table (dedupe (make-global-table-dup-nonTag expr)))
        (set! global-table (dedupe (append global-table built-ins)))
        (set! global-table (map
                            (lambda (x)
                                (let ((name (string-append "g" (number->string global-counter))))
                                    (set! global-counter (+ global-counter 1))
                                    (list name x)))
                            global-table))
        (set! global-table (append global-table (list (list "my_very_own_reverse" 'reverse ))))))
        
        
(define improper->proper
    (lambda (lst)
        (letrec ((rec-func
            (lambda (ls)
                (if (null? ls)
                    '()
                    (if (not (pair? (cdr ls)))
                        (list (car ls) (cdr ls))
                        (cons (car ls) (improper->proper (cdr ls))))))))
            (rec-func lst))))
        
(define make-symbol-table
    (lambda (expr)
        (letrec ((symbol-table-with-dups (lambda (expr)
                    (if (or (not (list? expr)) (empty? expr))
                        (if (symbol? expr)
                            (list expr)
                            (if (vector? expr)
                                (fold-left (lambda (acc x) (append acc (symbol-table-with-dups x))) '() (vector->list expr))
                                (if (pair? expr)
                                    (fold-left (lambda (acc x) (append acc (symbol-table-with-dups x))) '() (improper->proper expr))
                                    '())))
                        (if (and (equal? (car expr) 'const) (symbol? (cadr expr)) (= (length expr) 2))
                            (cdr expr)
                            (fold-left (lambda (acc x) (append acc (symbol-table-with-dups x))) '() expr))))))
            (set! symbol-table (dedupe (symbol-table-with-dups expr)))
            (set! symbol-table (map (lambda (x) (list x (symbol->string x) (string-length (symbol->string x)))) symbol-table))
            (set! string-table (append string-table 
                (filter (lambda (y) (not (equal? (car y) 'fake))) (map 
                    (lambda (x)
                        (let*  ((str (car x))
                                (name (cadr x))
                                (sname (string-append "s" (number->string string-counter))))
                            (if (null? (find-name-in-string-table name))
                                (begin
                                    (set! string-counter (+ string-counter 1))
                                    (list sname name))
                                (list 'fake 'fake))))
                    symbol-table))))
            )))


(define (string-split str delim)
  (define in (open-input-string str))
  (let loop ((rv '()) (out (open-output-string)))
    (define c (read-char in))
    (cond ((eof-object? c)
           (reverse (cons (get-output-string out) rv)))
          ((char=? c delim)
           (loop (cons (get-output-string out) rv)
                 (open-output-string)))
          (else
           (write-char c out)
           (loop rv out)))))
		   
		   
(define code-gen-symbol-table
    (lambda ()
        (let* ((first (car symbol-table))
               (str (list-ref first 1))
               (cname (find-name-in-string-table str)))
            (string-append
            
                "\tNEW_MALLOC 8, R12\n"
                "\tmov rdi, " cname "\n"
                "\tsub rdi, start_of_data\n"
                "\tshl rdi, 4\n"
                "\tor rdi, T_SYMBOL\n"
                "\tmov qword [R12], rdi\n"
                
                "\tmov rdi, SOB_NIL\n"
                "\tMAKE_LITERAL_PAIR_OPT R12, rdi\n"
                
                
                (fold-left
                    (lambda (acc x)
                        (let*  ((str (list-ref x 1))
                                (cname (find-name-in-string-table str)))
                            (string-append
                                acc
                                
                                "\tNEW_MALLOC 8, R12\n"
                                "\tmov rdi, " cname "\n"
                                "\tsub rdi, start_of_data\n"
                                "\tshl rdi, 4\n"
                                "\tor rdi, T_SYMBOL\n"
                                "\tmov qword [R12], rdi\n"
                                
                                "\tmov rdi, rax\n"
                                "\tMAKE_LITERAL_PAIR_OPT R12, rdi\n"
                            
                            )))
                    ""
                    (cdr symbol-table)
                )
                
                "\tmov qword [symbol_table], rax\n"
                
            )
        )
    )
)
                   

(define code-gen-global-table
	(lambda ()
		(let* ((prologue "section .data\nglobals:\n"))
            (string-append
                prologue
                (fold-left
                    (lambda (acc x) 
                        (let* ((name (car x)))
                                                (string-append acc name ":\n\t dq SOB_UNDEFINED\n")))
					""
					global-table
				)
				"\n"
			))))

			
(define code-gen-string-table
	(lambda ()
		(let* ((prologue "section .data\nstring_table:\n"))
            (string-append
                prologue
                (fold-left
                    (lambda (acc x) 
                        (let* ((sname (car x))
                               (name (cadr x)))
                                                (string-append acc sname ":\n\t MAKE_LITERAL_STRING \"" name "\"\n")))
					""
					string-table
				)
				"\n"
			))))
			
            
(define code-gen-const-table
    (lambda ()
        (let* ((prologue "section .data\nstart_of_data:\nsymbol_table:\n\tdq SOB_UNDEFINED\n"))
            (string-append
                prologue
                (fold-left
                    (lambda (acc x) 
                        (let* ((name (car x))
                               (value (cadr x))) 
                            (cond
                                ((const-void? value)
                                    (string-append acc name ":\n\t dq SOB_VOID\n"))
                                ((boolean? value)
                                    (let ((bool -1))
                                        (if value (set! bool 1) (set! bool 0))
                                        (string-append acc name ":\n\t dq MAKE_LITERAL(T_BOOL, " (format "~a" bool) ")\n")))
                            
                                ((integer? value)
                                (string-append acc name ":\n\t dq MAKE_LITERAL(T_INTEGER, " (number->string value) ")\n"))
                                
								;;NEEDS ADDING SUPPORT FOR TABS AND OTHER STUFFS BESIDE NEWLINE
                                ((string? value)
                                    (let* ((splitted (string-split value #\newline)))
                                        (string-append acc name ":\n\t MAKE_LITERAL_STRING "
                                                (fold-left
                                                        (lambda (acc word) 
                                                                (string-append acc ", CHAR_NEWLINE, \"" word "\""))
                                                        (string-append "\"" (car splitted) "\"")
                                                        (cdr splitted))
                                                "\n")
                                                        ))
                                                        
                                ((rational? value)
                                (let* ((numer (find-name-in-const-table (numerator value)))
                                       (denom (find-name-in-const-table (denominator value))))
                                (string-append acc name ":\n\t dq MAKE_LITERAL_FRACTION(" numer ", " denom ")\n")))
											
                                ((symbol? value)
                                (string-append acc name ":\n\t dq MAKE_LITERAL_SYMBOL(" (find-name-in-string-table (symbol->string value)) ")\n"))
                                ;((symbol? value) "")
                                
                                ((char? value)
                                    (cond
                                        ((equal? value #\newline)
                                            (string-append acc name ":\n\t dq MAKE_LITERAL(T_CHAR, CHAR_NEWLINE)\n"))
                                        (else
                                            (string-append acc name ":\n\t dq MAKE_LITERAL(T_CHAR, '" (string value) "')\n"))))
                                
                                ((null? value)
                                (string-append acc name ":\n\t dq SOB_NIL\n"))
                                
                                ((or (list? value) (pair? value))
                                    (let* ((my-name (find-name-in-const-table value))
                                           (car-name (find-name-in-const-table (car value)))
                                           (cdr-name (find-name-in-const-table (cdr value))))
                                        (string-append acc name ":\n\t dq MAKE_LITERAL_PAIR(" car-name ", " cdr-name ")\n")))
                                        
                                ((vector? value)
                                    (let* ((my-name (find-name-in-const-table value))
                                           (args-names (string-append (fold-left 
                                                        (lambda (vecAcc x)
                                                            (let* ((name (find-name-in-const-table x)))
                                                                (string-append vecAcc name ", ")))
                                               ""
                                               (vector->list value)) "\n")))
                                    (string-append acc my-name ":\n\t MAKE_LITERAL_VECTOR " args-names)))
									
                                (else 'code-gen-const-table-failed)
                                )))
                    ""
                    const-table)
                "\n"))))
                
                
(define set-up (lambda ()

    (string-append
        (code-gen-symbol-table)

        (fold-left
            (lambda (acc x)
                (let* ((name (symbol->string (cadr x)))
                    (gname (car x))
                    (real-name
                        (cond
                            ((equal? name "+") "plus")
                            ((equal? name "-") "minus")
                            ((equal? name "*") "multiply")
                            ((equal? name "/") "divide")
                            ((equal? name "=") "equal")
                            ((equal? name ">") "greater")
                            ((equal? name "<") "less")
                            ((equal? name "make-string") "make_string")
                            ((equal? name "string->symbol") "string_to_symbol")
                            ((equal? name "symbol->string") "symbol_to_string")
                            ((equal? name "char->integer") "char_to_integer")
                            ((equal? name "integer->char") "integer_to_char")
                            ((equal? name "set-car!") "set_car")
                            ((equal? name "set-cdr!") "set_cdr")
                            ((equal? name "vector-length") "vector_length")
                            ((equal? name "vector-ref") "vector_ref")
                            ((equal? name "vector-set!") "vector_set")
                            ((equal? name "string-length") "string_length")
                            ((equal? name "string-ref") "string_ref")
                            ((equal? name "string-set!") "string_set")
                            ((equal? name "make-vector") "make_vector")
                            (else name))))
                            
                            (if (contains? built-ins (string->symbol name))
                            
                    (string-append
                        acc
                        "
        NEW_MALLOC 16, rax
        mov rbx, 300776
        mov rdx, B_" real-name "
        MAKE_LITERAL_CLOSURE rax, rbx, rdx
        mov rax, [rax]
        mov qword [" gname "], rax
        "
                    )
                    acc)
            ))
            ""
            global-table
        )
)))
        
(define reverse-fix
    (lambda ()
        (string-append
        "
        mov rax, qword [" (find-in-global-table 'reverse) "]
        mov qword [my_very_own_reverse], rax
        ")))
