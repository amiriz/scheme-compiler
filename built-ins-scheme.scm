(define (list . x) x)

(define (our_map_bin f lst)
  (if (null? lst)
     '()
     (cons (f (car lst)) (our_map_bin f (cdr lst)))))

(define (our_append_bin l m)
    (if (null? l)
        m
        (cons (car l) (our_append_bin (cdr l) m))))
		
(define (< num . s)
	(if (null? s)
		#t
		(let ((next (car s)))
			(if (or (> num next) (= num next))
				#f
				(apply < (append (list next) (cdr s)))))))
        
(define (remainder num a)
    (cond
        ((< num 0)
            (- (remainder (- num) a)))
        ((< a 0)
            (remainder num (- a)))
        ((= a 1)
            0)
        (else (if (< (- num a) 0)
                num
                (remainder (- num a) a)))))

                 
(define (append . lst)
       (letrec ((append_bin (lambda (lst1 lst2)
                            (if (null? lst1) lst2
                                (cons (car lst1) (append_bin (cdr lst1) lst2)))))
                (append-rec
                 (lambda (lst) 
                          (if (null? lst) lst
                              (if (pair? (cdr lst)) (append_bin (car lst) (append-rec (cdr lst)))
                                  (car lst))))))
         (append-rec lst))) 
                
(define (reverse lst)
  (if (null? lst)
     '()
     (our_append_bin (reverse (cdr lst)) (list (car lst)))
  )
)

(define (some? function list)
    (and (pair? list)
         (or (function (car list))
             (some? function (cdr list)))))

(define (map function list1 . more-lists)
  (let ((lists (cons list1 more-lists)))
    (if (some? null? lists)
        '()
        (cons (apply function (our_map_bin car lists))
              (apply map (our_append_bin (list function) (our_map_bin cdr lists)))))))


